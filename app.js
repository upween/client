var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var port = process.env.PORT || 3000; 
var routes = require('./routes/index');
var session = require('express-session')
var multer = require('multer');
var cors = require('cors');
var Loki = require('lokijs');
var bodyParser = require('body-parser');
const helmet = require('helmet');

var app = express();
app.use(helmet.frameguard({ action: "deny" }));
app.use(helmet.xssFilter())
app.use(helmet.noCache())
app.use(helmet.noSniff())
// view engine setup
app.use(helmet.xssFilter())
app.use(helmet.noCache())
app.use(helmet.noSniff())
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({secret: 'rojgarclient'}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname,'uploads')));
app.use(express.static('uploads'));
app.all('/uploads/*', function (req,res, next) {

  if (req.session.login){
    res.status(200).send();
  }
  else{
    res.status(403).send({
      message: 'Access Forbidden'
    });
  }
  
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
routes(app);
var storage = multer.diskStorage({
  
  destination: function (req, file, cb) {
    if (req.cookies.modaltype == "image"){
      cb(null, './uploads/Pics/')
    }
    else if (req.cookies.modaltype == "attachresume"){
      cb(null, './uploads/CV/')
    }
    else if (req.cookies.modaltype == "domiciledocs"){
      cb(null, './uploads/Domicile/')
    }
    else if (req.cookies.modaltype == "Handicappeddoc"){
      cb(null, './uploads/HandicappedDoc/')
    }
    else if (req.cookies.modaltype == "Grievencedoc"){
      cb(null, './uploads/Grievence/')
    }
    else if (req.cookies.modaltype == "Feedback"){
      cb(null, './uploads/Feedback/')
    }
  },
  filename: function (req, file, cb) {
    //console.log(req.cookies.city)
    //var name = file.originalname
    //name=name.split(".");
    //debugger
    if (req.cookies.modaltype == "image"){
      cb(null, req.cookies.FirstName+req.cookies.CandidateId+path.extname(file.originalname))
    }
  else  if (req.cookies.modaltype == "attachresume"){
      cb(null, "Resume"+req.cookies.FirstName+req.cookies.CandidateId+path.extname(file.originalname))
    }
  else  if (req.cookies.modaltype == "domiciledocs"){
      cb(null,"Dom"+req.cookies.FirstName+req.cookies.CandidateId+path.extname(file.originalname))
    }
    else  if (req.cookies.modaltype == "Handicappeddoc"){
      cb(null,"HDoc"+req.cookies.FirstName+req.cookies.CandidateId+path.extname(file.originalname))
    }
    else  if (req.cookies.modaltype == "Grievencedoc"){
      cb(null,"Grievence"+Date.now()+path.extname(file.originalname))
    }
    else  if (req.cookies.modaltype == "Feedback"){
      cb(null,"Feedback"+req.cookies.FeedbackId+path.extname(file.originalname))
    }
    //console.log(req);
    //callme();
  }
});
// Init Upload
const upload = multer({
  storage: storage,
  limits:{fileSize: 1000000},
  fileFilter: function(req, file, cb){
    checkFileType(req,file, cb);
  }
}).single('myFile');

function checkFileType(req,file, cb){
  // Allowed ext
  //console.log(file);
  var ext = path.extname(file.originalname);
  if (req.cookies.modaltype == "image"){
	  if(ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
            cb('Error: Image Only!');
        }
		else{
		return cb(null,true);}
  }
  else if (req.cookies.modaltype == "attachresume"){
	  if(ext !== '.doc' && ext !== '.docx'&& ext !== '.pdf') {
            cb('Error: Document Only!');
        }
        
		else{
		return cb(null,true);}
 }
 else if (req.cookies.modaltype == "domiciledocs" ){
  if(ext !== '.doc' && ext !== '.docx'&& ext !== '.pdf') {
          cb('Error: Document Only!');
      }
      
  else{
  return cb(null,true);}
}
else if (req.cookies.modaltype == "Handicappeddoc"){
  if(ext !== '.doc' && ext !== '.docx'&& ext !== '.pdf') {
          cb('Error: Document Only!');
      }
      
  else{
  return cb(null,true);}
}
else if (req.cookies.modaltype == "Grievencedoc" || req.cookies.modaltype == "Feedback"){
  if(ext !== '.doc' && ext !== '.docx'&& ext !== '.pdf' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
          cb('Error: Document Only!');
      }
      
  else{
  return cb(null,true);}
}
}

app.post('/Profile_new', (req, res) => {
  upload(req, res, (err) => {
    if(err){
      if (err.code == "LIMIT_FILE_SIZE"){
        return res.end("File Size Limit Exceeded");
      }
      else{
        return res.end(err);
      } 
    } else {
      if(req.file == undefined){
        res.end("Error No File Selected");
      } else {
        res.end('File Uploaded!'+req.file.filename)
      }
    }
  });
}); 
app.post('/Profilereg', (req, res) => {
  upload(req, res, (err) => {
    if(err){
      if (err.code == "LIMIT_FILE_SIZE"){
        return res.end("File Size Limit Exceeded");
      }
      else{
        return res.end(err);
      } 
    } else {
      if(req.file == undefined){
        res.end("Error No File Selected");
      } else {
        res.end('File Uploaded!'+req.file.filename)
      }
    }
  });
}); 
app.post('/GrievenceForm', (req, res) => {
  upload(req, res, (err) => {
    if(err){
      if (err.code == "LIMIT_FILE_SIZE"){
        return res.end("File Size Limit Exceeded");
      }
      else{
        return res.end(err);
      } 
    } else {
      if(req.file == undefined){
        res.end("Error No File Selected");
      } else {
        res.end('File Uploaded!'+req.file.filename)
      }
    }
  });
});
app.post('/FeedBackForm', (req, res) => {
  console.log(req);
  upload(req, res, (err) => {
    console.log(err);
    if(err){
         if (err.code == "LIMIT_FILE_SIZE"){
        res.render('pages/FeedBack/FeedBackForm', {
          msg: "File Size Limit Exceeded",
          file:'',
          type: req.cookies.modaltype
        });
      }
      else{
      res.render('pages/FeedBack/FeedBackForm', {
        msg: err,
        file:'',
        type: req.cookies.modaltype
      });
    }
   } else {
      if(req.file == undefined){
        res.render('pages/FeedBack/FeedBackForm', {
          msg: 'Error: No File Selected!',
          file:'',
          type: req.cookies.modaltype
        });
      } else {
        res.render('pages/FeedBack/FeedBackForm', {
          msg: 'File Uploaded!',
          file: req.file.filename,
          type: req.cookies.modaltype
        });
      }
    }
  });
});
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('pages/error');
});
app.listen(port, function() {
  console.log('Server listening on port ' + port + '...');
});
module.exports = app;
