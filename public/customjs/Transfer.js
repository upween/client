const { InternalServerError } = require("http-errors");

function FillDistrict(districtid) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "district/0/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {
            jQuery("#district").empty();
           jQuery("#district").append(jQuery("<option ></option>").val("0").html("Select District"));
               for (var i = 0; i < data[0].length; i++) {
                jQuery("#district").append(jQuery("<option></option>").val(data[0][i].DistrictId).html("District Employment Exchange, "+data[0][i].DistrictName));
            }
            jQuery("#district1").empty();
            jQuery("#district1").append(jQuery("<option ></option>").val("0").html("Select District"));
                for (var i = 0; i < data[0].length; i++) {
                 jQuery("#district1").append(jQuery("<option></option>").val(data[0][i].DistrictId).html(data[0][i].DistrictName));
             }
             jQuery("#district1").val(districtid);
             jQuery("#district").val(districtid);
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}

jQuery('#district1').on('change', function () {
    FillCity(jQuery('#district1').val());
});

function FillCity(district) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "city/"+district+"/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {
            var data1 = data[0];
            jQuery("#citynam").empty();
            jQuery("#citynam").append(jQuery("<option ></option>").val("0").html("Select City"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#citynam").append(jQuery("<option></option>").val(data1[i].CityId).html(data1[i].CityName));
            }
        },
        error: function (xhr) {
            //swal(xhr.d, "", "error")
        }
    });
}

jQuery('#citynam').on('change', function () {
    FillVillage(jQuery('#citynam').val());
});

function FillVillage(city) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "village/" + city + "/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {
            var data1 = data[0];
            jQuery("#Village").empty();
           
           
            jQuery("#Village").append(jQuery("<option ></option>").val("0").html("Select Village"));
            jQuery("#Village").append(jQuery("<option ></option>").val("89898").html("Other"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#Village").append(jQuery("<option></option>").val(data1[i].VillageId).html(data1[i].VillageName));
            }
        },
        error: function (xhr) {
            //swal(xhr.d, "", "error")
        }
    });
}


function FillDivision() {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "MstDivision/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {
           
            jQuery("#Division").empty();
            jQuery("#Division").append(jQuery("<option ></option>").val("0").html("Select Division"));
                for (var i = 0; i < data[0].length; i++) {
                 jQuery("#Division").append(jQuery("<option></option>").val(data[0][i].Division_id).html(data[0][i].Division_name));
             }
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}

function FillCandidateDetail(){
    var path = serverpath + "secured/JobseekerProfile/'" + sessionStorage.getItem("CandidateId") + "'"
    securedajaxget(path, 'parsedatasecuredFillCandidateDetail', 'comment', 'control');
}
function parsedatasecuredFillCandidateDetail(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillCandidateDetail();
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else{
        var appendata="";
        appendata+=" <li>"+data[0][0].RegistrationId+"</li><li>"+data[0][0].GuardianFatherName+"</li><li>"+data[0][0].DateOfBirth+"</li><li> Jun 2022</li><li>"+data[0][0].Gender+"</li><li>"+data[0][0].CategoryName+"</li>";
        jQuery("#candidatedetail").html(appendata);
        FillDistrict(data[0][0].DistrictId) 
    }
   
}

function UpdTransfer(){
    var MasterData = {
        
        "p_CandidateId": sessionStorage.getItem("CandidateId"),
        "p_Area": jQuery("#Area").val(),
        "p_PermanentAddress": jQuery("#Address").val(),
        "p_PinCode": jQuery("#Pincode").val(),
        "p_DistrictId":jQuery("#district1").val(),
        "p_CityId":jQuery("#citynam").val(),
        "p_VillageId":jQuery("#Village").val(),
       }

    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/transferregistration/";
    securedajaxpost(path,'parsedatasecuredUpdTransfer','comment',MasterData,'control')
}
function parsedatasecuredUpdTransfer(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        UpdTransfer();
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else{
        if (data[0][0].ReturnValue == "1") {
              toastr.success("Update Successful", "", "success")
              return true;
          }
    }
   
}
function CheckValidationTransfer(){
        if(isValidate){
          
    if(jQuery("#district").val()==""){
        getvalidated('district','select','exchange District');
          return false;
 }

   else if(jQuery("#Area").val()==""){
    getvalidated('Area','select','Area');
      return false;
   }
   else if(jQuery("#Division").val()==""){
    getvalidated('Division','select','Division');
      return false;
   }
  
   else if(jQuery("#district1").val()==""){
    getvalidated('district1','select','district');
      return false;
   }
   else if(jQuery("#citynam").val()==""){
    getvalidated('citynam','select','City Name');
      return false;
   }
   else if(jQuery("#Village").val()==""){
    getvalidated('Village','select','Village Name');
      return false;
   }
   else if(jQuery("#Pincode").val()==""){
    getvalidated('Pincode','text','Pincode');
      return false;
   }
   else if(jQuery("#Address").val()==""){
    getvalidated('Address','text','Address');
      return false;
   }


      else{
        UpdTransfer();
    }
}
else{
    UpdTransfer();
}
}