function CheckIdentificationDetail() {
    if(isValidate){

    
    var idno= document.getElementById('cardno');
    if (jQuery('#idtype').val() == "0") {
        getvalidated('idtype','select','Identification Type'); 
    return false;}


    if (jQuery('#cardno').val() == "") {
        getvalidated('cardno','text','Card Number'); 
    return false;}




//     if (jQuery('#cardno').val() == '') {
       
       
  
//     if (jQuery('#idtype').val() == 'Driving Licence'){
//         getvalidated('cardno','text','Driving License Number'); 
//         return false; }
//     if (jQuery('#idtype').val() == 'Electric Licence'){
//         getvalidated('cardno','text','Electric License Number'); 
//         return false;  }

// }
if (jQuery('#idtype').val()  == 'Driving Licence' && idno.value.length < 8 && idno.value.length > 0 ) {
    checkLength('cardno','Driving license Number','16');
    return false;
}

if (jQuery('#idtype').val()  == 'Electric Licence'  && idno.value.length < 8 && idno.value.length > 0 ) {
    checkLength('cardno','Electric License Number','8');
    return false;
}
    else {
        InsUpdIdentificationDetail();
     
    }
}
else{
    InsUpdIdentificationDetail();
}
}

function InsUpdIdentificationDetail() {//Sonali
    $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);
 
    var MasterData = {
        "p_IdentificationId": localStorage.getItem("IdentificationId"),
        "p_CandidateId": sessionStorage.getItem("CandidateId"),
        "p_IdentificationType": jQuery('#idtype').val(),
        "p_IdentificationNumber": jQuery('#cardno').val(),
        "p_Date": jQuery('#IdDate').val(),
        "p_Upto": jQuery('#ValidUpto').val(),
        "p_IssuingAuthority": jQuery('#IssuingAuthority').val(),
        "p_IpAddress": sessionStorage.getItem("IpAddress"),
        "p_LicenseType":$('#licensetype').val(),
    }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/identificationdetails";
    securedajaxpost(path, 'parsrdataidentificationdetails', 'comment', MasterData, 'control')
}
function parsrdataidentificationdetails(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        InsUpdIdentificationDetail();
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else if (data[0][0].ReturnValue == "0") {
        toastr.warning("Card Detail Already Exist", "", "info")
        resetmodeIdedntification();
        return true;
    }
    else if (data[0][0].ReturnValue == "1") {
        
       
        toastr.success("Submit Successfully", "", "success")
        $('#myModal1').modal('toggle');
        FillIdentificationDetailSecured('parsedatasecuredidentificationdetails', 'identificationtable');
        resetmodeIdedntification();
        return true;
    }
    else if (data[0][0].ReturnValue == "2") {
        toastr.success("Submit Successfully", "", "success")
        $('#myModal1').modal('toggle');
        FillIdentificationDetailSecured('parsedatasecuredidentificationdetails', 'identificationtable');
        resetmodeIdedntification();
        return true;
    }

}
function FillIdentificationDetailSecured(funct, control) {//Sonali
    var path = serverpath + "secured/identificationdetails/0/'" + sessionStorage.getItem("CandidateId") + "'/'" + sessionStorage.getItem("IpAddress") + "'"
    securedajaxget(path, funct, 'comment', control);
}
function parsedatasecuredidentificationdetails(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillIdentificationDetailSecured('parsedatasecuredidentificationdetails', 'identificationtable');
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else {
        jQuery("#" + control).empty();
        var idenficationhead = "";
        if (data[0] == "") {
            localStorage.setItem("IdentificationId", "0");
           
        }
        else {
            var data1 = data[0];

            var appenddata = "";
            
            jQuery("#" + control).empty();
            for (var i = 0; i < data1.length; i++) {
                if(data1[i].Date ==''){
                    var date="-";
                }
                else{
                    var date= data1[i].Date ;
                }

                    if(data1[i].Upto ==''){
                        var upto="-";
                    }
                    else{
                        var upto= data1[i].Upto ;
                    }

                    if(data1[i].IssuingAuthority ==''){
                        var issuingAuthority="-";
                    }
                    else{
                        var issuingAuthority= data1[i].IssuingAuthority ;
                    }
                appenddata += "<tr><td>" + data1[i].IdentificationType + "</td><td>" + data1[i].IdentificationNumber + "</td><td>"+data1[i].LicenseType+"</td><td>" + date + "</td><td>" + upto + "</td><td style='    word-break: break-word;'>" + issuingAuthority + "</td><td><span style='margin-left:10px;'><a href='#identificationtab' onclick=EditIdDetail('" + data1[i].IdentificationDetailId + "','" + encodeURI(data1[i].IdentificationType) + "','" + encodeURI(data1[i].IdentificationNumber) + "','" + encodeURI(data1[i].Date) + "','" + encodeURI(data1[i].Upto) + "','" + encodeURI(data1[i].IssuingAuthority) + "','"+encodeURI(data1[i].LicenseType)+"') ><i class='fa fa-pencil'></i></a></span></td><td><span style='margin-left:10px;'><a href='#identificationtab' onclick=Delete("+data1[i].IdentificationDetailId+",'Identification') ><i class='fa fa-times'></i></a></span></td></tr>";
            }
            jQuery("#" + control).html(appenddata);

            idenficationhead += " <span lang='en'>Updated On</span> " + data[0][0].UpdateOn + "";
            
            
        //ProfileStatus('parsedataProfileStatus','progressbar');
        }
        
    }
    jQuery("#idhd").html(idenficationhead);
}
function EditIdDetail(IdentificationId, IdentificationType, IdentificationNumber, Date, Upto, Authority,LicenseType) {//Sonali
    modal('dld');
    $("#111").html("<h5>Edit-Identification Details</h5>");
    $("#idtype").prop("disabled", true);
        localStorage.setItem("IdentificationId", IdentificationId);
        jQuery('#idtype').val(decodeURI(IdentificationType)),
        jQuery('#cardno').val(decodeURI(IdentificationNumber)),
        jQuery('#IdDate').val(decodeURI(Date)),
        jQuery('#ValidUpto').val(decodeURI(Upto)),
        jQuery('#IssuingAuthority').val(decodeURI(Authority))
        $('#licensetype').empty();
        if(decodeURI(IdentificationType)=='Driving Licence'){
            $('#licensetype').append('<option>LMV Commercial</option>');
            $('#licensetype').append('<option>HMV</option>');
            $('#licensetype').val(decodeURI(LicenseType));
           
        }else{
            $('#licensetype').append('<option>Domestic</option>');
            $('#licensetype').append('<option>Industrial</option>');
            $('#licensetype').val(decodeURI(LicenseType));
           
        }
     

}
function resetmodeIdedntification() {//Sonali
    localStorage.setItem("IdentificationId", "0");
    jQuery('#idtype').val("0"),
        jQuery('#cardno').val(""),
        jQuery('#IdDate').val(""),
        jQuery('#ValidUpto').val(""),
        jQuery('#IssuingAuthority').val("")
        $("#idtype").prop("disabled", false);
        $('#licensetype').empty();
        $("#111").html("<h5>Identification Details</h5>");
        $('#licensetype').append('<option>License Type</option>');
        $("#cardno").attr("placeholder", "Identification Number");
        
        jQuery('#idtype').css('border-color', '');
        jQuery('#cardno').css('border-color', '');
                
        $('#valididtype').html("");
        $("#validcardno").html("");
        
}
function placeholder() {
    debugger;
    var sel = document.getElementById("idtype");
    var textbx = document.getElementById("cardno");
    var indexe = sel.selectedIndex;
    $("#validcardno").html("");
    $("#cardno").css('border-color', '');
   // $("#cardno").val("");
    if (indexe == 0) {
        $("#cardno").attr("placeholder", "Identification Number");

    }
    
    if (indexe == 1) {
        $("#cardno").attr("placeholder", "Driving license Number");
    }
    if (indexe == 2) {
        $("#cardno").attr("placeholder", "Electric License Number");
    }
}

$("#cardno").focusout(function () {
   
    var sel = document.getElementById("idtype");
    var textbx = document.getElementById("cardno");
    var indexe = sel.selectedIndex;
    var Idnumb = $("#cardno").val();
    if (Idnumb == '') {
        

        if (indexe == 1) {
            getvalidated('cardno','text','Identification Number')
        }

        if (indexe == 2) {
            getvalidated('cardno','text','Identification Number')
        }
    }
        else{
            
        if (indexe == 1) {
            checkLength('cardno','Driving license Number','16')
        }

        if (indexe == 2) {
            checkLength('cardno','Electric License Number','8')
        }
            
        }

    
});
$('#idtype').on('change', function(){
    $('#licensetype').html('');
    if($('#idtype').val()=='Driving Licence'){
        $('#licensetype').append('<option>LMV Commercial</option>');
        $('#licensetype').append('<option>HMV</option>');
       
    }else if($('#idtype').val()=='Electric Licence'){
        $('#licensetype').append('<option>Domestic</option>');
        $('#licensetype').append('<option>Industrial</option>');
       
    }else{
        jQuery('#licensetype').append('<option>License Type</option>');;
    }
});


function AvoidSpace(event) {
    var k = event ? event.which : window.event.keyCode;
    if (k == 32) return false;
}

