function CheckCertification(){
    if(isValidate){
        if ($.trim(jQuery('#Program').val()) == "") {
            getvalidated('Program','text',' Training Program Name');
            return false;
         }
        if ($.trim(jQuery('#Training').val()) == "") {
            getvalidated('Training','text','Training Centre');
            return false; }
        if ($.trim(jQuery('#Scheme').val()) == "") {
            getvalidated('Scheme','text','Scheme');
            return false; }
        else{
            InsUpdCertification();
        }
    }
    else{
        InsUpdCertification();
    } 
  
}
function InsUpdCertification() {


    $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);
 
    
    var MasterData = {
        "p_CertificationId":localStorage.getItem("CertificationId"),
        "p_TrainingProgramName": $("#Program").val(),
        "p_TrainingCenter":$("#Training").val() ,
        "p_Duration": $("#duration").val(),
        "p_Scheme":$("#Scheme").val(),
        "p_SkillSet":$("#Skill").val(),
        "p_JobPrefrences":$("#Prefrences").val(),
        "p_CertificationYear":$("#certificationyear").val(),
        "p_IsActive":"1",
        "p_CandidateId":sessionStorage.getItem("CandidateId")
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/certification";
    securedajaxpost(path, 'parsrdatacertification', 'comment', MasterData, 'control')
}
function parsrdatacertification(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        InsUpdCertification();
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else if (data[0][0].ReturnValue == "1") {
        ProfileStatus('parsedataProfileStatus','progressbar');
        resetCertification();
        $('#myModal1').modal('toggle');
        FillCertificationSecured('parsedatasecuredcertification', 'certificationtable');
        toastr.success("Insert Successful", "", "success")
        return true;
    }
    else if (data[0][0].ReturnValue == "0") {
       
        
        toastr.warning("Already Exists", "", "info")
        return true;
    }
    else if (data[0][0].ReturnValue == "2") {
        resetCertification();
        $('#myModal1').modal('toggle');
        FillCertificationSecured('parsedatasecuredcertification', 'certificationtable');
        toastr.success("Update Successful", "", "success")
        return true;
    }
   

}


function FillCertificationSecured(funct, control) {
    var path = serverpath + "secured/certification/0/'" + sessionStorage.getItem("CandidateId") + "'/'" + sessionStorage.getItem("IpAddress") + "'/0/'" + sessionStorage.getItem("CandidateId") + "'"
    securedajaxget(path, funct, 'comment', control);
}
function parsedatasecuredcertification(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillCertificationSecured('parsedatasecuredcertification', 'certificationtable');
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else {
        jQuery("#" + control).empty();
        var certificationhead = "";
        if (data[0] == "") {
            localStorage.setItem("CertificationId", "0");
           
        }
        else {
            
       
            var data1 = data[0];

            var appenddata = "";
            
            jQuery("#" + control).empty();
            for (var i = 0; i < data1.length; i++) {
                if(data1[i].FunctionalArea ==null){
                    var functionalArea="-";
                }
                else{
                    var functionalArea= data1[i].FunctionalArea ;
                }

                if(data1[i].Year ==null){
                    var year="-";
                }
                else{
                    var year= data1[i].Year ;
                }

                if(data1[i].Description ==null){
                    var skillSet="-";
                }
                else{
                    var skillSet= data1[i].Description ;
                }

                if(data1[i].Duration =='0'){
                    var duration="-";
                }
                else{
                    var duration= data1[i].Duration+" Month" ;
                }
                appenddata += "<tr><td style='    word-break: break-word;'>" +data1[i].TrainingProgramName+ "</td><td style='    word-break: break-word;'>" + data1[i].TrainingCenter+ "</td><td style='    word-break: break-word;'>" + data1[i].Scheme+ "</td><td>" +duration+ "</td><td>" + skillSet + "</td><td>" + functionalArea + "</td><td>" +year+ "</td><td><span style='margin-left:10px;'><a href='#certificationtab' onclick=EditCertification('" + data1[i].CertificationId + "','" + encodeURI(data1[i].TrainingProgramName) + "','" + encodeURI(data1[i].TrainingCenter) + "','" + encodeURI(data1[i].Scheme) + "','" + encodeURI(data1[i].Duration) + "','" + encodeURI(data1[i].SkillSet) + "','" + encodeURI(data1[i].JobPrefrences) + "','" + encodeURI(data1[i].CertificationYear) + "') '><i class='fa fa-pencil'></i></a></span></td><td><span style='margin-left:10px;'><a href='#certificationtab' onclick=Delete("+data1[i].CertificationId+",'Certification')><i class='fa fa-times'></i></a></span></td></tr>";
                certificationhead = " <span lang='en'>Updated On </span>" + data[0][i].UpdateOn + "";
            }
            jQuery("#" + control).html(appenddata);

            
            
            
        ProfileStatus('parsedataProfileStatus','progressbar');
        }
        
    }
    jQuery("#upd").html( certificationhead);
}
function EditCertification(CertificationId, TrainingProgramName, TrainingCenter, Scheme, MonthId, ESID,JobPrefrences,FinYearId) {
    modal('certification');
    
    $("#Skill").prop("disabled", true);
    $("#Prefrences").prop("disabled", true);
    localStorage.setItem("CertificationId", CertificationId);
    jQuery('#Program').val(decodeURI(TrainingProgramName)),
        jQuery('#Training').val(decodeURI(TrainingCenter)),
        jQuery('#duration').val(decodeURI(MonthId)),
        jQuery('#Scheme').val(decodeURI(Scheme)),
        jQuery('#Skill').val(decodeURI(ESID))
        jQuery('#Prefrences').val(decodeURI(JobPrefrences))
        if(FinYearId==null){
        jQuery('#certificationyear').val("0")
}
        else{
        jQuery('#certificationyear').val(decodeURI(FinYearId))
        }

        if(MonthId==null){
            jQuery('#duration').val("0")
    }
            else{
            jQuery('#duration').val(decodeURI(MonthId))
            }

            if(ESID==null){
                jQuery('#Skill').val("0")
        }
                else{
                jQuery('#Skill').val(decodeURI(ESID))
                }

        
}
function resetCertification(){//Sonali
    localStorage.setItem("CertificationId",'0');
    jQuery('#Program').val(""),
        jQuery('#Training').val(""),
        jQuery('#duration').val("0"),
        jQuery('#Scheme').val(""),
        jQuery('#Skill').val("0")
        jQuery('#Prefrences').val("NA"),
        jQuery('#certificationyear').val("0")
        $("#Skill").prop("disabled", false);
        $("#Prefrences").prop("disabled", false);

        jQuery('#Program').css('border-color', '');
        jQuery('#Training').css('border-color', '');
        jQuery('#Scheme').css('border-color', '');
                
        $('#validProgram').html("");
        $("#validTraining").html("");
        $("#validScheme").html("");
}

function FillDuration(funct,control) {
    var path =  serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillDuration(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDuration('parsedatasecuredFillDuration',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Duration (in month)"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].MonthId));
             }
        }
              
}

function FillSkillSet(funct,control) {
    var path =  serverpath + "secured/SkillSet/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillSkillSet(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillSkillSet('parsedatasecuredFillSkillSet','Skill');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
        else{
            jQuery("#Skill").empty();
            var data1 = data[0];
              jQuery("#Skill").append(jQuery("<option ></option>").val("0").html("Select Skill Set"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#Skill").append(jQuery("<option></option>").val(data1[i].ESID).html(data1[i].Description));
             }
             
        }
              
}

   jQuery('#Skill').on('change', function () {
    FillJobPrefrences('parsedatasecuredFillJobPrefrences','Prefrences',jQuery('#Skill').val());
       });

function FillJobPrefrences(funct,control,ESID) {
    var path =  serverpath + "secured/jobprefrences/'" + ESID + "'/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillJobPrefrences(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJobPrefrences('parsedatasecuredFillJobPrefrences','Prefrences');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
           jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Job Prefrences"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].FunctionalArea_id).html(data1[i].FunctionalArea));
             }
        }
              
}


function FillYear(funct,control) {
    var path =  serverpath + "secured/year/0/30"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillYear(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillYear('parsedatasecuredFillYear','certificationyear');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Certfication Year"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].FinYearId).html(data1[i].Year));
             }
        }
              
}


function certification(){
    FillCertificationSecured('parsedatasecuredcertification', 'certificationtable');
    FillDuration('parsedatasecuredFillDuration', 'duration');
    FillSkillSet('parsedatasecuredFillSkillSet','Skill');
    FillYear('parsedatasecuredFillYear','certificationyear');

}