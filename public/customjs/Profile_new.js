
$("#city").focusout(function(){
    if(jQuery("#district").val()=='99999'){
        getvalidated('district','select','District First');
    }
    else {getvalidated('city','select','Tehsil');
}
});
$("#village").focusout(function(){
    if(jQuery("#city").val()=='99999'){
        getvalidated('city','select','Tehsil First');
    }
    else {getvalidated('village','select','City/Village');
}
});
function checkemail() {
    var Email;
    Email = jQuery('#Email').val();
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(Email) == false) {
        $("#Email").focus();
        jQuery('#Email').css('border-color', 'red');
        return false;
    }
    else {
        jQuery('#Email').css('border-color', '');
    }
}



function FillDistrict() {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "district/19/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {
            jQuery("#district").empty();
            jQuery("#district").append(jQuery("<option ></option>").val("99999").html("Select District"));
            for (var i = 0; i < data[0].length; i++) {
                jQuery("#district").append(jQuery("<option></option>").val(data[0][i].DistrictId).html(data[0][i].DistrictName));
            }
           
        },
        error: function (xhr) {
            if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        }
    });
}
jQuery('#district').on('change', function () {
   
    FillCityEdit(jQuery('#district').val(),'99999');
    FillVillageEdit('99999','99999');
    
});

function FillCity(district) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "city/" + district + "/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {
            var data1 = data[0];
            jQuery("#city").empty();
            jQuery("#city").append(jQuery("<option ></option>").val("99999").html("Select Tehsil"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#city").append(jQuery("<option></option>").val(data1[i].CityId).html(data1[i].CityName));
            }
        },
        error: function (xhr) {
            if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        }
    });
}
jQuery('#city').on('change', function () {
    FillVillageEdit(jQuery('#city').val(),'99999');
    
});

function FillVillage(city) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "village/" + city + "/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {
            var data1 = data[0];
            jQuery("#village").empty();
            jQuery("#village").append(jQuery("<option ></option>").val("99999").html("Select City/Village"));
            jQuery("#village").append(jQuery("<option ></option>").val("89898").html("Other"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#village").append(jQuery("<option></option>").val(data1[i].VillageId).html(data1[i].VillageName));
            }
        },
        error: function (xhr) {
            if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        }
    });
}
function FillDistrictEdit(DistrictId,StateId) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "district/"+StateId+"/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {
            jQuery("#district").empty();
            jQuery("#district").append(jQuery("<option ></option>").val("99999").html("Select District"));
            for (var i = 0; i < data[0].length; i++) {
                jQuery("#district").append(jQuery("<option></option>").val(data[0][i].DistrictId).html(data[0][i].DistrictName));
            }
            jQuery("#district").val(DistrictId)
            getvalidated('district','select','District');
            getvalidated('district','select','District First');
        },
        error: function (xhr) {
            if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        }
    });
}
function FillCityEdit(DistrictId, CityId) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "city/" + DistrictId + "/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {
            var data1 = data[0];
            jQuery("#city").empty();
            jQuery("#city").append(jQuery("<option ></option>").val("99999").html("Select Tehsil"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#city").append(jQuery("<option></option>").val(data1[i].CityId).html(data1[i].CityName));
            }
            jQuery("#city").val(CityId)

        },
        error: function (xhr) {
            if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        }
    });
}
function FillVillageEdit(CityId, VillageId) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "village/" + CityId + "/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {
            var data1 = data[0];
            jQuery("#village").empty();
            jQuery("#village").append(jQuery("<option ></option>").val("99999").html("Select City/Village"));
            jQuery("#village").append(jQuery("<option ></option>").val("89898").html("Other"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#village").append(jQuery("<option></option>").val(data1[i].VillageId).html(data1[i].VillageName));
            }
            jQuery("#village").val(VillageId)

        },
        error: function (xhr) {
            if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        }
    });
}


function CheckValidation() {
    if(isValidate){
     
    var firstname=document.getElementById('basicjobseekerfirstname');
    var lastname=document.getElementById('jobseekerLastname');
    var fathername=document.getElementById('jobseekerfathername');
    var Phone=document.getElementById('Phone');
    if ($.trim(jQuery('#basicjobseekerfirstname').val()) == '') {
        getvalidated('basicjobseekerfirstname','text','First Name');
        return false;
    }
    if(firstname.value.length < 3 && firstname.value.length >0){
        checkLength('basicjobseekerfirstname','First Name','3');
        return false;
    }
    
    if ($.trim(jQuery('#jobseekerLastname').val())== '') {
        getvalidated('jobseekerLastname','text','Last Name');
        return false;
    }
    if(lastname.value.length < 3 && lastname.value.length >0 ){
        checkLength('jobseekerLastname','Last Name','3'); 
        return false;
    }
   
    if ($.trim(jQuery('#jobseekerfathername').val()) == '') {
        getvalidated('jobseekerfathername','text','Guardian/ Father Name');
        return false; }
        if(fathername.value.length < 3 && fathername.value.length>0){
            checkLength('jobseekerfathername','Guardian/Father Name','3');
            return false;
        }
   if (jQuery('#district').val() == '0') {
    getvalidated('district','select','District');
    return false;
 }
    if (jQuery('#city').val() == '99999') {
        getvalidated('city','select','Tehsil');
        return false; }
    if (jQuery('#village').val() == '99999') {
        getvalidated('village','select','City/Village');
        return false; }
    if (jQuery('#gender').val() =='0') {
        getvalidated('gender','select','Gender');
        return false; }
    if ($.trim(jQuery('#Phone').val()) != '' && Phone.value.length!=10) {
        getvalidated('Phone','number','Mobile Number');
        return false; }
        if ($.trim(jQuery('#Phone').val()) == '') {
            getvalidated('Phone','number','Mobile Number');
            return false; }
    if ($.trim(jQuery('#Email').val()) == '') {
        getvalidated('Email','email','Email');
        return false; }
    if($.trim($("#Email").val()) != ""){
            var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
         if($("#Email").val() .match(mailformat))
            {jQuery("#Email").css('border-color', '');
            $("#validEmail").html("");
               
               
            }
            else 
            {
                jQuery("#Email").css('border-color', 'red');
                $("#validEmail").html("Please enter correct Email Id");
                return false;
            }
        
    }
    
        InsUpdRegistration();
}
else{
    InsUpdRegistration();
}
    
};

function InsUpdRegistration() {
    $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);
 
    var MasterData = {
        "p_CandidateId": sessionStorage.getItem("CandidateId"),
        "p_FirstName": jQuery("#basicjobseekerfirstname").val(),
        "p_MiddleName": jQuery("#jobseekermiddlename").val(),
        "p_LastName": jQuery("#jobseekerLastname").val(),
        "p_FatherName": jQuery("#jobseekerfathername").val(),
        "p_Gender": jQuery("#gender").val(),
        "p_VillageId":  jQuery("#village option:selected").val()!=''?jQuery("#village option:selected").val():0,
        "p_DistrictId": jQuery("#district option:selected").val(),
        "p_CityId": jQuery("#city option:selected").val()!=''?jQuery("#city option:selected").val():0,
        "p_UniqueIdentification": '',
        "p_IdentificationNumber": '',
        "p_MobileNumber": jQuery("#Phone").val(),
        "p_EmailId": jQuery("#Email").val(),
        "p_UserName": '',
        "p_Password": '',
        "p_IpAddress": sessionStorage.getItem("IpAddress"),
        "p_jobalert_email":3,
        "p_jobalert_sms": 3,
        "p_StateId":'0',
        "p_NonMP_District":'0'
    }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/registration";
    securedajaxpost(path, 'parsrdataInsUpdRegistration', 'comment', MasterData, 'control')
}

function parsrdataInsUpdRegistration(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        InsUpdRegistration();

    }
    else if (data[0][0].ReturnValue == "3") {
        //   $('#Close').click();
       ProfileStatus('parsedataProfileStatus','progressbar');
        $('#myModal1').modal('toggle');
        FillProfileDetail('parsedatasecuredFillProfileDetail');
        toastr.success("Update Successful", "", "success")
        $("#RegistrationId").text('Registration:- ' + sessionStorage.getItem('RegistrationId'));
        return true;

    }
    else if (data[0][0].ReturnValue == "4") {
        toastr.warning("Email Id already Exists ", "", "info")
        return true;

    }
    else if (data[0][0].ReturnValue == "5") {
        toastr.warning("Mobile Number already Exists ", "", "info")
        return true;

    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
}


function FillProfileDetail(funct) {
    var path = serverpath + "secured/registration/'" + sessionStorage.getItem("CandidateId") + "'/0"
    securedajaxget(path, funct, 'comment', 'control');
}

function parsedatasecuredFillProfileDetail(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillProfileDetail('parsedatasecuredFillProfileDetail');
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    
    else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else {
        var workyeartill=''
       if(data[0][0].WorkedTillinMonth=='0'){
        workyeartill='Present'
       }
       else{
        workyeartill=data[0][0].WorkedTillinYear+ ', ' +data[0][0].WorkedTillinMonth
       }
        $("#Exchange").html('<div data-toggle="tooltip" data-placement="top" title="District Employment Exchange, ' + data[0][0].DistrictName +'"  style="overflow: hidden;text-overflow: ellipsis;white-space: nowrap; width: 100%;">District Employment Exchange, ' + data[0][0].DistrictName +'</div>')

var location = data[0][0].CityName + ',  ' + data[0][0].DistrictName;


$("#Location").html("<i class='fa fa-map-marker' tital='Location' aria-hidden='true' style='padding-right: 2px;width:17px;margin-bottom: 3%;'></i>&nbsp <div data-toggle='tooltip' data-placement='top' title='" +location + ",  India ' style=' overflow: hidden;text-overflow: ellipsis;white-space: nowrap; width: 100%;   margin-top: -11%;margin-left:8%'>" + location+ ", India</div>");

        sessionStorage.setItem("Username",data[0][0].UserName)
        $("#CandidateName").html("<div data-toggle='tooltip' data-placement='top' title='" + data[0][0].CandidateName + "' style=' overflow: hidden;text-overflow: ellipsis;white-space: nowrap; width: 90%;   margin-top: -11%;margin-left:5%;'>" + data[0][0].CandidateName + "</div>");
        $("#EmpDesignation").html(data[0][0].DesignationName + ", " + data[0][0].Organisation);

        $("#MobileNo").html("<i class='fa fa-phone'title='Mobile' style='padding-right: 2px;width:17px;' aria-hidden='true'></i>&nbsp" + data[0][0].MobileNumber + "&nbsp");
        $("#EmailId").html("<i class='fa fa-envelope' title='Email' style='padding-right: 2px;width:17px;margin-bottom: 2%;' aria-hidden='true'></i>&nbsp<div data-toggle='tooltip' data-placement='top' title='" + data[0][0].EmailId + "' style=' overflow: hidden;text-overflow: ellipsis;white-space: nowrap; width: 70%;   margin-top: -9%;margin-left:9%'>" + data[0][0].EmailId + "</div>");
        $("#EmpExperience").html("<i class='fa fa-suitcase' title='Experience' aria-hidden='true'style='padding-right: 2px;width:17px;'></i>&nbsp" + data[0][0].StartedWorkinYear + ', ' + data[0][0].StartedWorkinMonth + ' - '+workyeartill);
        $("#EmpSalary").html("<i class='fa fa-credit-card' title='Registration No' aria-hidden='true' style='padding-right: 2px;width:17px;'></i>&nbsp&nbsp" + data[0][0].RegistrationId);
        $("#UpdateOn").html('<span lang="en">Updated On</span>: ' + data[0][0].UpdateOn);
       
        sessionStorage.setItem("CandidateName", data[0][0].CandidateName);
        sessionStorage.setItem("MobileNumber", data[0][0].MobileNumber);
        sessionStorage.setItem("EmailId", data[0][0].EmailId);
        sessionStorage.setItem("CandidateDistrictName", data[0][0].DistrictName);
        sessionStorage.setItem("City", data[0][0].CityName);

        Cookies.set('modaltype', "", { expires: 1, path: '/' });
        Cookies.set('RegistrationId', data[0][0].RegistrationId, { expires: 1, path: '/' });
        Cookies.set('CandidateName', data[0][0].CandidateName, { expires: 1, path: '/' });
        Cookies.set('MobileNumber', data[0][0].MobileNumber, { expires: 1, path: '/' });
        Cookies.set('EmailId', data[0][0].EmailId, { expires: 1, path: '/' });
        Cookies.set('CandidateId', data[0][0].CandidateId, { expires: 1, path: '/' });
        Cookies.set('FirstName', data[0][0].FirstName, { expires: 1, path: '/' });
      
       // ProfileStatus('parsedataProfileStatus','progressbar');
    }


}
function FillProfileDetailForm(funct) {
    var path = serverpath + "secured/registration/'" + sessionStorage.getItem("CandidateId") + "'/0"
    securedajaxget(path, funct, 'comment', 'control');
}

function parsedatasecuredFillProfileDetailForm(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillProfileDetailForm('parsedatasecuredFillProfileDetailForm');
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else {

        jQuery("#basicjobseekerfirstname").val(data[0][0].FirstName)
        jQuery("#jobseekermiddlename").val(data[0][0].MiddleName)
        jQuery("#jobseekerLastname").val(data[0][0].LastName)
        jQuery("#jobseekerfathername").val(data[0][0].GuardianFatherName)
        jQuery("#Phone").val(data[0][0].MobileNumber)
        jQuery("#Email").val(data[0][0].EmailId)
        jQuery("#gender").val(data[0][0].Gender)
        
        FillDistrictEdit(data[0][0].DistrictId,'19');
            FillCityEdit(data[0][0].DistrictId, data[0][0].CityId);
            FillVillageEdit(data[0][0].CityId, data[0][0].VillageId);
          
        sessionStorage.setItem("CandidateName", data[0][0].CandidateName);
        sessionStorage.setItem("MobileNumber", data[0][0].MobileNumber);
        sessionStorage.setItem("EmailId", data[0][0].EmailId);
       
        Cookies.set('modaltype', "", { expires: 1, path: '/' });
        Cookies.set('RegistrationId', data[0][0].RegistrationId, { expires: 1, path: '/' });
        Cookies.set('CandidateName', data[0][0].CandidateName, { expires: 1, path: '/' });
        Cookies.set('MobileNumber', data[0][0].MobileNumber, { expires: 1, path: '/' });
        Cookies.set('EmailId', data[0][0].EmailId, { expires: 1, path: '/' });
        Cookies.set('CandidateId', data[0][0].CandidateId, { expires: 1, path: '/' });
        Cookies.set('FirstName', data[0][0].FirstName, { expires: 1, path: '/' });
      
        getvalidated('Phone','number','Mobile Number');
        getvalidated('Email','email','Email Id');
  
        getvalidated('gender','select','Gender');
       
        getvalidated('basicjobseekerfirstname','text','First Name');
        checkLength('basicjobseekerfirstname','First Name','3');
        getvalidated('jobseekerLastname','text','Last Name');
        checkLength('jobseekerLastname','Last Name','3'); 
        getvalidated('jobseekerfathername','text','Father/Guardian Name');
        checkLength('jobseekerfathername','Guardian/Father Name','3');
        jQuery("#district").css('border-color', '');
            $("#validdistrict").html("");
            jQuery("#city").css('border-color', '');
            $("#validcity").html("");
            jQuery("#village").css('border-color', '');
            $("#validvillage").html("");
       
       // ProfileStatus('parsedataProfileStatus','progressbar');
    }


}

function Delete(Id, Type) {
    jQuery.ajax({
        url: serverpath + "Delete/" + Id + "/'" + sessionStorage.getItem("CandidateId") + "'/" + Type + "",
      type: "GET",
      contentType: "application/json; charset=utf-8",
      cache: false,
      dataType: "json",
      success: function (data) {
        if (Type == 'Education') {
            fillFetchEducation('parsedatasecuredfillFetchEducation', 'Education');
        }

        if (Type == 'Certification') {
            FillCertificationSecured('parsedatasecuredcertification', 'certificationtable');
         }

        if (Type == 'Identification') {
            FillIdentificationDetailSecured('parsedatasecuredidentificationdetails', 'identificationtable');
           }

        if (Type == 'Language') {
            FetchLanguage('parsedatasecuredFetchLanguage', 'languagetable');
        }

        if (Type == 'TypingSteno') {
            FetchTyping('parsedatasecuredFetchTyping', 'Typingdetail');
        }

        if (Type == 'Project') {
            FillProjectDetail('parsedatasecuredFillProjectDetail', 'Projectdetail');
        }

        if (Type == 'Employment') {
            FillEmployementSecured('parsedatasecuredemployement', 'employementdetail');
        }

        if (Type == 'ITSkill') {
            fillFetchItSkill('parsedatasecuredfillFetchItSkill', 'ItSkillTable');
        }
        if (Type == 'PhysicalDetail') {
            FetchPhysicalDetail('parsedatasecuredFetchPhysicalDetail','physicaldetail');
        }
        ProfileStatus('parsedataProfileStatus','progressbar');
    },
    error: function (xhr) {
        if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    }

    })
    
}
function InsProfileImage(){
  
    var MasterData = {
        "p_CandidateId": sessionStorage.getItem("CandidateId"),
        "p_image": sessionStorage.getItem("PicFileName"),
        
        
    }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/jobseekerimage";
    securedajaxpost(path, 'parsrdataInsUpdProfileImage', 'comment', MasterData, 'control')
}
function parsrdataInsUpdProfileImage(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        InsProfileImage();

    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else {

       
    
       FillImage('parsedatasecuredFillImage');
       sessionStorage.setItem("OpenModal",'');
        sessionStorage.setItem("PicFileName","") ;
        
      
    }

}
function DeleteImage(funct){
    var path = serverpath + "secured/deletejobseekerimage/'" + sessionStorage.getItem("CandidateId") + "'"
    securedajaxget(path, funct, 'comment', 'control');
}
function parsedatasecuredDeleteImage(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        DeleteImage('parsedatasecuredDeleteImage');
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    
    else {
        InsProfileImage();
    }

}
$("#addeducation").click(function () {
    if(sessionStorage.getItem("illterate")=="Yes"){
        toastr.warning("Please First Delete Illerate Details", "", "info")
    }
    else{
        modal('education');
    }
});
function InsProfileResume(){
    
    var MasterData = {
        "p_CandidateId": sessionStorage.getItem("CandidateId"),
        "p_Resume": sessionStorage.getItem("CVFileName"),
        
        
    }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/Resume";
    securedajaxpost(path, 'parsrdataInsUpdResume', 'comment', MasterData, 'control')
}
function parsrdataInsUpdResume(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        InsProfileResume();

    }
      else  if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else {
        
        sessionStorage.setItem("OpenModal",'');
        FillResume('parsedatasecuredFillResume');
        ProfileStatus('parsedataProfileStatus','progressbar');
       
       
    }

}
function FillResume(funct){
    var path = serverpath + "secured/Resume/'" + sessionStorage.getItem("CandidateId") + "'"
    securedajaxget(path, funct, 'comment', 'control');
}
function parsedatasecuredFillResume(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillResume('parsedatasecuredFillResume');
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else {
        sessionStorage.setItem("OpenModal",'');
        sessionStorage.setItem("CVFileName","") ;

        if(data[0]==""){
            $('#AttechResumeUpdate').text("");
            $("#Attachresumeicon").html("<i class='fa fa-plus-circle' style='padding-left: 12px;    padding-right: 12px'></i>");
           
       }
else{
   
    
        $('#AttechResumeUpdate').text(data[0][0].Resume);
        $('#filehref').attr("href","/CV/"+data[0][0].Resume)
       sessionStorage.setItem("CVFileName",data[0][0].Resume) ;
       $("#Attachresumeicon").html("<i class='fa fa-pencil' style='padding-left: 12px;    padding-right: 12px'></i>");
   
}
    
    

    }

}
function DeleteResume(funct){
    var path = serverpath + "secured/DeleteResume/'" + sessionStorage.getItem("CandidateId") + "'"
    securedajaxget(path, funct, 'comment', 'control');
}
function parsedatasecuredDeleteResume(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        DeleteResume('parsedatasecuredDeleteResume');
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else {
        if(data[0][0].ReturnValue=="1"){
        toastr.success("Delete Successful", "", "success");
        FillResume('parsedatasecuredFillResume');
        sessionStorage.setItem("CVFileName","") ;
        }
    }

}

$("#basicjobseekerfirstname").focusout(function(){
    if($.trim(this.value)==''){
        getvalidated('basicjobseekerfirstname','text','First Name')
    }
    else{
        checkLength('basicjobseekerfirstname','First Name','3')
    }
    });
    $("#jobseekerLastname").focusout(function(){
        if($.trim(this.value)==''){
            getvalidated('jobseekerLastname','text','Last Name')
        }
        else{
            checkLength('jobseekerLastname','Last Name','3')
        }
        });
        $("#jobseekerfathername").focusout(function(){
            if($.trim(this.value)==''){
                getvalidated('jobseekerfathername','text','Guardian/Father Name')
            }
            else{
                checkLength('jobseekerfathername','Guardian/Father Name','3')
            }
            });



            function olduser(){

                var dateOld = new Date(sessionStorage.getItem("RegDate"))
                var datenew = new Date('2020-07-22 00:00:00')
                
                
                     if (dateOld.getTime() < datenew.getTime()) {
                        localStorage.setItem("Page","reg");
                        window.location = '/userDetail'
                         }
                   
                }