function CheckSkill() {
    if(isValidate){
        if ($.trim(jQuery('#ITSkills').val()) == "") {
            getvalidated('ITSkills','text','Skill');
            return false; }
         else{  
              InsUpdItSkills();
             }
    }
   else{
    InsUpdItSkills();
   }

};
function InsUpdItSkills() {
    $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);
 
    var MasterData = {
        "p_ITSkillId": localStorage.getItem("ITSkillId"),
        "p_CandidateId": sessionStorage.getItem("CandidateId"),
        "p_ITSkills": jQuery("#ITSkills").val(),
        "p_Version": jQuery("#Version").val(),
        "p_LastUsed": jQuery("#lastused").val(),
        "p_ExperienceInYear": jQuery("#ExpInYear").val(),
        "p_ExperienceInMonth": jQuery("#ExpInMonth").val(),
        "p_UserId": sessionStorage.getItem("CandidateId"),
        "p_IpAddress": sessionStorage.getItem("IpAddress"),
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/itskill";
    securedajaxpost(path, 'parsrdataitskill', 'comment', MasterData, 'control')
}
function parsrdataitskill(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        InsUpdItSkills();
    }
      else  if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else if (data[0][0].ReturnValue == "0") {
        //   $('#Close').click();
       
          toastr.warning("This Detail Already Exists", "", "info")
           return true;
       }
    else if (data[0][0].ReturnValue == "1") {
     //   $('#Close').click();
       
        $('#myModal1').modal('toggle');
        fillFetchItSkill('parsedatasecuredfillFetchItSkill','ItSkillTable');
       toastr.success("Insert Successful", "", "success")
        return true;
    }
    else if (data[0][0].ReturnValue == "2") {
      //  $('#Close').click();
        $('#myModal1').modal('toggle');
          resetModeItSkill ();
          fillFetchItSkill('parsedatasecuredfillFetchItSkill','ItSkillTable');
        toastr.success("Update Successful", "", "success")
        return true;
    }
   
}

function resetModeItSkill() {

        $("#ITSkills").prop("disabled", false);
        jQuery("#ITSkills").val(""),
        jQuery("#Version").val(""),
        jQuery("#lastused").val("0"),
        jQuery("#ExpInYear").val("0"),
        jQuery("#ExpInMonth").val("0")
        
        jQuery('#ITSkills').css('border-color', '');
        $('#validITSkills').html("");
}


function fillFetchItSkill(funct,control) {
    var path =  serverpath + "secured/itskill/'" + sessionStorage.getItem("CandidateId") + "'/'" + sessionStorage.getItem("IpAddress") + "'/'" + sessionStorage.getItem("CandidateId") + "'"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredfillFetchItSkill(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        fillFetchItSkill('parsedatasecuredfillFetchItSkill','ItSkillTable');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else{
        var itupdatedon = "";
        jQuery("#" + control).empty();
        if(data[0]=="")
        {
           localStorage.setItem("ITSkillId","0")
          //  jQuery("#"+control).html("Add ItSkill");
        }
        else{
        var data1 = data[0];
        localStorage.setItem("ITSkillId",data1[0].ITSkillId)
        var appenddata="";
        var exp="";
        jQuery("#"+control).empty();
        for (var i = 0; i < data[0].length; i++) {
            if(data1[i].ExperienceInYear =="0" && data1[i].ExperienceInMonth !="0" ){
                exp=data1[i].ExperienceInMonth+" Month";
            }
           if(data1[i].ExperienceInYear !="0" && data1[i].ExperienceInMonth !="0"){
             exp=data1[i].ExperienceInYear+" Year "+data1[i].ExperienceInMonth+" Month"
            }
           if(data1[i].ExperienceInYear =="0" && data1[i].ExperienceInMonth =="0" ){
                exp="NA";
            }
            if(data1[i].ExperienceInYear !="0" && data1[i].ExperienceInMonth =="0" ){
                exp=data1[i].ExperienceInYear+" Year";
            }
           
            if(data1[i].Version =='' || data1[i].Version ==null){
                var version="-";
            }
            else{
                var version= data1[i].Version ;
            }

            if(data1[i].LastUsed =='0' || data1[i].LastUsed ==null){
                var year="-";
            }
            else{
                var year= data1[i].LastUsed ;
            }

            appenddata += "<tr><td style='word-break:break-word'>"+ data1[i].ITSkills +" </td><td>"+ version +"</td><td>"+ year +"</td><td>"+exp+"</td><td><span style='margin-left:10px;'><a href='#ProfileSkillstab' onclick=EditItSkillDetail('"+data[0][i].ITSkillId+"','"+encodeURI(data[0][i].ITSkills)+"','"+encodeURI(data[0][i].Version)+"','"+encodeURI(data[0][i].LastUsed)+"','"+encodeURI(data[0][i].ExperienceInYear)+"','"+encodeURI(data[0][i].MonthId)+"')><i class='fa fa-pencil'></i></a></span></td><td><span style='margin-left:10px;'><a href='#ProfileSkillstab' onclick=Delete("+data1[i].ITSkillId+",'ITSkill') ><i class='fa fa-times'></i></a></span></td> </tr> ";
            itupdatedon = '<span lang="en">Updated On</span> ' + data[0][i].UpdateOn;
                 
        }
       jQuery("#"+control).html(appenddata);
       
    //   ProfileStatus('parsedataProfileStatus','progressbar');
    }
}
$("#UpdateOnItskill").html(itupdatedon); 
}



function EditItSkillDetail(ITSkillId,ITSkills,Version,LastUsed,FinYearId,MonthId) {
    modal('itskills');
      
    $("#ITSkills").prop("disabled", true);
    localStorage.setItem("ITSkillId",ITSkillId);
    jQuery("#ITSkills").val(decodeURI(ITSkills))
    jQuery("#Version").val(decodeURI(Version))
    jQuery("#lastused").val(decodeURI(LastUsed))
    jQuery("#ExpInYear").val(decodeURI(FinYearId))
    jQuery("#ExpInMonth").val(decodeURI(MonthId))
}


function FillAutoCompleteSkill(funct,control) {
    var path =  serverpath + "secured/skill/'0'/'" + sessionStorage.getItem("IpAddress") + "'/0/'" + sessionStorage.getItem("CandidateId") + "'"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillAutoCompleteSkill(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillAutoCompleteSkill('parsedatasecuredFillAutoCompleteSkill','ITSkills');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else{
        if(data[0]=="")
        {
           localStorage.setItem("SkillId","0")
        }
        else{
        var data1 = data[0];
        localStorage.setItem("SkillId",data1[0].SkillId)
       
        jQuery("#"+control).empty();
        if (data.length > 0) {
            sessionStorage.setItem('SkillName', JSON.stringify(data[0]));
        }
       }
}
}

jQuery("#ITSkills").keyup(function () {
    sessionStorage.setItem('SkillId', '0');   
});

jQuery("#ITSkills").typeahead({
    source: function (query, process) {
        var data = sessionStorage.getItem('SkillName');
        skillname = [];
        map = {};
        var Skill = "";
        jQuery.each(jQuery.parseJSON(data), function (i, Skill) {
            map[Skill.SkillName] = Skill;
            skillname.push(Skill.SkillName);
        });
        process(skillname);
    },
    minLength: 3,
    updater: function (item) {
        sessionStorage.setItem('SkillId', map[item].SkillId);
        return item;
    }
});

function Filllastused(funct,control) {
    var path =  serverpath + "secured/year/0/20"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFilllastused(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        Filllastused('parsedatasecuredFilllastused');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
        else{
            jQuery("#lastused").empty();
            var data1 = data[0];
            jQuery("#lastused").append(jQuery("<option ></option>").val("0").html("<span lang='en'>Select Last Used</span>"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#lastused").append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
             }

            //  jQuery("#ExpInYear").empty();
            //  var data1 = data[0];
            //  jQuery("#ExpInYear").append(jQuery("<option ></option>").val("0").html("Select Experience (Year)"));
            //  for (var i = 0; i < data1.length; i++) {
            //      jQuery("#ExpInYear").append(jQuery("<option></option>").val(data1[i].FinYearId).html(data1[i].FinYearId));
            //   }
        }
              
}

function FillExpInMonth(funct,control) {
    var path =  serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillExpInMonth(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillExpInMonth('parsedatasecuredFillExpInMonth');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
        else{
            jQuery("#ExpInMonth").empty();
            var data1 = data[0];
            jQuery("#ExpInMonth").append(jQuery("<option ></option>").val("0").html("<span lang='en'>Select Experience (Month)</span>"));
            for (var i = 0; i < 11; i++) {
                jQuery("#ExpInMonth").append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].MonthId));
             }
             
        }
              
}


function itskillAPI(){
    fillFetchItSkill('parsedatasecuredfillFetchItSkill','ItSkillTable');
    FillAutoCompleteSkill('parsedatasecuredFillAutoCompleteSkill','ITSkills');
    Filllastused('parsedatasecuredFilllastused');
    FillExpInMonth('parsedatasecuredFillExpInMonth');

}