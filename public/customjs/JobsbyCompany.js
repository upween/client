function FillCompany() {
    var path = serverpath + "secured/companyprofile/0/0"
    securedajaxget(path,'parsedatasecuredFillCompany','comment',"control");
    }



    function parsedatasecuredFillCompany(data){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillCompany();
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
        else{
            
            var rowLength = 4
            var data1 = data[0];
            var appenddata="";
            var desig = [];
            for (var i = 0; i < data1.length; i++) {
                desig.push(encodeURI(data[0][i].CName))
            }
            for (let i = 0; i < desig.length; i+=rowLength) {
                const tds = desig.slice(i, i + rowLength).map(value => `<td><a href='#.' onclick=SetCompany('${value}')>${decodeURI(value)}</a></td>`)
                appenddata += `<tr>${tds.join("")}</tr>`
                //  appenddata += "<tr><td>" + data1[i].Designation+ "</td></tr>";  values, rowLength = 4
            }
            jQuery("#Company").html(appenddata);  
        }    
        
    }
    
    jQuery("#txtSearch").keyup(function () {
        searchTextInTable("Company");
    })

    function SetCompany(CName){ 
        sessionStorage.setItem("CName",decodeURI(CName));
        sessionStorage.setItem("Designation",'');
        window.location='/joblisting';
    }