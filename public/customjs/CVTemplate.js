
function FillCVTemplate(funct) {
  $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);
 
  var path = serverpath + "secured/JobseekerProfile/'" + sessionStorage.getItem("CandidateId") + "'"
  securedajaxget(path, funct, 'comment', 'control');
}

function parsedatasecuredFillCVTemplate(data) {
  data = JSON.parse(data)
  if (data.message == "New token generated") {
    sessionStorage.setItem("token", data.data.token);
    FillCVTemplate('parsedatasecuredFillCVTemplate');
  }
  else if (data.status == 401) {
    toastr.warning("Unauthorized", "", "info")
    return true;
  }
  else {
   

  if(data[0][0].Marital_Status =='0' || data[0][0].Marital_Status ==null){
    var marital_Status="-";
}
else{
    var marital_Status= data[0][0].Marital_Status ;   
}
if(data[0][0].Area_Name =='0' || data[0][0].Area_Name ==null){
  var area_Name="-";
}
else{
  var area_Name= data[0][0].Area_Name ;
}
if(data[0][0].AreaOfInterest =='0' || data[0][0].AreaOfInterest ==null){
var areaOfInterest="-";
 }
 else{
var areaOfInterest= data[0][0].AreaOfInterest ;
 }
if(data[0][0].Eye_sight ==null && data[0][0].Height ==null&& data[0][0].Weight ==null&& data[0][0].Chest ==null){
  $("#OtherDetailSection").hide();
}
else{
  $("#OtherDetailSection").show();
}
 if(data[0][0].Eye_sight =='0' || data[0][0].Eye_sight ==null){
  var eye_sight="-";
}
else{
  var eye_sight= data[0][0].Eye_sight ;   
}

if(data[0][0].Height =='' || data[0][0].Height ==null){
  var height="0";
}
else{
  var height= data[0][0].Height ;   
}

if(data[0][0].Weight =='' || data[0][0].Weight ==null){
  var weight="0";
}
else{
  var weight= data[0][0].Weight ;   
}

if(data[0][0].Chest =='' || data[0][0].Chest ==null){
  var chest="0";
}
else{
  var chest= data[0][0].Chest ;   
}





    var maindetails = "";
    var personal = "";
    var contact="";
    var otherdetail="";
    var experience="";
    var education="";
    var jobpreference="";
    var typing="";
    var identification="";
    var certification="";
    var language="";
    var designationname=""
    if(data[0][0].DesignationName==null || data[0][0].DesignationName==undefined ||data[0][0].DesignationName=='' || data[0][0].Organisation==null || data[0][0].Organisation==undefined ||data[0][0].Organisation=='')
    {
      designationname=''
    }
    else {
      designationname=data[0][0].DesignationName +' at '+data[0][0].Organisation
    }
    if(data[0][0].DifferentlyAbled=='Handicap'){
      var DifferentlyAbled='Yes';
  }
  else{
      var DifferentlyAbled='No';
  }
    maindetails += "<div id='headshot' class='quickFade'><img alt='"+data[0][0].CandidateName+"' id='CVImage' /></div>" +
      "<div id='name'><h1 class='quickFade delayTwo' id='NameCV'>" + data[0][0].CandidateName + "</h1><h2 class='quickFade delayThree' id='OrganisationCV' >" + designationname + "</h2></div><div id='contactDetails' class='quickFade delayFour'>" +
      "<ul> <li id='emailCV'>" + data[0][0].EmailId + "</li><li id='phoneNoCV'>" + data[0][0].MobileNumber + "</li></ul></div><div class='clear'></div>";
    personal += "<div class='col-md-3 col-sm-2'>Name:</div><div class='col-md-3 col-sm-4' id='cadName'>" + data[0][0].CandidateName + "</div>"+
    "<div class='col-md-3 col-sm-2'>Date-of Birth:</div><div class='col-md-3 col-sm-4' id='dateofbirth'>"+data[0][0].DateOfBirth+"</div>"+
    "<div class='clearfix' style='padding: 25px 0px 0px 0px;'></div><div class='col-md-3 col-sm-2'>Father's/Husband  Name:</div><div class='col-md-3 col-sm-4' style='word-break:break-word'>"+data[0][0].GuardianFatherName+"</div>"+
    "<div class='col-md-3 col-sm-2'>Gender:</div><div class='col-md-3 col-sm-4'>"+data[0][0].Gender+"</div><div class='clearfix' style='padding: 25px 0px 0px 0px;'></div>"+
    "<div class='col-md-3 col-sm-2'>Marital Status:</div><div class='col-md-3 col-sm-4'>"+marital_Status+"</div>"+
    "<div class='col-md-3 col-sm-2'>Category:</div><div class='col-md-3 col-sm-4'>"+data[0][0].CategoryName+"</div><div class='clearfix' style='padding: 25px 0px 0px 0px;'></div>"+
    "<div class='col-md-3 col-sm-2'>Job Status:</div><div class='col-md-3 col-sm-4'>	"+data[0][0].Employment_Status+"</div>"+
    "<div class='col-md-3 col-sm-2'>Domicile:</div><div class='col-md-3 col-sm-4'>"+data[0][0].Resident_name+"</div><div class='clearfix' style='padding: 25px 0px 0px 0px;'></div>"+
    "<div class='col-md-3 col-sm-2'>Religion:</div><div class='col-md-3 col-sm-4' >"+data[0][0].Religion_Name+"</div>"+
    "<div class='col-md-3 col-sm-2'>Physical Ability:</div><div class='col-md-3 col-sm-4' >"+DifferentlyAbled+"</div>"+
    "<div class='col-md-3 col-sm-2'>Green Card:</div><div class='col-md-3 col-sm-4'>"+data[0][0].GreenCard+"</div><div class='clearfix' style='padding: 25px 0px 0px 0px;'></div>"+
   
    "<div class='col-md-3 col-sm-2'>Area Of Intrest:</div><div class='col-md-3 col-sm-4' >"+areaOfInterest+"</div><div class='clearfix' style='padding: 25px 0px 0px 0px;'></div>";
    contact+="<div class='col-md-3 col-sm-'>Area:</div><div class='col-md-3 col-sm-4'>"+area_Name+"</div>"+
    "<div class='col-md-3 col-sm-2'>District:</div><div class='col-md-3 col-sm-4'style='word-break:break-word'>"+data[0][0].DistrictName+"</div>"+
    "<div class='col-md-3 col-sm-2'>Address:</div><div class='col-md-3 col-sm-4'style='word-break:break-word' >"+data[0][0].PermanentAddress+"</div><div class='clearfix' style='padding: 25px 0px 0px 0px;'></div>"+
    "<div class='col-md-3 col-sm-2'>City:</div><div class='col-md-3 col-sm-4'>"+data[0][0].CityName+"</div><div class='clearfix' style='padding: 25px 0px 0px 0px;'></div>"+
    "<div class='col-md-3 col-sm-2'>Pin Code:</div><div class='col-md-3 col-sm-4'>"+data[0][0].PinCode+"</div>"+
    "<div class='col-md-3 col-sm-2'>Mobile No:</div><div class='col-md-3 col-sm-4'>"+data[0][0].MobileNumber+"</div><div class='clearfix' style='padding: 25px 0px 0px 0px;'></div>"+
    "<div class='col-md-3 col-sm-2'>Email:</div><div class='col-md-3 col-sm-4'>"+data[0][0].EmailId+"</div><div class='clearfix' style='padding: 25px 0px 0px 0px;'></div>";
    otherdetail+="	<div class='col-md-3 col-sm-2'>Eye Sight:</div><div class='col-md-3 col-sm-4'>"+eye_sight+"</div>"+
    "<div class='col-md-3 col-sm-2'>Height:</div><div class='col-md-3 col-sm-4'>"+height+" cm</div><div class='clearfix' style='padding: 25px 0px 0px 0px;'></div>"+
    "<div class='col-md-3 col-sm-2'>Weight:</div><div class='col-md-3 col-sm-4'>"+weight+" cm</div>"+
    "<div class='col-md-3 col-sm-2'>Chest:</div><div class='col-md-3 col-sm-4'>"+chest+" cm</div><div class='clearfix' style='padding: 25px 0px 0px 0px;'></div>";
    if(data[1]==""){
      $("#expSection").hide()
      experience+="<tr><td><span style='color:red'>Not Filled.</span></td></tr>";
    }else{

    for (var i = 0; i < data[1].length; i++) {

      if (data[1][i].IsCurrentCompany == '1') {
        experience+="<tr><td  scope='col' style='word-break:break-word'>"+data[1][i].DesignationName+"</td><td scope='col' style='word-break:break-word'>"+data[1][i].Organisation+"</td><td scope='col' style='word-break:break-word'>" + data[1][i].StartedWorkinYear + ", "+data[1][i].StartedWorkinMonth+" To Present</td><td  scope='col' style='word-break:break-word'>"+data[1][i].SalaryinLacs+" Lac, "+data[1][i].SalaryinThousand+"</td><tr>";
     }
    
    if (data[1][i].IsCurrentCompany == '2') {
      experience+="<tr><td  scope='col' style='word-break:break-word'>"+data[1][i].DesignationName+"</td><td scope='col' style='word-break:break-word'>"+data[1][i].Organisation+"</td><td scope='col' style='word-break:break-word'>" + data[1][i].StartedWorkinYear + ", "+data[1][i].StartedWorkinMonth+" To " + data[1][i].WorkedTillinYear+", "+data[1][i].WorkedTillinMonth+"</td><td  scope='col' style='word-break:break-word'>"+data[1][i].SalaryinLacs+" Lac, "+data[1][i].SalaryinThousand+"</td><tr>";
    }
    }
  }
  if(data[2]==""){
    $("#eduSection").hide()
    education+="<tr><td><span style='color:red'>Not Filled.</span></td></tr>";
  }else{
    for (var i = 0; i < data[2].length; i++) {
      
if(data[2][i].Sub =='0' || data[2][i].Sub ==null || data[2][i].Sub ==''){
  var sub="-";
}
else{
  var sub= data[2][i].Sub ;   
}

      education+="<tr><td  scope='col' style='word-break:break-word'>"+data[2][i].Qualif_name+"</td><td scope='col' style='word-break:break-word'>"+data[2][i].Sub_Group_Name+"</td><td scope='col' style='word-break:break-word'>" + sub +"</td><td  scope='col' style='word-break:break-word'>"+data[2][i].Percentage+"%</td><td  scope='col' style='word-break:break-word'>"+data[2][i].Year+"</td><td  scope='col' style='word-break:break-word'>"+data[2][i].UniversityName+"</td></tr>";
     }
    }
   
    if(data[4]==""){
      $("#typingSection").hide()
      typing+="<tr><td><span style='color:red'>Not Filled.</span></td></tr>";
    }else{
    
     for (var i = 0; i < data[4].length; i++) {
      if(data[4][i].TS_Institute_Name =='0' || data[4][i].TS_Institute_Name ==null || data[4][i].TS_Institute_Name ==''){
        var tS_Institute_Name="-";
      }
      else{
        var tS_Institute_Name= data[4][i].TS_Institute_Name ;   
      }
      if(data[4][i].Year =='0' || data[4][i].Year ==null || data[4][i].Year ==''){
  var year="-";
}
else{
  var year= data[4][i].Year ;   
}

if(data[4][i].Speed =='' || data[4][i].Speed =='0' || data[4][i].Speed ==null || data[4][i].Speed ==''){
  var speed="-";
}
else{
  var speed= data[4][i].Speed ;   
}
      typing+="<tr><td  scope='col' style='word-break:break-word'>"+data[4][i].Lang_name+"</td><td scope='col' style='word-break:break-word'>"+data[4][i].Type+"</td><td scope='col' style='word-break:break-word;width:28%'>" + tS_Institute_Name +"</td><td  scope='col' style='word-break:break-word'>"+year+"</td><td  scope='col' style='word-break:break-word'>"+speed+"</td></tr>";
     }
    }
    if(data[5]==""){
      $("#identificationSection").hide()
      identification+="<tr><td><span style='color:red'>Not Filled.</span></td></tr>";
    }else{
     for (var i = 0; i < data[5].length; i++) {
      if(data[5][i].Date =='' || data[5][i].Date =='0' || data[5][i].Date ==null){
        var date="-";
      }
      else{
        var date= data[5][i].Date ;   
      }
      if(data[5][i].Upto =='' || data[5][i].Upto =='0' || data[5][i].Upto ==null){
        var upto="-";
      }
      else{
        var upto= data[5][i].Upto ;   
      }
      if(data[5][i].IssuingAuthority =='' || data[5][i].IssuingAuthority =='0' || data[5][i].IssuingAuthority ==null){
        var issuingAuthority="-";
      }
      else{
        var issuingAuthority= data[5][i].IssuingAuthority ;   
      }
      identification+="<tr><td  scope='col' style='word-break:break-word'>"+data[5][i].IdentificationType+"</td><td scope='col' style='word-break:break-word'>"+data[5][i].IdentificationNumber+"</td><td scope='col' style='word-break:break-word'>" + data[5][i].LicenseType +"</td><td  scope='col' style='word-break:break-word'>"+date+"</td><td  scope='col' style='word-break:break-word'>"+upto+"</td><td  scope='col'style='word-break:break-word'>"+issuingAuthority+"</td></tr>";
     }
    }
    if(data[3]==""){
      $("#certificationSection").hide()
      certification+="<tr><td><span style='color:red'>Not Filled.</span></td></tr>";
    }else{
     for (var i = 0; i < data[3].length; i++) {
      if(data[3][i].Duration =='' || data[3][i].Duration =='0' || data[3][i].Duration ==null){
        var duration="-";
      }
      else{
        var duration= data[3][i].Duration +"Month";   
      }

      if(data[3][i].Description =='' || data[3][i].Description =='0' || data[3][i].Description ==null){
        var description="-";
      }
      else{
        var description= data[3][i].Description ;   
      }

      if(data[3][i].Year =='' || data[3][i].Year =='0' || data[3][i].Year ==null){
        var year="-";
      }
      else{
        var year= data[3][i].Year ;   
      }
      certification+="<tr><td  scope='col' style='word-break:break-word'>"+data[3][i].TrainingProgramName+"</td><td scope='col' style='word-break:break-word'>"+data[3][i].TrainingCenter+"</td><td scope='col' style='word-break:break-word'>" + duration +" </td><td  scope='col' style='word-break:break-word'>"+description+"</td><td  scope='col' style='word-break:break-word'>"+year+"</td></tr>";
     }
    }
    if(data[6]==""){
      $("#languageSection").hide()
      language+="<tr><td><span style='color:red'>Not Filled.</span></td></tr>";
    }else{
     for (var i = 0; i < data[6].length; i++) {
      
      language+="<tr><td  scope='col' style='word-break:break-word'>"+data[6][i].Lang_name+"</td><td scope='col' style='word-break:break-word'>"+data[6][i].Read_L+"</td><td scope='col' style='word-break:break-word'>" + data[6][i].Write_L +"</td><td  scope='col' style='word-break:break-word'>"+data[6][i].Speak+"</td></tr>";
     }
    }
    if(data[0][0].Industry==null||data[0][0].Industry==''){
      $("#prefferedSection").hide()
      jobpreference+="<tr><td><span style='color:red'>Not Filled.</span></td></tr>";
    }
    else{
      jobpreference+="<tr><td  scope='col'>1</td><td scope='col' style='word-break:break-word'>"+data[0][0].Industry+"</td><td scope='col' style='word-break:break-word'>" + data[0][0].Title +"</td></tr>";
    }
    $("#mainDetails").html(maindetails);
    $("#RegistrationNo").html(data[0][0].RegistrationId);
    $("#ProfileSummaryCV").html(data[0][0].ProfileSummary);
    $("#PersonalDetailCV").html(personal);
    $("#ContactDetailCV").html(contact);
    $("#OtherDetailCV").html(otherdetail);
    $("#ExperienceCV").html(experience);
    $("#EducationCV").html(education);
    $("#JobPreferenceCV").html(jobpreference);
    $("#CertificationCV").html(certification);
    $("#IdentificationCV").html(identification);
    $("#TypingCV").html(typing);
    $("#LanguageCV").html(language);
    FillImageCV('parsedatasecuredFillImageCV');
  }

}

function FillImageCV(funct){
  var path = serverpath + "secured/jobseekerimage/'" + sessionStorage.getItem("CandidateId") + "'"
  securedajaxget(path, funct, 'comment', 'control');
}
function parsedatasecuredFillImageCV(data) {
  data = JSON.parse(data)
  if (data.message == "New token generated") {
      sessionStorage.setItem("token", data.data.token);
      FillImageCV('parsedatasecuredFillImageCV');
  }
  else if (data.status == 401) {
      toastr.warning("Unauthorized", "", "info")
      return true;
  }
  
  else {
     if(data[0]==""){
     
      $('#CVImage').attr('src',"images/candidates/01.jpg");
      
     }
     else{
    
      $('#CVImage').attr('src',"Pics/"+data[0][0].image);
  
}
  }

}


// var doc = new jsPDF();
// var specialElementHandlers = {
//   "#editor": function (element, renderer) {
//     return true;
//   }
// };

// $("#cmd").click(function () {
//   doc.fromHTML($("#cv").html(), 15, 15, {
//     width: 170,
//     elementHandlers: specialElementHandlers
//   });
//   doc.save("sample-file.pdf");
// });




