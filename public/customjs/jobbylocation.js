

function FillLocation() {
    var path = serverpath + "secured/district/0/0/0/0"
    securedajaxget(path,'parsedatasecuredFillLocation','comment',"control");
    }


    function parsedatasecuredFillLocation(data){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillLocation();
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
        else{
            
            var rowLength = 4
            var data1 = data[0];
            var appenddata="";
            var desig = [];
            for (var i = 0; i < data1.length; i++) {
                desig.push(encodeURI(data[0][i].DistrictName))
            }
            for (let i = 0; i < desig.length; i+=rowLength) {
                const tds = desig.slice(i, i + rowLength).map(value => `<td><a href='#.' onclick=SetLocation('${value}')>${decodeURI(value)}</a></td>`)
                appenddata += `<tr>${tds.join("")}</tr>`
                //  appenddata += "<tr><td>" + data1[i].Designation+ "</td></tr>";  values, rowLength = 4
            }
            jQuery("#Location").html(appenddata);  
        }    
        
    }
    
    jQuery("#txtSearch").keyup(function () {
        searchTextInTable("Location");
    })

    

function SetLocation(Location){ 
    sessionStorage.setItem("Location",decodeURI(Location));
    sessionStorage.setItem("Designation",'');
    sessionStorage.setItem("Qualification",'');
    sessionStorage.setItem("Sector",'');
    window.location='/joblisting';
}