function FillSecuredDesignation(funct, control) {
    var path = serverpath + "secured/designation/0/0/0/0"
    securedajaxget(path, funct, 'comment', control);
}
function parsedatasecureddesignation(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillSecuredDesignation('parsedatasecureddesignation', 'jobdesignation');
    }
    else {
        var data1 = data[0];
        jQuery("#" + control).empty();
        for (var i = 0; i < 6; i++) {
            jQuery("#" + control).append("<li id='" + data1[i].DesignationId + "'><a href='/JobListing'>" + data1[i].DesignationName + "</a><li>");
        }
    }
}
function FillSecuredDepartment(funct, control) {
    var path = serverpath + "secured/department/0/0/0/0"
    securedajaxget(path, funct, 'comment', control);
}
function parsedatasecureddepartment(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillSecuredDepartment('parsedatasecureddepartment', 'jobdepartment');
    }
    else {
        var data1 = data[0];
        jQuery("#" + control).empty();
        for (var i = 0; i < 6; i++) {
            jQuery("#" + control).append("<li id='" + data1[i].DepartmentId + "'><a href='/JobListing'>" + data1[i].DepartmentName + "</a><li>");
        }
    }
}
function FillSecuredState(funct, control) {
    var path = serverpath + "secured/state/0/0/0/0"
    securedajaxget(path, funct, 'comment', control);
}
function parsedatasecuredstate(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillSecuredState('parsedatasecuredstate', 'jobstate');
    }
    else {
        var data1 = data[0];
        jQuery("#" + control).empty();
        for (var i = 0; i < 6; i++) {
            jQuery("#" + control).append("<li id='" + data1[i].StateId + "'><a href='/JobListing'>" + data1[i].StateName + "</a><li>");
        }
    }
}
function FillSecuredCategory(funct, control) {
    var path = serverpath + "secured/category/0/0/0/0"
    securedajaxget(path, funct, 'comment', control);
}
function parsedatacategorysecured(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillSecuredCategory('parsedatacategorysecured', 'jobcategory');
    }
    else {
        var data1 = data[0];
        jQuery("#" + control).empty();
        for (var i = 0; i < 6; i++) {
            jQuery("#" + control).append("<li id='" + data1[i].CategoryId + "'><a href='/JobListing'>" + data1[i].CategoryName + "</a><li>");
        }
    }
}
function FillSecuredcity(funct, control) {
    var path = serverpath + "secured/district/0/0/0/0"
    securedajaxget(path, funct, 'comment', control);
}
function parsedatacitysecured(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillSecuredcity('parsedatacitysecured', 'jobdepartment');
    }
    else {
        var data1 = data[0];
        jQuery("#" + control).empty();
        for (var i = 0; i < 6; i++) {
            jQuery("#" + control).append("<li id='" + data1[i].DistrictId + "'><a href='/JobListing'>" + data1[i].DistrictName + "</a><li>");
        }
    }
}
function FillSkillSecured(funct, control) {
    var path = serverpath + "secured/skill/0/0/0/0"
    securedajaxget(path, funct, 'comment', control);
}
function parsedatasecuredskill(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillSkillSecured('parsedatasecuredskill', 'jobskill');
    }
    else {
        var data1 = data[0];
        jQuery("#" + control).empty();
        for (var i = 0; i < 6; i++) {
            jQuery("#" + control).append("<li id='" + data1[i].SkillId + "'><a href='/JobListing'>" + data1[i].SkillName + "</a><li>");
        }
    }
}
