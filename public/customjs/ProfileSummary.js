function CheckProfileSummary() {
    if(isValidate){
        var str= jQuery('#profilesummarytext').val()
        if ( $.trim(str)== "") {     
           getvalidated('profilesummarytext','text','Profile Summary');
           return false;
        }
        else{    InsProfileSummary()
        }
    }
    else{    InsProfileSummary()
    }
   
   

};

function InsProfileSummary() {
    $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);


    var MasterData = {
        "p_ProfileSummaryId":localStorage.getItem("ProfileSummaryId"),
        "p_CandidateId": sessionStorage.getItem("CandidateId"),
        "p_ProfileSummary": $.trim(jQuery("#profilesummarytext").val()),
        "p_IpAddress": sessionStorage.getItem("IpAddress"),
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/profileSummary";
    securedajaxpost(path, 'parsrdataprofilesummary', 'comment', MasterData, 'control')
}

function parsrdataprofilesummary(data) {
      data = JSON.parse(data)
      if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        InsProfileSummary();
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
       if (data[0][0].ReturnValue == "1") {
      //  $('#Close').click();
        ProfileStatus('parsedataProfileStatus','progressbar');
        $('#myModal1').modal('toggle');
        FetchProfileSummary('parsedatasecuredFetchProfileSummary','Summarydetail');
        toastr.success("Insert Successful", "", "success")
        return true;
    }
     else if (data[0][0].ReturnValue == "2") {
      //  $('#Close').click();
        $('#myModal1').modal('toggle');
        FetchProfileSummary('parsedatasecuredFetchProfileSummary','Summarydetail');
        // resetProfileSummary();
        toastr.success("Update Successful", "", "success")
        return true;
    }
   

}

function resetProfileSummary() {
    jQuery("#profilesummarytext").val("")
     }


 function FetchProfileSummary(funct,control) {
        var path = serverpath + "secured/profileSummary/'"+sessionStorage.getItem("CandidateId")+"'/'"+sessionStorage.getItem("IpAddress")+"'"
        securedajaxget(path,funct,'comment',control);
    }

    function parsedatasecuredFetchProfileSummary(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FetchProfileSummary('parsedatasecuredFetchProfileSummary','Summarydetail');
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
           else if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        
        else{
            if(data[0]=="")
            {
               localStorage.setItem("ProfileSummaryId","0")
               jQuery("#"+control).html(" <span lang='en'>Add Profile Summary</span>");
            }
            else{
            var data1 = data[0];
            localStorage.setItem("ProfileSummaryId",data1[0].ProfileSummaryId)
            var appenddata="";
            var profilehead="";
            jQuery("#"+control).empty();
            appenddata+=""+data1[0].ProfileSummary+"";
            jQuery("#"+control).html(appenddata);
            jQuery("#profilesummarytext").val(appenddata);
           profilehead+=" <span lang='en'>Updated On</span> "+data[0][0].UpdateOn+"";
           jQuery("#ProfileummaryUpdateON1").html(profilehead);
           getvalidated('profilesummarytext','text','Profile Summary');
       // ProfileStatus('parsedataProfileStatus','progressbar');
        }
    }
}
 


