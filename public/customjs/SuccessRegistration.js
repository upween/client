﻿var emailcheck='N'
function ValidateLogin() {
  var MasterData = {
        
    "p_UserId":sessionStorage.getItem("Username"),
    "p_Password": sessionStorage.getItem("Password")
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "validatelogin";
ajaxpost(path, 'parsedatalogin', 'comment', MasterData, 'control');

}   
function parsedatalogin(data){   
  data = JSON.parse(data)
  if (data.result.userDetails){
    if (data.result.userDetails.ReturnValue == "1" ||data.result.userDetails.ReturnValue == "2") {
    Cookies.set('RegistrationId', data.result.userDetails.RegistrationId, { expires: 1, path: '/' });
      sessionStorage.setItem("token", data.result.token);
      sessionStorage.setItem("refreshToken", data.result.refreshToken)
      sessionStorage.setItem("RegistrationId", data.result.userDetails.RegistrationId);
      sessionStorage.setItem("CandidateId", data.result.userDetails.CandidateId);
      sessionStorage.setItem("CandidateName", data.result.userDetails.CandidateName);
      sessionStorage.setItem("MobileNumber", data.result.userDetails.MobileNumber);
      sessionStorage.setItem("EmailId", data.result.userDetails.EmailId);
      sessionStorage.setItem("RegDate", data.result.userDetails.RegDate);

      Cookies.set('modaltype', "", { expires: 1, path: '/' });
      Cookies.set('RegistrationId', data.result.userDetails.RegistrationId, { expires: 1, path: '/' });
      Cookies.set('CandidateName', data.result.userDetails.CandidateName, { expires: 1, path: '/' });
      Cookies.set('MobileNumber', data.result.userDetails.MobileNumber, { expires: 1, path: '/' });
      Cookies.set('EmailId', data.result.userDetails.EmailId, { expires: 1, path: '/' });
      Cookies.set('CandidateId', data.result.userDetails.CandidateId, { expires: 1, path: '/' });
      Cookies.set('FirstName', data.result.userDetails.FirstName, { expires: 1, path: '/' });
      sessionStorage.setItem("CandidateDistrictName", data.result.userDetails.DistrictName);
      // window.location = '/Profilereg'
      window.location = '/RegistrationSuccess'

  }
  }
  
  else{
    toastr.warning(data, "", "info")
  }
}
function sendemailjs(OTP,emaildisplay){
  var sub = "Rojgar Verification OTP";
  var body = `Dear User,
  </div>
  <div style='font-size:18px;'><br>This is a system-generated mail that is being sent out to you with regard to your account at MP Rojgar Portal. </div>
  <div style='font-size:20px;'>
  <br>
  Please use  : ${OTP} as the one-time password for secure login in your account email id (${emaildisplay})This password is valid for 10 minutes.`;
  sentmailglobal(emaildisplay,body,sub) ;


}
function sendmsg(Msg,Mobile){
  var body ="Use  "+Msg+" as OTP. OTP valid for 10 mins. Don't share with anyone...MY MP Rojgar"
    
  sentSmsGlobal(Mobile,body) ;
  }
function InsUpdotp(Type,Id,verificationtype) {

  sessionStorage.verifytype=verificationtype;
var  generateOTPmaster =$("#checkemailotp").val(); 


   var MasterData = {
           "p_CandidateId" : Id,
           "p_Flag" : verificationtype,
           "p_FlagValue" :generateOTPmaster,
          "p_Type":Type
         
   }
   MasterData = JSON.stringify(MasterData)
   var path = serverpath + "jobseeker_verification";
   ajaxpost(path, 'parsrdataemailverification', 'comment', MasterData, 'control')
}
function parsrdataemailverification(data) {
   data = JSON.parse(data)

   if (data[0][0].ReturnValue == "4") {
    emailcheck="Y"
  //  InsUpdotp1('Match',sessionStorage.CandidateId,'mobile');

 
   }
   else if (data[0][0].ReturnValue == "5") {
       
       toastr.warning("Please Enter Correct E-Mail OTP", "", "info")
       return false;
   }
}

  $("#Next").click(function(){
    if (jQuery('#checkemailotp').val() == '') {
      toastr.warning("Please Enter E-Mail OTP", "", "info")
       return false;
  }
  if (jQuery('#checkmobileotp').val() == '') {
    toastr.warning("Please Enter Mobile OTP", "", "info")
       return false;
}
else {
  ValidateLogin();

  // if(emailcheck=='Y'){
  //   InsUpdotp1('Match',sessionStorage.CandidateId,'mobile');
  // }
  // else{
  //  InsUpdotp('Match',sessionStorage.CandidateId,'email');
  // }
  }
 });
 function InsUpdotp1(Type,Id,verificationtype) {

  sessionStorage.verifytype=verificationtype;
  var  generateOTPmaster =$("#checkmobileotp").val();
  var MasterData = {
           "p_CandidateId" : Id,
           "p_Flag" : verificationtype,
           "p_FlagValue" :generateOTPmaster,
          "p_Type":Type
         
   }
   MasterData = JSON.stringify(MasterData)
   var path = serverpath + "jobseeker_verification";
   ajaxpost(path, 'parsrdataemailverification1', 'comment', MasterData, 'control')
}
function parsrdataemailverification1(data) {
   data = JSON.parse(data)
  if (data[0][0].ReturnValue == "4") {
    toastr.success("OTP Verified", "", "success");
    ValidateLogin();
   
 
   }
   else if (data[0][0].ReturnValue == "5") {
       toastr.warning("Please Enter Correct Mobile OTP", "", "info")
       return true;
   }
}