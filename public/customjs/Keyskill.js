function CheckKeySkill() {
    if(isValidate){

    
    if ($("#SkillName").val()== '') {
        $(".demo-default").css('border-color', 'red');
        $("#validSkillName").html("Please select Key Skill");
        return false;
    }
    else {
        $("#SkillName").css('border-color', '');
        $("#validSkillName").html("");

        DeleteKeySkill();
    }
}
else{
    DeleteKeySkill();
}
}
function InsUpdKeySkill() {
    $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);
 
    var MasterData = {
        "p_SkillId": localStorage.getItem("SkillId"),
        "p_Skill": $("#SkillName").val(),
        "p_CandidateId": sessionStorage.getItem("CandidateId"),
        "p_IpAddress": sessionStorage.getItem("IpAddress"),
        "p_UserId": sessionStorage.getItem("CandidateId")
    }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/keyskill";
    securedajaxpost(path, 'parsrdatakeyskill', 'comment', MasterData, 'control')
}
function parsrdatakeyskill(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        InsUpdKeySkill();
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else if (data[0][0].ReturnValue == "1") {
        ProfileStatus('parsedataProfileStatus','progressbar');
        toastr.success("Submit Successfully", "", "success")
        $('#myModal1').modal('toggle');
        FillKeySkillSecured('parsedatasecuredkeyskill', 'keyskilldetail');

        return true;
    }


}
function FillKeySkillSecured(funct, control) {
    var path = serverpath + "secured/keyskill/'" + sessionStorage.getItem("CandidateId") + "'/'" + sessionStorage.getItem("IpAddress") + "'/'" + sessionStorage.getItem("CandidateId") + "'"
    securedajaxget(path, funct, 'comment', control);
}
function parsedatasecuredkeyskill(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillKeySkillSecured('parsedatasecuredkeyskill', 'keyskilldetail');
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else {
        if (data[0] == "") {
            localStorage.setItem("SkillId", "0");
            jQuery("#" + control).html("<span lang='en'>Add Key Skill</span>");
        }
        else {
            var data1 = data[0];

            var appenddata = "";
            var SkillId="";

            jQuery("#" + control).empty();

            for (var i = 0; i < data1.length; i++) {
                var SkillName = data1[i].Skill.split(",");
               
                
                for (var i = 0; i < SkillName.length; i++) {
                    appenddata += "<div class='chip'>" + SkillName[i] + "</div>";
                    
                }
                jQuery("#keyskillupdateon").html("<span lang='en'>Updated On</span> "+data1[0].UpdateOn);
            
            }
            jQuery("#" + control).html(appenddata);
            
           
            // $('#SkillName').height("30");
            
      //  ProfileStatus('parsedataProfileStatus','progressbar');
        }
    }        
}
function DeleteKeySkill() {
    var path = serverpath + "secured/deletekeyskill/'" + sessionStorage.getItem("CandidateId") + "'"
    securedajaxget(path, 'parsedatasecureddeletekeyskill', 'comment', 'control');
}
function parsedatasecureddeletekeyskill(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        DeleteKeySkill();
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
      else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else {
        InsUpdKeySkill();
    }
}

