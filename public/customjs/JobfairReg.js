function InsUpdJobFairReg() {

    var Mail;
    Mail = jQuery('#jfEmailId').val();

    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if (reg.test(Mail) == false) {
        $(window).scrollTop(0);
        toastr.warning("Please Enter Correct E-MailId", "", "info");
        jQuery('#jfEmailId').css('border-color', 'red'); 
        return true; 
    }
    else { 
        jQuery('#jfEmailId').css('border-color', '');
        
     }

   

    var Fields = [];  
    $.each($("input[name='checkbox']:checked"), function(){ 
       Fields.push($(this).val());
    //   Fields += this.value + ","
    });
  

    var MasterData = {
        "p_JobFairRegistrationId": '0',
        "p_FirstName": jQuery("#JFfirstname").val(),
        "p_MiddleName": jQuery("#JFmiddlename").val(),
        "p_LastName": jQuery("#jflastname").val(),
        "p_Age": jQuery("#jfage").val(),
        "p_PhoneNumber": jQuery("#jfPhone").val(),
        "p_EmailId": jQuery("#jfEmailId").val(),
        "p_Qualification": jQuery("#jfQualification option:selected").val(),
        "p_Fresher": jQuery("#jfFresher option:selected").text(),
        "p_CurrentOrganisation": jQuery("#jsCurrentOrganisation").val(),
        "p_CurrentDesignation": jQuery("#jfCurrentDesignation").val(),
        "p_Role": jQuery("#jfrole").val(),
        "p_Salary": jQuery("#jfCurrentSalary").val(),
        "p_JobLocation": jQuery("#jfCurrentJobLocation").val(),
        "p_Expectation": jQuery("#jfExpectations").val(),
        "p_Resident": jQuery("#jfDomicile").val(),
        "p_JobFairId": jQuery("#jfjobfair option:selected").text(),
        "p_Fields": Fields.join(", "),
    }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "JobFairReg";
    ajaxpost(path, 'parsrdataJobFairReg', 'comment', MasterData, 'control')
}
function parsrdataJobFairReg(data) {
    data = JSON.parse(data)

    if (data[0][0].ReturnValue == "0") {
        toastr.warning("You Are Already Registered", "", "info")
        return true;
    }

   else if (data[0][0].ReturnValue == "1") {
        toastr.warning("You Are Already Registered", "", "info")
        return true;
    }
    else if (data[0][0].ReturnValue == "2") {

    //    sessionStorage.setItem("JobFairRegistrationId", data[0][0].JobFairRegistrationId);
         resetJobfairReg();
    //    window.location = '/SuccessRegistration'
    }
    else if (data[0][0].ReturnValue == "3") {
        resetJobfairReg();
        toastr.success("Update Successful", "", "success")
        return true;
    }
}


function resetJobfairReg() {
        jQuery("#JFfirstname").val(""),
        jQuery("#JFmiddlename").val(""),
        jQuery("#jflastname").val(""),
        jQuery("#jfage").val(""),
        jQuery("#jfPhone").val(""),
        jQuery("#jfEmailId").val(""),
        jQuery("#state").val('0'),
        jQuery("#district").val('0'),
        jQuery("#city").val('0'),
        jQuery("#jfQualification").val('0'),
        jQuery("#jfFresher").val('Yes'),
        jQuery("#jsCurrentOrganisation").val(""),
        jQuery("#jfCurrentDesignation").val(""),
        jQuery("#jfrole").val(""),
        jQuery("#jfCurrentSalary").val(""),
        jQuery("#jfCurrentJobLocation").val(""),
        jQuery("#jfExpectations").val(""),
        jQuery("#jfDomicile").val('Select Domicile'),
        jQuery("#jfjobfair").val('0'),
        $("#checkboxList").prop("checked", false) 
      //  $("#checkboxList")[0][0].checked = false
}


function FillStates2() {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "State/0/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {
            jQuery("#state").empty();
            jQuery("#state").append(jQuery("<option ></option>").val("0").html("Select State"));
                for (var i = 0; i < data[0].length; i++) {
                jQuery("#state").append(jQuery("<option></option>").val(data[0][i].StateId).html(data[0][i].StateName));
            }
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
} 
  
jQuery('#state').on('change', function () {
    FillDistrict(jQuery('#state').val());
});

function FillDistrict() {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "district/0/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {
            jQuery("#district").empty();
            jQuery("#district").append(jQuery("<option ></option>").val("0").html("Select District"));
            for (var i = 0; i < data[0].length; i++) {
                jQuery("#district").append(jQuery("<option></option>").val(data[0][i].DistrictId).html(data[0][i].DistrictName));
            }
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}
jQuery('#district').on('change', function () {
    FillCity(jQuery('#district').val());
});

function FillCity(district) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "city/" + district + "/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {
            var data1 = data[0];
            jQuery("#city").empty();
            jQuery("#city").append(jQuery("<option ></option>").val("0").html("Select City"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#city").append(jQuery("<option></option>").val(data1[i].CityId).html(data1[i].CityName));
            }
        },
        error: function (xhr) {
            //swal(xhr.d, "", "error")
        }
    });
}

function FillCourse(funct,control) {
    var path =  serverpath + "Course/0/0/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillCourse(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillCourse('parsedatasecuredFillCourse','jfQualification');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Qualification"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].CourseId).html(data1[i].CourseName));
             }
        }
    }


    