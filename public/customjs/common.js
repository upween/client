

function FillSecuredSkills() {
    var path = serverpath + "SkillSet/0/0"
    securedajaxget(path,'parsedataskillsecured','comment','control');
}
function parsedataskillsecured(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillSecuredSkills();
    }
    else{
    var data1 = data[0];
    $('#SkillName').selectize({
        persist: false,
        createOnBlur: true,
         valueField: 'SkillName',
        labelField: 'SkillName',
        searchField: 'SkillName',
        options: data1,
        create: true
    });
        // $('.skillsearch').magicsearch({
        //     dataSource: data1,
        //     fields: ['SkillName'],
        //     id: 'SkillId',
        //     format: '%SkillName%',
        //     multiple: true,
        //     multiField: 'SkillName',
        //     multiStyle: {
        //         space: 5,
        //         width: 80
        //     }
        
        // });

    }
    }


 
function FillCategorySecured(funct,control){
    var path = serverpath + "secured/jobprefrences/0/0"
    securedajaxget(path,funct,'comment',control);
  
}
function parsedatasecuredcategory(data,control){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillCategorySecured('parsedatasecuredcategory',control);
    }
    else{
        var data1 = data[0];
        jQuery("#"+control).empty();
        jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Categories"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].FunctionalArea_id).html(data1[i].FunctionalArea));
        }
    }
}
function FillcitySecured(funct,control){
    var path = serverpath + "secured/district/0/0/0/0"
    securedajaxget(path,funct,'comment',control);
}
function parsedatasecuredcity(data,control){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillcitySecured('parsedatasecuredcity',control);
    }
    else{
        var data1 = data[0];
            jQuery("#"+control).empty();
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select District"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
            }
    }
}

// -------------------FOOTER DETAIL-------------------//dmpl

function FillState() {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "State/0/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {
            jQuery("#StateFetch").append(jQuery("<option ></option>").val("0").html("Select State"));
            for (var i = 0; i < data[0].length; i++) {
                jQuery("#StateFetch").append(jQuery("<option></option>").val(data[0][i].StateId).html(data[0][i].StateName));
            }
            //jQuery("#StateFetch").val('19');
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}





function FillFooterDetail(StateId) {
  jQuery.ajax({
      url: serverpath + "footerDetail/"+StateId+"",
      type: "GET",
      contentType: "application/json; charset=utf-8",
      cache: false,
    dataType: "json",
    success: function (data) {
        var appenddata="";
  appenddata += "<div class='col-lg-8 col-md-5 col-xs-12'  style='margin-top: 10px;><div class='row'> <div class='col-lg-4 col-md-12 col-xs-12 toll-nos'><i class='mqfi-phone'></i><span x-ms-format-detection='none' class='no-holder'><span lang='en'>Toll Free No:</span><span class='toll-nos-val'>"+ data[0][0].TollFreeNum +"</span></span></span></div><div class='col-lg-4 col-md-12 col-xs-12  color-base dark-link'><i class='fa fa-check-circle' aria-hidden='true'></i><span style='padding-left: 5px' lang='en'>Time:</span><span class='toll-nos-val'>" + data[0][0].OfficeTime + "</span></div><div class='col-lg-4 col-md-12 col-xs-12  color-base dark-link'><div style='overflow: hidden;text-overflow: ellipsis;white-space: nowrap; width: 115%;padding-left: 57px;margin-left:-20%;'><i class='fa fa-envelope-o' style='padding-right: 5px;' aria-hidden='true'></i><a href='" + data[0][0].EMailId + "'  class='under-link' draggable='false'>" + data[0][0].EMailId + "</a></div></div></div></div><div class='social' style='margin-top: 0%;margin-left: 75%''><a href='" + data[0][0].FacebookId + "'' target='_blank' style='color: #5039c7'><i class='fa fa-facebook-square' aria-hidden='true'></i> </a><a href='" + data[0][0].TwitterId + "'' target='_blank' style='color: #5039c7'><i class='fa fa-twitter-square' aria-hidden='true'></i></a><a href='" + data[0][0].GooglePlusId + "'' target='_blank' style='color: #5039c7'><i class='fa fa-google-plus-square' aria-hidden='true'></i></a><a href='" + data[0][0].LinkedinId + "'' target='_blank' style='color: #5039c7'><i class='fa fa-linkedin-square' aria-hidden='true'></i></a></div></div>"
     
       $('#FooterDetail').html(appenddata);
       
    },
    error: function (result) {
    }
});
}

//------------------------------------------------------------------//

toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": true,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};
function SessionClear(){
    sessionStorage.clear();
    localStorage.clear();
    Cookies.remove('1P_JAR');
    Cookies.remove('CandidateName');
    Cookies.remove('CuterCounter_81730');
    Cookies.remove('EmailId');
    Cookies.remove('FirstName');
    Cookies.remove('JFTitle');
    Cookies.remove('MobileNumber');
    Cookies.remove('RegistrationId');
    Cookies.remove('__utma');
    //Cookies.remove('langCookie');
    Cookies.remove('CandidateId');
    Cookies.remove('modaltype');
    Cookies.remove('__utmz');
    Cookies.remove('Employer');
}
function Counter(){
    var path = serverpath + "counter"
    ajaxget(path,'parsedatacounter','comment','control');
}
function parsedatacounter(data,control){
    data = JSON.parse(data)
    $("#jobseeker").html(data[0][0].JobSeekers);
    $("#employers").html(data[0][0].Employers);
   $("#Vacancies").html(data[0][0].Vacancies);
    $("#ClVacancies").html(data[0][0].ClVacancies);
    $('.count').each(function () {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 4000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });
}
function generateOTP() { 
          
    // Declare a digits variable  
    // which stores all digits 
    var digits = '0123456789'; 
    let OTP = ''; 
    for (let i = 0; i < 4; i++ ) { 
        OTP += digits[Math.floor(Math.random() * 10)]; 
    } 
    return OTP; 
  
 
} 


function CheckFeedBackUser(UserName,Type) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "feedbackUser/"+UserName+"/"+Type+"",
        //data: "{}",
        cache: false,
        dataType: "json",
        success: function (data) {
            if (Type == 'Feedback') {
                if (data[0][0].ReturnValue == "2") {
                 $("#validName").html("Please Enter Valid User Name");
                 $("#Name").css('border-color', 'red');
                 $("#Name").focus();
                    return false;
                     }
                 else if (data[0][0].ReturnValue == "1") {
                    $("#Name").css('border-color', '');
                     $("#validName").html("");
                  return true;
                    }
            }
            if (Type == 'Grievance') { 
                if (data[0][0].ReturnValue == "2") {
                 $("#validRegistrationId").html("Please Enter Valid Registered ID");
                 $("#RegistrationId").css('border-color', 'red');
                 $("#RegistrationId").focus();
                 return false;
                  }
            else if (data[0][0].ReturnValue == "1") {
                
                $("#validRegistrationId").html("");
                $("#RegistrationId").css('border-color', '');
                 return true;
                  }
        }
    }
    });
}

//$(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);


function ProfileStatus(funct,control) {
    var path = serverpath + "ProfileStatus/"+sessionStorage.getItem("CandidateId")+""
    securedajaxget(path,funct,'comment',control);
}

function parsedataProfileStatus(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        ProfileStatus('parsedataProfileStatus','progressbar');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    if(data.errno || data.status==504) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }
    else{
        if(!Array.isArray(data) || !Array.isArray(data[0])) {
            return;
        }
        var data1 = data[0];
      
        var appenddata = "";
            appenddata += "<div class='progress-bar progress-bar-info' role='progressbar' aria-valuenow='40' aria-valuemin='0' aria-valuemax='100' style='width: "+data[0][0].ProfileStatus+"%'> <span style='color:black'>"+data[0][0].ProfileStatus+"%</span> </div>";
            
            sessionStorage.setItem("ProfileStatus",data[0][0].ProfileStatus)
        jQuery("#progressbar").html(appenddata);
       
        if(parseInt(sessionStorage.getItem("ProfileStatus"))>=50 &&sessionStorage.getItem("Page")=="Reg"){
           // sendemailprofile();
          //  window.location="/Profile_new";
          sessionStorage.setItem('RegistrationId',data[0][0].RegistrationId);
          Cookies.set('RegistrationId', data[0][0].RegistrationId, { expires: 1, path: '/' });
          window.location="/Home";
        }
       else if(parseInt(sessionStorage.getItem("ProfileStatus"))<50 &&sessionStorage.getItem("Page")==""){
          window.location="/Profilereg";
         }

    // else {
    //     window.location="/Profilereg";
    //    }
        
    } 
}

function FillMonth(control) {
    var path =  serverpath + "month/0"
    securedajaxget(path,'parsedatasecuredFillMonth','comment',control);
}

function parsedatasecuredFillMonth(data,control){  
    data = JSON.parse(data)
    

            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].ShortMonthName));
             }
        
              
}

function sentmailglobal(to,body,subject) {
var msg=`<table width='600' border='0' align='center' cellpadding='0' cellspacing='0'>
<tr>
<td align='left' valign='top' style='border-right: solid thick #f0f0f0; border-left: solid thick #f0f0f0; border-top: solid thick #f0f0f0;'>
<img src='http://mprojgar.gov.in/images/header_name.gif' style='width:100%'> 
</td>
 </tr>
 <tr>
 <td align='center' valign='top' style=' border-right: solid thick #f0f0f0; border-left: solid thick #f0f0f0; background-color:#f0f0f0; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000; padding:10px;'><table width='100%' border='0' cellspacing='0' cellpadding='0' style='margin-top:10px;'>
 <tr>
 <td align='left' valign='top' style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#525252;'>
 <div style='font-size:22px;'>
 ${body}
 </div>
 <div>
 <br> 
    For any further queries, please write to us at helpdesk.mprojgar@mp.gov.in with your current MP Rojgar portal username or call us at our toll free number 18002757751 (Monday to Saturday between 10 am – 6 pm ). 
  <br><br>
  Note: This is autogenerated email please do not reply.<br></div></td></tr></table></td></tr>
</table>`
        var MasterData = {
               "To":to,
               "Msg":msg,
               "Subject":subject
              
        }
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "sentmail";
        ajaxpost(path, 'datasentmail', 'comment', MasterData, 'control')
     }
     function datasentmail(data) {
        data = JSON.parse(data)
     
     }
     function sentSmsGlobal(to,msg) {

        var MasterData = {
               "mobile":to,
               "message":msg,
              
        }
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "sendsms";
        ajaxpost(path, 'datasentsms', 'comment', MasterData, 'control')
     }
     function datasentsms(data) {
        data = JSON.parse(data)
     
     }

     function PrintUserRegCard() {
        if($('#userregnumberprint').val()==""){
          toastr.warning("Please enter Registration Number", "", "info")
            return false;
        }
        else{
        $('#printuserregbtn').attr('disabled',true)
     
        // var path = serverpath + "knowyourregnew/'RegistrationId'/'"+$('#userregnumberprint').val()+"'";
        // ajaxget(path, 'parsrdataPrintUserRegCard', 'comment', 'control');
        // }
        // }
        var MasterData = {  
            "p_FilterType":'RegistrationId',
            "p_FilterValue":$('#userregnumberprint').val(),
            "p_FirstName":'',
            "p_LastName":'',
            "p_Gender":''
              
        };
        MasterData = JSON.stringify(MasterData)
        var path = serverpath +"knowyourregnew"
          ajaxpost(path, 'parsrdataPrintUserRegCard', 'comment',MasterData,'control');
        }
    }
        function parsrdataPrintUserRegCard(data) {
            data = JSON.parse(data);
            $.unblockUI();
            $('#messageprint').text('');
            $('#printuserregbtn').attr('disabled',false)
            if(!Array.isArray(data) || !Array.isArray(data[0]) || !data[0].length) {
                       $('#messageprint').text('You are not registered');
               }
           else{
            sessionStorage.CandidateId=data[0][0].CandidateId;
            sessionStorage.CandidateUsername=data[0][0].UserName;
            PrintRegCard();
           } 
           }
          

         
          



