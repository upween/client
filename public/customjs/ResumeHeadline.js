function CheckResumeHeadline(){
    if(isValidate){
        if ($.trim(jQuery('#resumehealdline').val()) == '') {
            getvalidated('resumehealdline','text','Detail');
      }
      else { 
        
          InsUpdResumeHeadline();
       }
    }
    else { 
        
        InsUpdResumeHeadline();
     }
   
}
function InsUpdResumeHeadline(){
    $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);

    var MasterData = {
      "p_DetailId": localStorage.getItem("ResumeHeadLineId"),
      "p_Detail": $("#resumehealdline").val(),
      "p_CandidateId":sessionStorage.getItem("CandidateId"),
      "p_IpAddress": sessionStorage.getItem("IpAddress"),
      "p_UserId": sessionStorage.getItem("CandidateId")
  
    }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/resumeheadline";
    securedajaxpost(path,'parsrdataresumeheadline','comment',MasterData,'control')
}
function parsrdataresumeheadline(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        InsUpdResumeHeadline();
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else if (data[0][0].ReturnValue == "1") {
        ProfileStatus('parsedataProfileStatus','progressbar');
        toastr.success("Submit Successfully", "", "success")
        $('#myModal1').modal('toggle');
        FillResumeHeadlineSecured('parsedatasecuredresumeheadline','detail');
        return true;
    }
    else if (data[0][0].ReturnValue == "2") {
        toastr.success("Submit Successfully", "", "success")
        $('#myModal1').modal('toggle');
        FillResumeHeadlineSecured('parsedatasecuredresumeheadline','detail');
        return true;
    }
   
     }
     function FillResumeHeadlineSecured(funct,control) {
        var path = serverpath + "secured/resumeheadline/'"+sessionStorage.getItem("CandidateId")+"'/'"+sessionStorage.getItem("IpAddress")+"'/'"+sessionStorage.getItem("CandidateId")+"'"
        securedajaxget(path,funct,'comment',control);
    }
    function parsedatasecuredresumeheadline(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillResumeHeadlineSecured('parsedatasecuredresumeheadline','detail');
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
           else if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        
        else{
           // ProfileStatus('parsedataProfileStatus','progressbar');
            if(data[0]=="")
            {
               localStorage.setItem("ResumeHeadLineId","0")
               jQuery("#"+control).html("<span lang='en'>Add Resume Headline</span>");
            }
            else{
            var data1 = data[0];
            localStorage.setItem("ResumeHeadLineId",data1[0].DetailId)
            var appenddata="";
            var resumehead="";
            jQuery("#"+control).empty();
            appenddata+=""+data1[0].Detail+"";
            jQuery("#"+control).html(appenddata);
            jQuery("#resumehealdline").val(appenddata);
           resumehead+=" <span lang='en'>Updated On</span> "+data[0][0].UpdateOn+"";
           jQuery("#rsmhd").html(resumehead);
           getvalidated('resumehealdline','text','Detail');
           
        //ProfileStatus('parsedataProfileStatus','progressbar');
        }
    }
}