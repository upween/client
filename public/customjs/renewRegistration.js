function renewRegistration() {

    var MasterData = {
        

        "p_CandidateId": sessionStorage.getItem("CandidateId")
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "registration_renew";
    ajaxpost(path, 'parsrdatarenewRegistration', 'comment', MasterData, 'control')
}
function parsrdatarenewRegistration(data) {
    data = JSON.parse(data)
    if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }
  if(data[0][0].renewal_date=='1'){
    toastr.warning("Renew Registration is not available", "", "info");
    
  }
  else{
        $('#renewDate').text(data[0][0].renewal_date);
        sessionStorage.renewal_date=data[0][0].renewal_date;
        toastr.success("Renew Registration Successfully", "", "success");
        $('#closerenewModal').click();
  }
 
    
   
}