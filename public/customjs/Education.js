

function CheckEducation() {
    if(isValidate){

    
    if ($("#ExamPassed" ).val() == "23" ||$("#ExamPassed" ).val() == "24"){
         if (jQuery('#ExamPassed').val() == "0") {
        getvalidated('ExamPassed','select','Qualification');
        return false;
    }
  
    if (jQuery('#Course').val() == "0") {
        getvalidated('Course','select','Subject Group');
        return false;
    }
    


   
else{  InsUpdEducation()}}
    else{
    if (jQuery('#ExamPassed').val() == "0") {
        getvalidated('ExamPassed','select','Qualification');
        return false;
    }
  
    if (jQuery('#Course').val() == "0") {
        getvalidated('Course','select','Subject Group');
        return false;
    }
    

   
    if (jQuery('#CourseType').val() == "0") {
        getvalidated('CourseType','select','CourseType');
        return false;
    }

    if (jQuery('#university').val() == "0") {
        getvalidated('university','select','University/Board');
        return false;
    }
    if (jQuery('#PassingOutYear').val() == "0") {
        getvalidated('PassingOutYear','select','PassingOutYear');
        return false;
    }
    if (jQuery('#GradingSystem').val() == "0") {
        getvalidated('GradingSystem','select','GradingSystem');
        return false;
    }
    if (jQuery('#Percentage').val() == "") {
        getvalidated('Percentage','number','Percentage');
        return false;
    }
    if (jQuery('#Division').val() == "0") {
        getvalidated('Division','select','Division');
        return false;
    }
else{  InsUpdEducation()
}
}
    }
    else{  InsUpdEducation()}

};

function InsUpdEducation() {

    $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);
 

    var MasterData = {

        "p_GraduationId":sessionStorage.getItem("GraduationId"),
        "p_CandidateId": sessionStorage.getItem("CandidateId"),
        "p_EducationId": jQuery("#ExamPassed").val(),
        "p_CourseId": jQuery("#Course").val(),
        "p_SpecializationId": $("#Specialization").val(),
        "p_SubjectId": "",
        "p_UniversityId": jQuery("#university option:selected").val(),
        "p_CourseTypeId": jQuery("#CourseType option:selected").val(),
        "p_PassingOutYearId": jQuery("#PassingOutYear option:selected").val(),
        "p_GradingSystemId": '0',
        "p_Percentage": jQuery("#Percentage").val(),
        "p_Division": jQuery("#Division option:selected").val(),
        "p_IpAddress": sessionStorage.getItem("IpAddress"),
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/graduation";
    securedajaxpost(path, 'parsrdatagraduation', 'comment', MasterData, 'control')
}
function parsrdatagraduation(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        InsUpdEducation();
    }
      else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else if (data[0][0].ReturnValue == "1") {
        
     //   $('#Close').click();
        $('#myModal1').modal('toggle');
        fillFetchEducation('parsedatasecuredfillFetchEducation','Education');
        toastr.success("Insert Successful", "", "success")
        ProfileStatus('parsedataProfileStatus','progressbar');
        return true;
    }
    else if (data[0][0].ReturnValue == "2") {
      //  $('#Close').click();
        ProfileStatus('parsedataProfileStatus','progressbar');
        $('#myModal1').modal('toggle');
        resetModeEducation();
        fillFetchEducation('parsedatasecuredfillFetchEducation','Education');
        toastr.success("Update Successful", "", "success")
        return true;
    }
    else if (data[0][0].ReturnValue == "0") {
        
        toastr.warning("Education Detail already Exists ", "", "info")
        return true;
    }
    else if (data[0][0].ReturnValue == "3") {
        
        toastr.warning("Same passing year exist", "", "info")
        return true;
    }

}


function resetModeEducation() {
        jQuery('#ExamPassed').css('border-color', '');
        jQuery('#Course').css('border-color', '');
        jQuery('#Specialization').css('border-color', '');
        jQuery('#CourseType').css('border-color', '');
        jQuery('#university').css('border-color', '');
        jQuery('#PassingOutYear').css('border-color', '');
        jQuery('#GradingSystem').css('border-color', '');
        jQuery('#Percentage').css('border-color', '');
        jQuery('#Division').css('border-color', '');
        $('#Specialization').trigger('set', { id: "" });
         
        $('#validExamPassed').html("");
        $("#validCourse").html("");
        $("#validSpecialization").html("");
        $("#validCourseType").html("");
        $("#validuniversity").html("");
        $("#validPassingOutYear").html("");
        $("#validGradingSystem").html("");
        $("#validPercentage").html("");
        $("#validDivision").html("");

        jQuery("#ExamPassed").val("0"),
        jQuery("#Course").val("0"),
        jQuery("#Specialization").val(""),
        jQuery("#university").val("0"),
        jQuery("#CourseType").val("0"),
        jQuery("#PassingOutYear").val("0"),
        jQuery("#GradingSystem").val("0"),
        jQuery("#Marks").val(""),
        jQuery("#Percentage").val(""),
        jQuery("#Division").val("0"),
       jQuery("#ExamPassed").prop("disabled", false);
        $("#Specialization").prop("disabled", false);
        $("#Course").prop("disabled", false);

}



function fillFetchEducation(funct,control) {
    var path =  serverpath + "secured/graduation/'" + sessionStorage.getItem("CandidateId") + "'/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredfillFetchEducation(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        fillFetchEducation('parsedatasecuredfillFetchEducation','Education');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else{
        var updatedon = "";
        var sessionEducation="";
        if(data[0]=="")
        {
           localStorage.setItem("GraduationId","0")
           sessionStorage.setItem("GraduationId","0")
           jQuery("#"+control).html("<p><span lang='en'>Add Education</span></p>")
           sessionStorage.setItem("illterate","");
        }
        else{
        var data1 = data[0];
        localStorage.setItem("GraduationId",data1[0].GraduationId)
        sessionStorage.setItem("GraduationId",data1[0].GraduationId)
        var appenddata="";
        var setpreference="";
        jQuery("#"+control).empty();
        for (var i = 0; i < data[0].length; i++) {
      if(data[0][i].Qualif_name=="Illiterate"||data[0][i].Qualif_name=="Literate"){
      //  appenddata += "  <p  class='profiledetail'>" + data[0][i].Qualif_name +  ' : '  + data[0][i].Sub_Group_Name+"<span style='margin-left:10px;'><a href='#educationtab' onclick=EditeducationDetail('" + data[0][i].GraduationId + "','"+ data[0][i].EducationId +"','"+ data[0][i].CourseId +"','"+ data[0][i].SpecializationId +"','"+ data[0][i].Percentage +"','"+ data[0][i].Division +"','"+ data[0][i].UniversityId +"','"+ data[0][i].CourseTypeId +"','"+ data[0][i].PassingOutYearId +"','"+data[0][i].GradingSystemId +"') ><i class='fa fa-pencil'></i></a></span><span style='margin-left:10px;'><a href='#educationtab' onclick=Delete("+data1[i].GraduationId+",'Education') ><i class='fa fa-times' style='padding-left:20px;'></i></a></span></p> ";
        appenddata +="<tr><td>" + data[0][i].Qualif_name + "</td><td>" + data[0][i].Sub_Group_Name + "</td><td>-</td><td>-</td><td>-</td><td>-</td><td><span style='margin-left:10px;'> <a href='#educationtab' onclick=EditeducationDetail('" + data[0][i].GraduationId + "','"+ data[0][i].EducationId +"','"+ data[0][i].CourseId +"','"+ encodeURI(data[0][i].SpecializationId) +"','"+ data[0][i].Percentage +"','"+ data[0][i].Division +"','"+ data[0][i].UniversityId +"','"+ data[0][i].CourseTypeId +"','"+ data[0][i].PassingOutYearId +"','"+data[0][i].GradingSystemId +"') ><i class='fa fa-pencil'></i></a></span> </td><td> <span style='margin-left:10px;'><a href='#educationtab' onclick=Delete("+data1[i].GraduationId+",'Education') ><i class='fa fa-times' style='padding-left:20px;'></i></a></span> </td></tr>";

        if(data[0][i].Qualif_name=="Illiterate"){

        sessionStorage.setItem("illterate","Yes");
        }
    }
      else{
        appenddata +="<tr><td>" + data[0][i].Qualif_name + "</td><td>" + data[0][i].Sub_Group_Name + "</td><td>"+ data[0][i].UniversityName +"</td><td>"+ data[0][i].Q_Division_Detail +"</td><td>"+ data[0][i].Percentage + " %</td><td>"+ data[0][i].Year +"</td><td><span style='margin-left:10px;'> <a href='#educationtab' onclick=EditeducationDetail('" + data[0][i].GraduationId + "','"+ data[0][i].EducationId +"','"+ data[0][i].CourseId +"','"+encodeURI(data[0][i].SpecializationId) +"','"+ encodeURI(data[0][i].Percentage) +"','"+ data[0][i].Division +"','"+ data[0][i].UniversityId +"','"+ data[0][i].CourseTypeId +"','"+ data[0][i].PassingOutYearId +"','"+data[0][i].GradingSystemId +"') ><i class='fa fa-pencil'></i></a></span> </td><td> <span style='margin-left:10px;'><a href='#educationtab' onclick=Delete("+data1[i].GraduationId+",'Education') ><i class='fa fa-times' style='padding-left:20px;'></i></a></span> </td></tr>";

         //   appenddata += "  <p  class='profiledetail'>" + data[0][i].Qualif_name +  ' : '  + data[0][i].Sub_Group_Name+"<span style='margin-left:10px;'><a href='#educationtab' onclick=EditeducationDetail('" + data[0][i].GraduationId + "','"+ data[0][i].EducationId +"','"+ data[0][i].CourseId +"','"+ data[0][i].SpecializationId +"','"+ encodeURI(data[0][i].Percentage) +"','"+ data[0][i].Division +"','"+ data[0][i].UniversityId +"','"+ data[0][i].CourseTypeId +"','"+ data[0][i].PassingOutYearId +"','"+data[0][i].GradingSystemId +"') ><i class='fa fa-pencil'></i></a></span><span style='margin-left:10px;'><a href='#educationtab' onclick=Delete("+data1[i].GraduationId+",'Education') ><i class='fa fa-times' style='padding-left:20px;'></i></a></span></p> <p class='profilesubheading'>" + data[0][i].UniversityName + '-' + data[0][i].Year + "</p>";
            sessionStorage.setItem("illterate","No");

        }
        updatedon    = '<span lang="en">Updated On</span> ' + data[0][0].UpdateOn;
        
           setpreference+="<tr><td>"+[i+1]+"</td><td>Qualification</td><td>"+data[0][i].Qualif_name+"</td><td>"+data[0][i].Sub_Group_Name+"</td><td><input id='"+data[0][i].GraduationId+"' type='checkbox' name='checkbox' value='"+data[0][i].Qualif_name+"'></td></tr>"
           sessionEducation=data[0][i].Qualif_name;

        }
    }
        jQuery("#"+control).html(appenddata);
        jQuery("#setpreferencetable").html(setpreference);
        sessionStorage.CandidateQualification=sessionEducation;

       // ProfileStatus('parsedataProfileStatus','progressbar');
    }
    $("#UpdateOn1").html(updatedon);
}



function EditeducationDetail(GraduationId,EducationId,CourseId,SpecializationId,Percentage,Division,UniversityId,CourseTypeId,PassingOutYearId,GradingSystemId) {
    modal('education');
      
    sessionStorage.setItem("GraduationId",GraduationId);
    jQuery("#ExamPassed").val(EducationId)
  //  FillCourseEdit(EducationId,CourseId); 
    FillCourseEdit('parsedatasecuredFillCourseEdit','Course',EducationId,CourseId);
  
 // $('#Specialization').trigger('set', { id: SpecializationId });
    $('#Specialization').val(decodeURI(SpecializationId));

    jQuery("#Percentage").val(decodeURI(Percentage))
    jQuery("#Division").val(Division)
    jQuery("#university").val(UniversityId)
    jQuery("#CourseType").val(CourseTypeId)
    jQuery("#PassingOutYear").val(PassingOutYearId)
    jQuery("#GradingSystem").val(GradingSystemId)
    //jQuery("#ExamPassed").prop("disabled", true);
    if ($("#ExamPassed" ).val() == "24") {
        $("#Specialization1").show(); 
        $("#EduSubject").show();
        $("#EduSubject").val("0")
      //  $("#course1").hide();
        $("#CourseType1").hide();
        $("#University1").hide();
        $("#PassingOutYear1").hide();
        $("#GradingSystem1").hide();
        $("#Percentage1").hide();
        $("#Division1").hide();
    }
    else if ($("#ExamPassed" ).val() == "23") {
        $("#Specialization1").show();
        $("#EduSubject").show();
        $("#EduSubject").val("0")
      //  $("#course1").hide();
        $("#CourseType1").hide();
        $("#University1").hide();
        $("#PassingOutYear1").hide();
        $("#GradingSystem1").hide();
        $("#Percentage1").hide();
        $("#Division1").hide();
    }
    else  {
        $("#Specialization1").show();
        $("#EduSubject").show();
        $("#EduSubject").val("0")
      //  $("#course1").hide();
        $("#CourseType1").show();
        $("#University1").show();
        $("#PassingOutYear1").show();
        $("#GradingSystem1").show();
        $("#Percentage1").show();
        $("#Division1").show();
    }
    $("#ExamPassed").prop("disabled", true);
    $("#Course").prop("disabled", true);
    $("#Specialization").prop("disabled", true);
}




function FillEducation(funct,control) {
    var path =  serverpath + "secured/qualification/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillEducation(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillEducation('parsedatasecuredFillEducation','ExamPassed');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
        else{
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val('0').html("Select Education"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Qualif_id).html(data1[i].Qualif_name));
            }
        }
                  
}

jQuery('#ExamPassed').on('change', function () {
  //  FillCourse(jQuery('#ExamPassed').val());
        FillCourse('parsedatasecuredFillCourse','Course',jQuery('#ExamPassed').val());
        if(sessionStorage.getItem("illterate")=="No" && jQuery('#ExamPassed').val()=="24"){
            toastr.warning("Please First Delete Your Qualification Details", "", "info")
          $("#Specialization").prop("disabled", true);
          $("#Course").prop("disabled", true);
           return false;
        }
       
});


function FillCourse(funct,control,EducationId) {
    var path =  serverpath + "secured/subjectgroup/0/'" + EducationId + "'/1"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillCourse(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillCourse('parsedatasecuredFillCourse','Course');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Subject Group"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Sub_Group_id).html(data1[i].Sub_Group_Name));
             }
        }
    }
    function FillCourseType(funct,control) {
        var path =  serverpath + "secured/coursetype/0/0"
        securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillCourseType(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillCourseType('parsedatasecuredFillCourseType','CourseType');
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
           else if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        
            else{
                var data1 = data[0];
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Course Type"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].CourseTypeId).html(data1[i].CourseTypeName));
                }
            }
                  
    }

    function Filluniversity(funct,control) {
        var path =  serverpath + "secured/university/0/0/0/0"
        securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFilluniversity(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            Filluniversity('parsedatasecuredFilluniversity','university');
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
           else if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        
            else{
                var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select University/Board"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].UniversityId).html(data1[i].UniversityName));
            }
            }
                  
    }
    
                  
    

    function FillPassingOutYear(funct,control) {
        var path =  serverpath + "secured/year/0/50"
        securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillPassingOutYear(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillPassingOutYear('parsedatasecuredFillPassingOutYear','PassingOutYear');
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
           else if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        
            else{
                var data1 = data[0];
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Passing Out Year"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].FinYearId).html(data1[i].Year));
                 }
            }
                  
    }
    function FillCourseEdit(funct,control,EducationId,CourseId) {
        sessionStorage.setItem("FillCourseEditCourseId",CourseId);
        sessionStorage.setItem("FillCourseEditEducationId",EducationId);
        var path =  serverpath + "secured/subjectgroup/0/'" + EducationId + "'/1"
        securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillCourseEdit(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token); 
            FillCourseEdit('parsedatasecuredFillCourseEdit','Course',sessionStorage.getItem("FillCourseEditEducationId"),sessionStorage.getItem("FillCourseEditCourseId"));
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
           else if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Course"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Sub_Group_id).html(data1[i].Sub_Group_Name));
                 }
                jQuery("#"+control).val(sessionStorage.getItem("FillCourseEditCourseId"))
            }
                  
    }


    
    function InsUpdSetPreference() {

        var setpreference = [];
        $.each($("input[name='checkbox']:checked"), function(){ 
            setpreference.push($(this).val());
        });
        localStorage.CandidateQualification=setpreference;
        sessionStorage.CandidateQualification=setpreference;

          var MasterData = {
            
            "p_CandidateId": sessionStorage.getItem("CandidateId"),
            "p_Qualification": setpreference.join(", "),
            "p_Location": "",
            "p_Industry": "",
            "p_Title": "",
            "p_Type": "Set",
            
        };
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "secured/setpreference";
        securedajaxpost(path, 'parsrdatasetpreference', 'comment', MasterData, 'control')
    }
    function parsrdatasetpreference(data) {
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            InsUpdSetPreference();
        }
          else  if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        
        else if (data[0][0].ReturnValue == "1") {
       //   sessionStorage.setItem('CandidateQualification',sessionStorage.CandidateQualification)

            $("#Close").click();
            sessionStorage.setItem("TypeHome","SetPreference");
            localStorage.setItem("TypeHome","SetPreference");
            FillSetPreference();
            toastr.success("Sumbit Successfully", "", "success")
            location.reload();
            return true;
        }
        else if (data[0][0].ReturnValue == "2") {
      //    sessionStorage.setItem('CandidateQualification',sessionStorage.CandidateQualification)

           $("#Close").click();
           sessionStorage.setItem("TypeHome","SetPreference")
           localStorage.setItem("TypeHome","SetPreference")
           FillSetPreference();
            toastr.success("Sumbit Successfully", "", "success")
            location.reload();
            return true;

        }
        
    
    }
    function CheckPreference(){
        var radios = document.getElementsByName("checkbox");
        var formValid = false;

        var i = 0;
        while (!formValid && i < radios.length) {
            if (radios[i].checked)
                formValid = true;
            i++;
        }

        if (!formValid) {

            
            toastr.warning("Please Select Atleast one Preference", "", "info")
            return false;



        }
        else{
            InsUpdSetPreference();
        }
    }
  
    function percentagecheck(){
        if(jQuery("#Division").val()=='0')
        {
            getvalidated('Division','select','Division');
        }
        if (jQuery("#Division").val()=='1' ){
        if(jQuery("#Percentage").val()>100||jQuery("#Percentage").val()<60){
            jQuery("#Percentage").val("");
            jQuery("#Percentage").focus();
            toastr.warning("Please Enter proper Percentage", "", "info")
            return false;
        }
    }
        if (jQuery("#Division").val()=='2' ){
            if(jQuery("#Percentage").val()>59||jQuery("#Percentage").val()<46){
                jQuery("#Percentage").val("");
                jQuery("#Percentage").focus();
                toastr.warning("Please Enter proper Percentage", "", "info")
                return false;
            }
        }
            if (jQuery("#Division").val()=='3' ){
                if(jQuery("#Percentage").val()>45||jQuery("#Percentage").val()<33){
                    jQuery("#Percentage").val("");
                    jQuery("#Percentage").focus();
                    toastr.warning("Please Enter proper Percentage", "", "info")
                    return false;
                }
            }
                if (jQuery("#Division").val()=='4' ){
                    if(jQuery("#Percentage").val()>100||jQuery("#Percentage").val()<85){
                        jQuery("#Percentage").val("");
                        jQuery("#Percentage").focus();
                        toastr.warning("Please Enter proper Percentage", "", "info")
                        return false;
                    }
                    
    }
    if (jQuery("#Division").val()=='5' ){
        if(jQuery("#Percentage").val()>84||jQuery("#Percentage").val()<70){
            jQuery("#Percentage").val("");
            jQuery("#Percentage").focus();
            toastr.warning("Please Enter proper Percentage", "", "info")
            return false;
        }
    }
                   if (jQuery("#Division").val()=='6' ){
        if(jQuery("#Percentage").val()>69||jQuery("#Percentage").val()<55){
            jQuery("#Percentage").val("");
            jQuery("#Percentage").focus();
            toastr.warning("Please Enter proper Percentage", "", "info")
            return false;
        }
                }
                if (jQuery("#Division").val()=='9' ){
                    if(jQuery("#Percentage").val()>32||jQuery("#Percentage").val()<0){
                        jQuery("#Percentage").val("");
                        jQuery("#Percentage").focus();
                        toastr.warning("Please Enter proper Percentage", "", "info")
                        return false;
                    }
                }
        }
    
    $("#Percentage").focusout(function () {

      if( $("#Percentage").val()=='')  {
        getvalidated('Percentage','text','Percentage');
      }
      if( $("#Percentage").val()!='')  {
        percentagecheck();
        getvalidated('Percentage','text','Percentage');
      }

    });

    
    $("#Division").focusout(function () {

        if( $("#Division").val()=='0')  {
          getvalidated('Division','select','Division');
        }
      
  
      });
      
    // function divisioncheck(){
    //     if(jQuery("#Percentage").val()=='')
    //     {
    //         getvalidated('Percentage','text','Percentage');
    //     }
        
    //     if(parseInt(jQuery("#Percentage").val())<=100 && parseInt(jQuery("#Percentage").val())>=60){

    //         if(jQuery("#Division option:selected").text() !="First"){
    //             jQuery("#Division").focus();
               
            
    //         toastr.warning("Please Enter select proper division", "", "info")
    //         return false;
    //         }
    //     }
    //    else if(parseInt(jQuery("#Percentage").val())<60 && parseInt(jQuery("#Percentage").val())>=40){

    //         if(jQuery("#Division option:selected").text() !="Second"){
    //         jQuery("#Division").focus();
            
    //         toastr.warning("Please Enter select proper division", "", "info")
    //         return false;
    //     }
    // }
    //   else  if(parseInt(jQuery("#Percentage").val())>40){

    //         if(jQuery("#Division option:selected").text() !="Fail"){
    //         jQuery("#Division").focus();
           
    //         toastr.warning("Please Enter select proper division", "", "info")
    //         return false;
    //     }
    //     }
     
    // }

   

    function FillSetPreference() {
        
        var path =  serverpath + "secured/setpreference/"+sessionStorage.getItem("CandidateId")+""
        securedajaxget(path,'parsedatasecuredFillSetPreference','comment','control');
    }
    function parsedatasecuredFillSetPreference(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token); 
            FillSetPreference();
        }
           else if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        
    
            else{
                if(data[0][0]==""){      
                var CheckId = data[0][0].Qualification.split(",");

                
                for (var i = 0; i < CheckId.length; i++) {
                    $("#"+CheckId[i]).prop("checked",true);
                }

                
            }
            }
          
                  
    }


    function FillDivision(funct,control) {
        var path =  serverpath + "secured/QualificationDivision/0/0"
        securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillDivision(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillDivision('parsedatasecuredFillDivision','Division');
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
           else if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        
            else{
                var data1 = data[0];
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Division"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Q_Division_id).html(data1[i].Q_Division_Detail));
                 }
            }
                  
    }


    function educationAPI(){
        fillFetchEducation('parsedatasecuredfillFetchEducation','Education');
        FillEducation('parsedatasecuredFillEducation','ExamPassed');
        FillCourseType('parsedatasecuredFillCourseType','CourseType');
        Filluniversity('parsedatasecuredFilluniversity','university');
        FillPassingOutYear('parsedatasecuredFillPassingOutYear','PassingOutYear');
        FillDivision('parsedatasecuredFillDivision','Division');

    }