
function UserBasicDetails(){
        var path = serverpath + "secured/registration/'" + sessionStorage.getItem("CandidateId") + "'/0"
        securedajaxget(path, 'parsedatasecuredFillProfileDetail', 'comment', 'control');
    }
    function parsedatasecuredFillProfileDetail(data) {
        data = JSON.parse(data)
        if (data.message == "New token generated") {
            sessionStorage.setItem("token", data.data.token);
            UserBasicDetails();
        }
        else if (data.status == 401) {
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
        else{
            if(sessionStorage.CandidateDistrictName==''){
                sessionStorage.CandidateDistrictName=data[0][0].DistrictName;
            }
            sessionStorage.renewal_date=data[0][0].renewal_date;
        var Contact = "<i class='fa fa-phone' aria-hidden='true'></i>&nbsp"+data[0][0].MobileNumber+"&nbsp<span></span>";
        var EmailId ="<i class='fa fa-envelope-o' aria-hidden='true'></i>&nbsp"+data[0][0].EmailId+"&nbsp";
        var regnumber = "<i class='fa fa-credit-card' title='Registration No' aria-hidden='true' style='padding-right: 2px;width:17px;'></i>&nbsp"+sessionStorage.getItem("RegistrationId")+"";
        if(data[0][0].DesignationName==null){
            $("#homedesignation").html("");  
        }
        else{
        $("#homedesignation").html(data[0][0].DesignationName + ", " + data[0][0].Organisation);
        }
        $("#UserName").html(sessionStorage.getItem("CandidateName"));
        $("#UserMobileNo").html(Contact);
        $("#UserEmailId").html(EmailId);
        $("#registrationnumber").html(regnumber);
        
        }
    }
    

function olduser(){

var dateOld = new Date(sessionStorage.getItem("RegDate"))
var datenew = new Date('2020-07-22 00:00:00')


     if (dateOld.getTime() < datenew.getTime()) {
        localStorage.setItem("Page","Home");
       // window.location = '/userDetail'
     }
    
}
