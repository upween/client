function CheckValidation(){


	if (jQuery('#WatchWords').val() == '') {
        toastr.warning("Please Enter Watch Words", "", "info")
        return true;
    }
    else if (jQuery('#Location').val() == '') {
        toastr.warning("Please Enter Your Location ", "", "info")
        return true;
    }
    else if (jQuery('#WorkExperince').val() == '0') {
        toastr.warning("Please Select Yuor Work Experince In Year", "", "info")
        return true;
    }
    else if (jQuery('#WorkExperinceMnth').val() == '0') {
        toastr.warning("Please Select Work Experince In Month", "", "info")
        return true;
    }
    else if (jQuery('#ExpectedSalary').val() == '0') {
        toastr.warning("Please Select Expected Salary ", "", "info");
        return true;
    }
    else if (jQuery('#Industry').val() == '') {
        toastr.warning("Please Enter Industry ", "", "info");
        return true;
    }
    else if (jQuery('#CategoryAlert').val() == '0') {
        toastr.warning("Please Select  Category ", "", "info");
        return true;
   }
    else if (jQuery('#Role').val() == '0') {
        toastr.warning("Please Select Role", "", "info");
        return true;
    }
    else if (jQuery('#Designation').val() == '0') {
        toastr.warning("Please Select Designation", "", "info")
        return true;
 }
 else if (jQuery('#Skill').val() == '') {
    toastr.warning("Please Enter Skill", "", "info")
    return true;
}
else if (jQuery('#RegEmailId').val() == '') {
    toastr.warning("Please Enter E-Mail ID", "", "info")
    return true;
}
        else if (jQuery('#RegMblNo').val() == '') {
            toastr.warning("Please Enter Registered Mobile number", "", "info")
            return true;
        }
		else{
			InsUpdJobAlert();
		}
        
};


function InsUpdJobAlert(){
    var RegEmailId;
    RegEmailId = jQuery('#RegEmailId').val();

    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if (reg.test(RegEmailId) == false) {
        toastr.warning("Please Enter Correct E-MailId", "", "info");
        return false;
    }


    var MasterData = {
    
    "p_JobAlertId":'0',
	"p_WatchWords":jQuery("#WatchWords").val(),
    "p_Location":jQuery("#Location").val(),
	"p_WorkExperinceInYear":jQuery("#WorkExperince").val(),
	"p_WorkExperinceInMnth":jQuery("#WorkExperinceMnth option:selected").val(),
	"p_ExpectedSalary":jQuery("#ExpectedSalary option:selected").val(),
	"p_Industry":jQuery("#Industry").val(),
	"p_Category":jQuery("#CategoryAlert").val(),
	"p_Role":jQuery("#Role option:selected").val(),
	"p_Designation":jQuery("#Designation option:selected").val(),
    "p_Skill":jQuery("#Skill").val(),
	"p_RegEmailId":jQuery("#RegEmailId").val(),
    "p_RegMobileNo":jQuery("#RegMblNo").val(),
    "p_IpAddress": sessionStorage.getItem("IpAddress"),
	"p_UserId":"1"
	}
	jQuery.ajax({
		url: serverpath + 'JobAlert',
		type: "POST",
		data: JSON.stringify(MasterData),
		contentType: "application/json; charset=utf-8",
		success: function (data, status, jqXHR) {
			resetMode();
			if (data[0][0].ReturnValue == "1") {
				
			  toastr.success("Submit Successfully", "", "success")
			  return true;
			}
		},
		error: function (xhr) {
		}
	});
}

function resetMode() {
    $("#btnsubmit").html('Submit');
    jQuery("#WatchWords").val("");
     jQuery("#Location").val("");
     jQuery("#WorkExperince").val("");
     jQuery("#WorkExperinceMnth").val("");
     jQuery("#ExpectedSalary").val("");
     jQuery("#Industry").val(""); 
     jQuery("#Category").val("");
     jQuery("#Role").val(""); 
     jQuery("#Designation").val("");
     jQuery("#Skill").val(""); 
     jQuery("#RegEmailId").val("");
     jQuery("#RegMblNo").val("");
    }
    function FillLocation() {
        var path = serverpath + "district/0/0/0/0"
        ajaxget(path, 'parsedataFillLocation', 'comment', 'control');
    }
        function parsedataFillLocation(data) {
            data = JSON.parse(data)
            if (data.length > 0) {
                sessionStorage.setItem('DistrictName', JSON.stringify(data[0]));
            }
           
               
            
        }
    
    jQuery("#Location").keyup(function () {
        sessionStorage.setItem('DistrictId', '0');
    });
    
    jQuery("#Location").typeahead({
        source: function (query, process) {
            var data = sessionStorage.getItem('DistrictName');
            districtname = [];
            map = {};
            var Location = "";
            jQuery.each(jQuery.parseJSON(data), function (i, Location) {
                map[Location.DistrictName] = Location;
                districtname.push(Location.DistrictName);
            });
            process(districtname);
        },
        minLength: 3,
        updater: function (item) {
            sessionStorage.setItem('DistrictId', map[item].DistrictId);
            return item;
        }
    });
    function FillExperienceYear() {
        var path =  serverpath + "year/0"
        ajaxget(path,'parsedataFillExperienceYear','comment','control');
    }
    function parsedataFillExperienceYear(data){  
        data = JSON.parse(data)
        var data1 = data[0];
                jQuery("#WorkExperince").append(jQuery("<option ></option>").val("0").html("Select Experience in Year"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#WorkExperince").append(jQuery("<option></option>").val(data1[i].FinYearId).html(data1[i].Year));
                 }
            
                  
    }
    function FillCategoryJobAlert(funct){
        var path = serverpath + "category/0/0/0/0"
        ajaxget(path,funct,'comment','control');
      
    }
    function parsedataCategoryJobAlert(data){
        data = JSON.parse(data)
      
            var data1 = data[0];
            jQuery("#CategoryAlert").empty();
            jQuery("#CategoryAlert").append(jQuery("<option ></option>").val("0").html("Select Categories"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#CategoryAlert").append(jQuery("<option></option>").val(data1[i].CategoryId).html(data1[i].CategoryName));
            
        }
    }
    function FillRoleJobAlert(funct){
        var path = serverpath + "role/0/0/0/0"
        ajaxget(path,funct,'comment','control');
      
    }
    function parsedataRoleJobAlert(data){
        data = JSON.parse(data)
      
            var data1 = data[0];
            jQuery("#Role").empty();
            jQuery("#Role").append(jQuery("<option ></option>").val("0").html("Select Role"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#Role").append(jQuery("<option></option>").val(data1[i].RoleId).html(data1[i].RoleName));
            
        }
    }
    function FillDesignationJobAlert(funct){
        var path = serverpath + "designation/0/0/0/0"
        ajaxget(path,funct,'comment','control');
      
    }
    function parsedataDesignationJobAlert(data){
        data = JSON.parse(data)
      
            var data1 = data[0];
            jQuery("#Designation").empty();
            jQuery("#Designation").append(jQuery("<option ></option>").val("0").html("Select Designation"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#Designation").append(jQuery("<option></option>").val(data1[i].DesignationId).html(data1[i].DesignationName));
            
        }
    }