function DistrictOffice() {

    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "districtoffice",
        cache: false,
        dataType: "json",
        success: function (data) {

            var cacheArr = [];
            for (var i = 0; i < data[0].length; i++) {
                if (cacheArr.indexOf(data[0][i].DivisionCallCentre) >= 0) {
                    continue;
                }
                cacheArr.push(data[0][i].DivisionCallCentre);
              
                    var cityData = " <div class='col-md-6 column' ><li><div class='contact-now'><div class='contact'  onclick=DistrictDetail('" + data[0][i].DistrictSystemId + "','" + encodeURI(data[0][i].DivisionCallCentre) + "','" + encodeURI(data[0][i].Administration) + "','" + encodeURI(data[0][i].CentreManager) + "') data-toggle='modal' data-target='#myModalcontact'><span class='homeicon'><i class='fa fa-home'></i></span> <div class='information'><div class='Contactus'>District&nbspOffice:-<span class='DivisionCentre'>" + data[0][i].DivisionCallCentre + "</span></div></div><br><span class='homeicon'><i class='fa fa-user'></i></span> <div class='information'><div class='Contactus'>Manager:-<span class='DivisionCentre'>" + data[0][i].CentreManager + "</span></div></div><br><span class='homeicon'><i class='fa fa-phone'></i></span><div class='information'> <div class='Contactus'>Phone:<span class='DivisionCentre'>" + data[0][i].Contact + "</span></div></div><br><span class='homeicon'><i class='fa fa-envelope-o'></i></span> <div class='information'><div class='Contactus'>EMail:-<span class='DivisionCentre'>" + data[0][i].EMailId + "</span></div></div><br></div></div></div></li></div>";
                    $('#AllDistrictOffice').append($(cityData));
                

            }
        },
        error: function (xhr) {
            //swal(xhr.d, "", "error")
        }
    });


}

function DistrictDetail(DistrictId, DivisionCallCentre, Administration, CentreManager) {

    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "districtofficedetail/" + DistrictId + "",
        cache: false,
        dataType: "json",
        success: function (data) {
            var appenddata ="<li class='row'><div class='col-md-6 col-xs-6'>Name Of Zonal</div><div class='col-md-6 col-xs-6'><span>"+decodeURI(DivisionCallCentre)+"</span></div></li><li class='row'><div class='col-md-6 col-xs-6'>Administration</div><div class='col-md-6 col-xs-6'><span style='    width: 102%'>"+decodeURI(Administration)+"</span></div></li><li class='row'><div class='col-md-6 col-xs-6'>Districts Covered</div><div class='col-md-6 col-xs-6'><span>"+data[0][0].DistrictCovered+"</span></div></li><li class='row'><div class='col-md-6 col-xs-6'>Centre Manager</div> <div class='col-md-6 col-xs-6'><span>"+decodeURI(CentreManager)+"</span></div></li>";
            jQuery("#districtdetail").html(appenddata);

        },
        error: function (xhr) {
            //swal(xhr.d, "", "error")
        }
    });


}


