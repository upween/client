
 function CheckPersonalDetail(){
  if(isValidate==true){

  
     var PinNo =document.getElementById('Pin');
     if (jQuery('#DOBtextbox').val() == "") {
       $('#myModal1').scrollTop(0);
       getvalidated('DOBtextbox','text','Date of birth');
      return false;
     }
     if (jQuery('#DOBtextbox').val() != "") {
       DOBCheck()

       if (DOBCheck()==false) {
         $('#myModal1').scrollTop(0);
           $("#validDOBtextbox").html("Date of birth should be 14+");
           jQuery('#DOBtextbox').css('border-color', 'red');
           return false;
       }
       else{
         $("#validDOBtextbox").html("");
         jQuery('#DOBtextbox').css('border-color', '');
       }
     }
   if (jQuery('#resident').val() == "0") {
     $('#myModal1').scrollTop(0);
     getvalidated('resident','select','Domicile');

     return false;
   }
 
   if (jQuery('#permanentaddress').val() == "") {
     $('#myModal1').scrollTop(0);
     getvalidated('permanentaddress','text','Permanent Address');
     return false;
  }
 if (jQuery('#presentaddress').val() == "") {
   $('#myModal1').scrollTop(0);
   getvalidated('presentaddress','text','Present Address');
   return false;
  }
if (jQuery('#Pin').val() == '') {
 $('#myModal1').scrollTop(0);
 getvalidated('Pin','text','Pin');
 return false;
  }
  if(PinNo.value.length != 6){
   $('#myModal1').scrollTop(0);
   checkLength('Pin','Pin','6');
   return false;
}
if (jQuery('#Category').val() == "0") {
 getvalidated('Category','select','Category');
 return false;
  }

if (jQuery('#Religion').val() == '0') {
 getvalidated('Religion','select','Religion');
 return false;
}
if (jQuery('#SubCaste').val() == '') {
 getvalidated('SubCaste','text','Sub-Caste');
 return false;
}
if (jQuery('#nationality').val() == "0") {
 getvalidated('nationality','select','Nationality');
 return false;
  }

if (jQuery('#Proficiency').val() == "0") {
 getvalidated('Proficiency','select','Employed Status');
 return false;
  }
if (jQuery('#differentlyabled').val() == "0") {
 getvalidated('differentlyabled','select','Physical Handicapped');
 return false;
}


else { 
 if(jQuery('#differentlyabled').val()=='Handicap' ||$("#resident" ).val() =="1"){
   $("#UploadTab").show()
  }
  else{
   $("#UploadTab").hide()
  }
 InsUpdPersonalDetail();
}
  }
  else{
   InsUpdPersonalDetail();
  }
    }
   function InsUpdPersonalDetail(){
     var chkval = $("#chkgreencardStatus")[0].checked
     if (chkval == true){
         chkval = "Yes"
     }else{
         chkval="No"
     }
     $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);

     var MasterData = {
       "p_PersonalDetailsId": localStorage.getItem("PersonalDetailId"),
       "p_CandidateId": sessionStorage.getItem("CandidateId"),
       "p_DateOfBirth": $("#DOBtextbox").val(),
       "p_Resident": $("#resident").val(),
       "p_PermanentAddress": $("#permanentaddress").val(),
       "p_PresentAddress": $("#presentaddress").val(),
       "p_PinCode":$("#Pin").val(),
       "p_BloodGroup": $("#BloodGroup").val(),
       "p_MaritalStatus":$("#Marital").val(),
       "p_Category":$("#Category option:selected").val(),
       "p_Religion":$("#Religion option:selected").val(),
       "p_SubCaste":$("#SubCaste").val(),
       "p_Nationality":$("#nationality option:selected").val(),
       "p_Language":'1',
       "p_Proficiency":$("#Proficiency").val(),
       "p_DifferentlyAbled":$("#differentlyabled option:selected").val(),
       "p_DifferentlyAbledType":$("#DisabilityDetail option:selected").val(),
       "p_DiffAbledSubType":$("#HandicappedSubCategory option:selected").val(),
       "p_DisablityPriority":$("#DisablityPriority option:selected").val(),
       "p_Area":$("#Area").val(),
       "p_Block":$("#Block option:selected").val(),
       "p_Panchayat":$("#Panchayat option:selected").val(),
       "p_Village":"",
       "p_AreaOfInterest":$("#AreaOfInterest").val(),
       "p_GreenCard":chkval,
       "p_IpAddress":sessionStorage.getItem("IpAddress"),
       "p_UserId":sessionStorage.getItem("CandidateId")
     }
   
     MasterData = JSON.stringify(MasterData)
     var path = serverpath + "secured/personaldetails"; 
     securedajaxpost(path,'parsrdatapersonaldetails','comment',MasterData,'control')
 }
 function parsrdatapersonaldetails(data){
     data = JSON.parse(data)
     if (data.message == "New token generated"){
       sessionStorage.setItem("token", data.data.token);
       InsUpdPersonalDetail();
   }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

     if (data[0][0].ReturnValue == "1") {
       toastr.success("Submit Successfully", "", "success")
       ProfileStatus('parsedataProfileStatus','progressbar');
       FillPersonalDetailSecured('parsedatasecuredpersonaldetails','personaldetail');
       $('#myModal1').modal('toggle');
       }
      else if (data[0][0].ReturnValue == "2") {
       ProfileStatus('parsedataProfileStatus','progressbar');
       FillPersonalDetailSecured('parsedatasecuredpersonaldetails','personaldetail');
       $('#myModal1').modal('toggle');
       toastr.success("Submit Successfully", "", "success")
      }
     }
    
    
                 
     // }
      function FillPersonalDetailSecured(funct,control) {
       var path = serverpath + "secured/personaldetails/'"+sessionStorage.getItem("CandidateId")+"'/'"+sessionStorage.getItem("IpAddress")+"'/'"+sessionStorage.getItem("CandidateId")+"'"
       securedajaxget(path,funct,'comment',control);
   }
   function parsedatasecuredpersonaldetails(data,control){  
       data = JSON.parse(data)
       if (data.message == "New token generated"){
           sessionStorage.setItem("token", data.data.token);
           FillPersonalDetailSecured('parsedatasecuredpersonaldetails','personaldetail');
       }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
     //  else if (errordata.status == 401){
          //     toastr.warning("Unauthorized", "", "info")
             //  return true;
           //}
       else{
       
           if (data[0]==""){
               localStorage.setItem("PersonalDetailId","0");
              jQuery("#"+control).html("<div class='col-md-12' style='    margin-left: 2%;' lang='en'>   Add Personal Detail</div>");
               $("#EmpSalary").hide();
           }
           else{
            // ProfileStatus('parsedataProfileStatus','progressbar');
           var data1 = data[0];
         
           var appenddata="";
           localStorage.setItem("PersonalDetailId",data1[0].PersonalDetailsId)
           jQuery("#"+control).empty();

           

           if(data1[0].bloodgroup_name =='0' || data1[0].bloodgroup_name ==null) {
             var Bloodgroup_name="-";                
         }
         else{
             var Bloodgroup_name= data1[0].bloodgroup_name ;
         }

         if(data1[0].Marital_Status =='0' || data1[0].Marital_Status ==null){
           var marital_Status="-";
       }
       else{
           var marital_Status= data1[0].Marital_Status ;   
       }
       if(data1[0].Area_Name =='0' || data1[0].Area_Name ==null){
         var area_Name="-";
     }
     else{
         var area_Name= data1[0].Area_Name ;
     }
       if(data1[0].AreaOfInterest =='0' || data1[0].AreaOfInterest ==null){
       var areaOfInterest="-";
        }
        else{
       var areaOfInterest= data1[0].AreaOfInterest ;
        }

        
        if(data1[0].DomicileDocs ==''|| data1[0].Resident_id!='1' || data1[0].DomicileDocs ==null){
         var domicileDocs="";
          }
          else{
           var domicileDocs= "(<a href='Domicile/"+data1[0].DomicileDocs+"' download><span class='profilesubheading'>"+data1[0].DomicileDocs+"</span></a>)";
         
         }

          if(data1[0].HandicappedDocs ==''|| data1[0].DifferentlyAbled!='Handicap' || data1[0].HandicappedDocs ==null || data1[0].HandicappedDocs ==undefined){
           var handicappedDocs="";
            }
            else{
           var handicappedDocs= "(<a href='HandicappedDoc/"+data1[0].HandicappedDocs+"' download><span class='profilesubheading'>"+data1[0].HandicappedDocs+"</span></a>)" ;
           $("#UploadTab").show()
          
           $("#uploadhandicapdoc").show()
         }
   if(data1[0].Resident_id=='1'){
     $(window).scrollTop("UploadTab");
           $("#UploadTab").show()
     $("#uploaddomiciledoc").show()
   }
   if(data1[0].DifferentlyAbled=='Handicap'){
     $(window).scrollTop("UploadTab");
     $("#UploadTab").show()
     $("#uploadhandicapdoc").show()
     var DiffAbled='Yes'
   }
   else{
    var DiffAbled='No'
   }
           appenddata+="<div class='col-md-12'><div class='col-md-4'><p class='profilesubheading'><span  lang='en'>Date of Birth</span></p><p class='profiledetail'>"+data1[0].DateOfBirth+"</p></div>"+
           "<div class='col-md-4'><p class='profilesubheading'><span  lang='en'>Domicile</span></p><p class='profiledetail'>"+data1[0].Resident+' '+domicileDocs+"</p></div>"+
           "<div class='col-md-4'><p class='profilesubheading'><span  lang='en'>Permanent Address</span></p><p class='profiledetail' style='word-break: break-word;'>"+data1[0].PermanentAddress+"</p></div></div>"+
           
           "<div class='col-md-12'>"+
           "<div class='col-md-4'><p class='profilesubheading'><span  lang='en'>Pin&nbsp</span></p><p class='profiledetail' >"+data1[0].PinCode+"</p></div>"+
           "<div class='col-md-4'><p class='profilesubheading'><span  lang='en'>Blood Group</span></p><p class='profiledetail'>"+Bloodgroup_name+" </p></div>"+
           "<div class='col-md-4'><p class='profilesubheading'><span  lang='en'>Marital Status</span></p><p class='profiledetail'>"+marital_Status+" </p></div></div>"+
         
           "<div class='col-md-12'>"+
           "<div class='col-md-4'><p class='profilesubheading'><span  lang='en'>Category</span></p><p class='profiledetail'>"+data1[0].Category+" </p></div>"+
           "<div class='col-md-4'><p class='profilesubheading'><span  lang='en'>Nationality</span></p><p class='profiledetail'>"+data1[0].Nationality+" </p></div>"+
           "<div class='col-md-4'><p class='profilesubheading'><span  lang='en'>Religion</span></p><p class='profiledetail'>"+data1[0].Religion+"</p></div></div>"+
         
           "<div class='col-md-12'>"+
           "<div class='col-md-4'><p class='profilesubheading'><span  lang='en'>Sub-Caste</span></p><p class='profiledetail'>"+data1[0].SubCaste+"</p></div>"+
           "<div class='col-md-4'><p class='profilesubheading'><span  lang='en'>Differently abled</span></p><p class='profiledetail'>"+DiffAbled+' '+handicappedDocs+"</p></div>"+
           "<div class='col-md-4'><p class='profilesubheading'><span  lang='en'>Area</span></p><p class='profiledetail'>"+area_Name+"</p></div></div>"+
          
           "<div class='col-md-12'>"+
          "<div class='col-md-4'><p class='profilesubheading'><span  lang='en'>Area Of Interest</span></p><p class='profiledetail'>"+areaOfInterest+"</p></div>"+
           "<div class='col-md-4'><p class='profilesubheading'><span  lang='en'>Green Card</span></p><p class='profiledetail'>"+data1[0].GreenCard+"</p></div></div></div>";
           jQuery("#"+control).html(appenddata);
           jQuery("#personaldetailupdateon").html("<span lang='en'>Updated On</span> "+data1[0].UpdateOn);
           $("#DomicileFileName").text(data1[0].DomicileDocs);
            $("#DOBtextbox").val(data1[0].DateOfBirth)
            $("#resident").val(data1[0].Resident_id)
            $("#permanentaddress").val(data1[0].PermanentAddress),
            $("#presentaddress").val(data1[0].PresentAddress),
            $("#Pin").val(data1[0].PinCode),
            $("#BloodGroup").val(data1[0].BloodGroup),
            $("#Marital").val(data1[0].MaritalStatus),
            $("#Category").val(data1[0].CategoryId),
            $("#Religion").val(data1[0].Religion_id),
            $("#SubCaste").val(data1[0].SubCaste),
            $("#nationality").val(data1[0].Nationality_id),
            $("#Area").val(data1[0].Area),
            $("#Proficiency").val(data1[0].Proficiency),
            $("#differentlyabled").val(data1[0].DifferentlyAbled),
            $("#DisabilityDetail").val(data1[0].DifferentlyAbledType)
            $("#HandicappedSubCategory").val(data1[0].DiffAbledsubType)
            $("#DisablityPriority").val(data1[0].DisablityPriority)
            $("#AreaOfInterest").val(data1[0].AreaInterest)
         
            $("#DOBtextbox").prop( "disabled", 'disabled' );

           if(sessionStorage.getItem('PPAddress')=='No'){
            $("#noAdd").prop("checked",true )
           }
            else if(sessionStorage.getItem('PPAddress')=='Yes'){
           $("#yesAdd").prop("checked",true )
    }

            if (data1[0].GreenCard =="Yes"){
                $("#chkgreencardStatus").prop("checked",true )
            }

          

           if(data1[0].DifferentlyAbled=="Handicap"){
             $("#DisabilityType").show(); 
             $("#DisablityPrio").show();
             
              }
           else{
             $("#DisabilityType").hide(); 
             $("#DisablityPrio").hide();
           }

           if(data1[0].Resident_id=="1"){
             $("#Domicilehide").show(); 
             $("#Domicilenamehide").show();
             
              }
           else{
             $("#Domicilehide").hide(); 
             $("#Domicilenamehide").hide();
           }
           
         getvalidated('permanentaddress','text','Permanent Address');
         getvalidated('presentaddress','text','Present Address');
         getvalidated('DOBtextbox','text','Date of Birth');
         getvalidated('differentlyabled','select','Physical Handicapped');
         getvalidated('resident','select','Domicile');
         getvalidated('Proficiency','select','Employed Status');
         getvalidated('nationality','select','Nationality');
         getvalidated('SubCaste','text','Sub-Caste');
         getvalidated('Religion','select','Religion');
         checkLength('Pin','Pin','6');
         getvalidated('Pin','text','Pin');
      // ProfileStatus('parsedataProfileStatus','progressbar');
   }
   }
 }


function FillDistrictPersonal() {
 jQuery.ajax({
     type: "GET",
     contentType: "application/json; charset=utf-8",
     url: serverpath + "district/0/0/0/0",
     cache: false,
     dataType: "json",
     success: function (data) {
         jQuery("#districtP").empty();
         jQuery("#districtP").append(jQuery("<option ></option>").val("0").html("Select District"));
         for (var i = 0; i < data[0].length; i++) {
             jQuery("#districtP").append(jQuery("<option></option>").val(data[0][i].DistrictId).html(data[0][i].DistrictName));
         }
     },
     error: function (xhr) {
        if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    }
 });
}
jQuery('#districtP').on('change', function () {
 FillCity(jQuery('#districtP').val());
});

function FillCity(district) {
 jQuery.ajax({
     type: "GET",
     contentType: "application/json; charset=utf-8",
     url: serverpath + "city/" + district + "/0/0/0",
     cache: false,
     dataType: "json",
     success: function (data) {
         var data1 = data[0];
         jQuery("#cityP").empty();
         jQuery("#cityP").append(jQuery("<option ></option>").val("0").html("Select City"));
         for (var i = 0; i < data1.length; i++) {
             jQuery("#cityP").append(jQuery("<option></option>").val(data1[i].CityId).html(data1[i].CityName));
         }
     },
     error: function (xhr) {
        if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    }
 });
}
function DOBCheck(){
if (jQuery('#DOBtextbox').val() != '') {
 var dob1 = jQuery('#DOBtextbox').val().split('/');
 var dob =dob1[2]+'-'+dob1[1]+'-'+dob1[0]
 //var d1 = new Date();
 //var d2 = new Date(parseInt(dob));
 //var years = (d1.getFullYear() - d2.getFullYear()-1970);
 var years = new Date(new Date() - new Date(dob)).getFullYear() - 1970;
 if (years < 14) {
     $("#validDOBtextbox").html("Date of birth should be 14+");
     jQuery('#DOBtextbox').css('border-color', 'red');
     return false;
 }
 else{
   $("#validDOBtextbox").html("");
   jQuery('#DOBtextbox').css('border-color', '');
   return true;
 }
}
else{
 getvalidated('DOBtextbox','text','Date of Birth');
 return true;
}
}
// var dateToday = new Date();
// var dates = $("#DOBtextbox").datepicker({
//     defaultDate: "+1w",
//     changeMonth: true,
//     numberOfMonths: 1,
//     maxDate: dateToday,
//     onSelect: function(selectedDate) {
//         var option = this.id == "fromdate" ? "minDate" : "",
//             instance = $(this).data("datepicker"),
//             date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
//         dates.not(this).datepicker("option", option, date);
//     }
// });


function InsDelDomicileDocs(){
 
 var MasterData = {
     "p_CandidateId": sessionStorage.getItem("CandidateId"),
     "p_DomicileDocs": sessionStorage.getItem("DomicileFileName"),
     
     
 }
 MasterData = JSON.stringify(MasterData)
 var path = serverpath + "secured/DomicileDocs";
 securedajaxpost(path, 'parsrdataInsDelDomicileDocs', 'comment', MasterData, 'control')
}
function parsrdataInsDelDomicileDocs(data) {
 data = JSON.parse(data)
 if (data.message == "New token generated") {
     sessionStorage.setItem("token", data.data.token);
     InsDelDomicileDocs();

 }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

 else {
  
    sessionStorage.setItem("OpenModal",'');
     sessionStorage.setItem("DomicileFileName","") ;
     FillPersonalDetailSecured('parsedatasecuredpersonaldetails','personaldetail');
   
 }

}



function UpdHandicappedDoc(){
 
 var MasterData = {
     "p_CandidateId": sessionStorage.getItem("CandidateId"),
     "p_HandicappedDocs": sessionStorage.getItem("HandicappedFileName"),
     
     
 }
 MasterData = JSON.stringify(MasterData)
 var path = serverpath + "secured/HandicappedDocs";
 securedajaxpost(path, 'parsrdataUpdHandicappedDoc', 'comment', MasterData, 'control')
}
function parsrdataUpdHandicappedDoc(data) {
 data = JSON.parse(data)
 if (data.message == "New token generated") {
     sessionStorage.setItem("token", data.data.token);
    UpdHandicappedDoc();

 }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

 else {
  
    sessionStorage.setItem("OpenModal",'');
     sessionStorage.setItem("HandicappedFileName","") ;
    FillPersonalDetailSecured('parsedatasecuredpersonaldetails','personaldetail');
   
 }

}

function FillMaritalstatus(funct,control) {
 var path =  serverpath + "secured/Maritalstatus/0/0"
 securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillMaritalstatus(data,control){  
 data = JSON.parse(data)
 if (data.message == "New token generated"){
     sessionStorage.setItem("token", data.data.token);
     FillMaritalstatus('parsedatasecuredFillMaritalstatus','Marital');
 }
 else if (data.status == 401){
     toastr.warning("Unauthorized", "", "info")
     return true;
 }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

     else{
         jQuery("#"+control).empty();
         var data1 = data[0];
         jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Marital Status"));
         for (var i = 0; i < data1.length; i++) {
             jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Marital_id).html(data1[i].Marital_Status));
          }
     }
           
}

function FillBloodGroup(funct,control) {
 var path =  serverpath + "secured/BloodGroup/0/0"
 securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillBloodGroup(data,control){  
 data = JSON.parse(data)
 if (data.message == "New token generated"){
     sessionStorage.setItem("token", data.data.token);
     FillBloodGroup('parsedatasecuredFillBloodGroup','BloodGroup');
 }
 else if (data.status == 401){
     toastr.warning("Unauthorized", "", "info")
     return true;
 }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

     else{
         jQuery("#"+control).empty();
         var data1 = data[0];
         jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Blood Group"));
         for (var i = 0; i < data1.length; i++) {
             jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].bloodgroup_id).html(data1[i].bloodgroup_name));
          }
     }
           
}

function FillCategory(funct,control) {
 var path =  serverpath + "secured/category/0/0/0/0"
 securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillCategory(data,control){  
 data = JSON.parse(data)
 if (data.message == "New token generated"){
     sessionStorage.setItem("token", data.data.token);
     FillCategory('parsedatasecuredFillCategory','Category');
 }
 else if (data.status == 401){
     toastr.warning("Unauthorized", "", "info")
     return true;
 }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

     else{
         jQuery("#"+control).empty();
         var data1 = data[0];
         jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Category"));
         for (var i = 0; i < data1.length; i++) {
             jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].CategoryId).html(data1[i].CategoryName));
          }
     }
           
}

function FillReligion(funct,control) {
 var path =  serverpath + "secured/Religion/0/0"
 securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillReligion(data,control){  
 data = JSON.parse(data)
 if (data.message == "New token generated"){
     sessionStorage.setItem("token", data.data.token);
     FillReligion('parsedatasecuredFillReligion','Religion');
 }
 else if (data.status == 401){
     toastr.warning("Unauthorized", "", "info")
     return true;
 }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

     else{
         jQuery("#"+control).empty();
         var data1 = data[0];
         jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Religion"));
         for (var i = 0; i < data1.length; i++) {
             jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Religion_id).html(data1[i].Religion_Name));
          }
     }
           
}

function FillNationality(funct,control) {
 var path =  serverpath + "secured/Nationality/0/0"
 securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillNationality(data,control){  
 data = JSON.parse(data)
 if (data.message == "New token generated"){
     sessionStorage.setItem("token", data.data.token);
     FillNationality('parsedatasecuredFillNationality','nationality');
 }
 else if (data.status == 401){
     toastr.warning("Unauthorized", "", "info")
     return true;
 }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

     else{
         jQuery("#"+control).empty();
         var data1 = data[0];
         jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Nationality"));
         for (var i = 0; i < data1.length; i++) {
             jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Nationality_id).html(data1[i].Nationality_name));
          }
     }
           
}

function FillAreaOfInterest(funct,control) {
 var path =  serverpath + "secured/AreaOfInterest/0/0"
 securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillAreaOfInterest(data,control){  
 data = JSON.parse(data)
 if (data.message == "New token generated"){
     sessionStorage.setItem("token", data.data.token);
     FillAreaOfInterest('parsedatasecuredFillAreaOfInterest','AreaOfInterest');
 }
 else if (data.status == 401){
     toastr.warning("Unauthorized", "", "info")
     return true;
 }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

     else{
         jQuery("#"+control).empty();
         var data1 = data[0];
         jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Area Of Interest"));
         for (var i = 0; i < data1.length; i++) {
             jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].AreaOfInterest_id).html(data1[i].AreaOfInterest));
          }
     }
           
}

function FillEmploymentstatus(funct,control) {
 var path =  serverpath + "secured/Employmentstatus/0/0"
 securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillEmploymentstatus(data,control){  
 data = JSON.parse(data)
 if (data.message == "New token generated"){
     sessionStorage.setItem("token", data.data.token);
     FillEmploymentstatus('parsedatasecuredFillEmploymentstatus','Proficiency');
 }
 else if (data.status == 401){
     toastr.warning("Unauthorized", "", "info")
     return true;
 }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

     else{
         jQuery("#"+control).empty();
         var data1 = data[0];
         jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Employed Status"));
         for (var i = 0; i < data1.length; i++) {
             jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].jobstatus_id).html(data1[i].Employment_Status));
          }
     }
           
}

function FillResident(funct,control) {
 var path =  serverpath + "secured/Resident/0/0"
 securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillResident(data,control){  
 data = JSON.parse(data)
 if (data.message == "New token generated"){
     sessionStorage.setItem("token", data.data.token);
     FillResident('parsedatasecuredFillResident','resident');
 }
 else if (data.status == 401){
     toastr.warning("Unauthorized", "", "info")
     return true;
 }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

     else{
         jQuery("#"+control).empty();
         var data1 = data[0];
         jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Domicile"));
         for (var i = 0; i < data1.length; i++) {
             jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Resident_id).html(data1[i].Resident_name));
          }
     }
           
}

function FillHCCategory(funct,control) {
 var path =  serverpath + "secured/HCCategory/0/0"
 securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillHCCategory(data,control){  
 data = JSON.parse(data)
 if (data.message == "New token generated"){
     sessionStorage.setItem("token", data.data.token);
     FillHCCategory('parsedatasecuredFillHCCategory','DisabilityDetail');
 }
 else if (data.status == 401){
     toastr.warning("Unauthorized", "", "info")
     return true;
 }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

     else{
         jQuery("#"+control).empty();
         var data1 = data[0];
         jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Handicapped Category"));
         for (var i = 0; i < data1.length; i++) {
             jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].HC_Category_id).html(data1[i].HC_Description));
          }
     }
           
}

function FillHCsubCategory(funct,control) {
 var path =  serverpath + "secured/HCsubCategory/0/0"
 securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillHCsubCategory(data,control){  
 data = JSON.parse(data)
 if (data.message == "New token generated"){
     sessionStorage.setItem("token", data.data.token);
     FillHCsubCategory('parsedatasecuredFillHCsubCategory','HandicappedSubCategory');
 }
 else if (data.status == 401){
     toastr.warning("Unauthorized", "", "info")
     return true;
 }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

     else{
         jQuery("#"+control).empty();
         var data1 = data[0];
         jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Handicapped Sub Category"));
         for (var i = 0; i < data1.length; i++) {
             jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].HC_SubCategory_id).html(data1[i].HC_Category_Description));
          }
     }
           
}

function FillDisablityPriority(funct,control) {
 var path =  serverpath + "secured/DisablityPriority/0/0"
 securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillDisablityPriority(data,control){  
 data = JSON.parse(data)
 if (data.message == "New token generated"){
     sessionStorage.setItem("token", data.data.token);
     FillDisablityPriority('parsedatasecuredFillDisablityPriority','DisablityPriority');
 }
 else if (data.status == 401){
     toastr.warning("Unauthorized", "", "info")
     return true;
 }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

     else{
         jQuery("#"+control).empty();
         var data1 = data[0];
         jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Disablity Priority"));
         for (var i = 0; i < data1.length; i++) {
             jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].DisablityPriority_id).html(data1[i].DisablityPriority_name));
          }
     }
           
}

function FillArea(funct,control) {
 var path =  serverpath + "secured/Area/0/0"
 securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillArea(data,control){  
 data = JSON.parse(data)
 if (data.message == "New token generated"){
     sessionStorage.setItem("token", data.data.token);
     FillArea('parsedatasecuredFillArea','Area');
 }
 else if (data.status == 401){
     toastr.warning("Unauthorized", "", "info")
     return true;
 }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

     else{
         jQuery("#"+control).empty();
         var data1 = data[0];
         jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Area"));
         for (var i = 0; i < data1.length; i++) {
             jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Area_Id).html(data1[i].Area_Name));
          }
     }
           
}

function FillBlock(funct,control) {
 var path =  serverpath + "secured/city/0/0/0/0"
 securedajaxget(path,funct,'comment',control);
}
function parsedatasecuredFillBlock(data,control){  
 data = JSON.parse(data)
 if (data.message == "New token generated"){
     sessionStorage.setItem("token", data.data.token);
     FillBlock('parsedatasecuredFillBlock','Block');
 }
 else if (data.status == 401){
     toastr.warning("Unauthorized", "", "info")
     return true;
 }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

     else{
         jQuery("#"+control).empty();
         var data1 = data[0];
         jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Block"));
         for (var i = 0; i < data1.length; i++) {
             jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].CityId).html(data1[i].CityName));
          }
     }
           
}


jQuery('#Block').on('change', function () {
 FillPanchayat('parsedatasecuredFillPanchayat','Panchayat',jQuery('#Block').val());
});

function FillPanchayat(funct,control,city) {
 var path =  serverpath + "secured/village/" + city + "/0/0/0"
 securedajaxget(path,funct,'comment',control);
}
function parsedatasecuredFillPanchayat(data,control){  
 data = JSON.parse(data)
 if (data.message == "New token generated"){
     sessionStorage.setItem("token", data.data.token);
     FillPanchayat('parsedatasecuredFillPanchayat','Panchayat',jQuery('#Block').val());
 }
 else if (data.status == 401){
     toastr.warning("Unauthorized", "", "info")
     return true;
 }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

     else{
         jQuery("#"+control).empty();
         var data1 = data[0];
         jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Panchayat"));
         for (var i = 0; i < data1.length; i++) {
             jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].VillageId).html(data1[i].VillageName));
          }
     }
           
}


   $("#Pin").focusout(function(){
     if($("#Pin").val()==''){
       getvalidated('Pin','text','Pin');
     }
     else{
       checkLength('Pin','Pin','6')
     }
   })


$('input[type="radio"]').click(function () {
 if ($(this).attr('id') == 'yesAdd') {
   document.getElementById('presentaddress').value = document.getElementById('permanentaddress').value;
   sessionStorage.setItem('PPAddress','Yes')
 }
 else if ($(this).attr('id') == 'noAdd') {
     $("#presentaddress").val("")
     sessionStorage.setItem('PPAddress','No')
     }
});



function personaldetailAPI(){
    FillDistrictPersonal();
    FillPersonalDetailSecured('parsedatasecuredpersonaldetails','personaldetail');
    FillMaritalstatus('parsedatasecuredFillMaritalstatus', 'Marital');
    FillBloodGroup('parsedatasecuredFillBloodGroup','BloodGroup');
    FillCategory('parsedatasecuredFillCategory','Category');
    FillReligion('parsedatasecuredFillReligion','Religion');
    FillNationality('parsedatasecuredFillNationality','nationality');
    FillAreaOfInterest('parsedatasecuredFillAreaOfInterest','AreaOfInterest');
    FillEmploymentstatus('parsedatasecuredFillEmploymentstatus','Proficiency');
    FillResident('parsedatasecuredFillResident','resident');
    FillHCCategory('parsedatasecuredFillHCCategory','DisabilityDetail');
    FillHCsubCategory('parsedatasecuredFillHCsubCategory','HandicappedSubCategory');
    FillDisablityPriority('parsedatasecuredFillDisablityPriority','DisablityPriority');
    FillArea('parsedatasecuredFillArea','Area');
    FillBlock('parsedatasecuredFillBlock','Block');

}