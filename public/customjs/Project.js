function CheckValidationproject() {
    if(isValidate){
     
        if ($("#prjtStatus").val() == "1") {
    if ($.trim(jQuery('#ProjectTitle').val()) == '') {
        getvalidated('ProjectTitle','text','Project Title');
        return false;
    }
   
    if (jQuery('#EmployementOREdu').val() == '0') {
        getvalidated('EmployementOREdu','select','Emp/Education');
        return false;
    }
    
    if ($.trim(jQuery('#Clientname').val()) == '') {
        getvalidated('Clientname','text','Client Name');
        return false;
    }
    
   
    if (jQuery('#workingfromyear').val() == '0') {
        getvalidated('workingfromyear','select','Work From in Year');
        return false;
     }
    if (jQuery('#workingfrommonth').val() == '0') {
        getvalidated('workingfrommonth','select','Work From in Month');
        return false;
    }
    if ($.trim(jQuery('#comments').val()) == '') {
        getvalidated('comments','text','Comment');
        return false;
     
    }
    else{
        InsUpdProject();  
     } 
    }
  

if ($("#prjtStatus").val() == "2" ||$("#prjtStatus").val() == "0" ) {
    if ($.trim(jQuery('#ProjectTitle').val()) == '') {

        getvalidated('ProjectTitle','text','Project Title');
        return false;
    }
   
    if (jQuery('#EmployementOREdu').val() == '0') {
        getvalidated('EmployementOREdu','select','Emp/Education');
        return false;
    }
    
    if ($.trim(jQuery('#Clientname').val()) == '') {
        getvalidated('Clientname','text','Client Name');
        return false;
    }
    if (jQuery('#prjtStatus').val() == '0') {
        getvalidated('prjtStatus','select','Project Status');
        return false;
     }
   
    if (jQuery('#workingfromyear').val() == '0') {
        getvalidated('workingfromyear','select','Work From in Year');
        return false;
     }
     if (jQuery('#workingfrommonth').val() == '0') {
        getvalidated('workingfrommonth','select','Work From in Month');
        return false;
    }
    if (jQuery('#workedtillyear').val() == '0') {
        getvalidated('workedtillyear','select','WorkTill in Year');
        return false;
    }
    if (jQuery('#workedtillmonth').val() == '0') {
        getvalidated('workedtillmonth','select','WorkTill in Month');
        return false;
    }
    if ($.trim(jQuery('#comments').val()) == '') {
        getvalidated('comments','text','Comment');
        return false;
     
    }
    else{
        InsUpdProject();
     } 
}
    }
    else{
        InsUpdProject();  
     } 

};

function InsUpdProject(){
    $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);
 

if ($('#prjtStatus').val()== "1") {
    var MasterData = {
        
    "p_ProjectId" : localStorage.getItem("ProjectId"),
    "p_CandidateId" : sessionStorage.getItem("CandidateId"),
    "p_ProjectTitle" : jQuery("#ProjectTitle").val(),
    "p_EmploymentOREducation" : jQuery('#EmployementOREdu option:selected').val(),
    "p_ClientName" : jQuery("#Clientname").val(),
    "p_ProjectStatus" : jQuery('#prjtStatus option:selected').val(),
    "p_StartedWorkingFrom" : jQuery('#workingfromyear').val(),
    "p_WorkingFrommnth" : $("#workingfrommonth").val(),
    "p_WorkedTill" : '0',
    "p_WorkedTillMnth" : '0',
    "p_ProjectDetail" : jQuery('#comments').val(),
    "p_IpAddress" : sessionStorage.getItem("IpAddress"),
    "p_UserId" : sessionStorage.getItem("CandidateId"),
    }
}
else if ($('#prjtStatus').val()== "2" ||$("#prjtStatus").val() == "0"){
    var MasterData = {
        
        "p_ProjectId" : localStorage.getItem("ProjectId"),
        "p_CandidateId" : sessionStorage.getItem("CandidateId"),
        "p_ProjectTitle" : jQuery('#ProjectTitle').val(),
        "p_EmploymentOREducation" : jQuery('#EmployementOREdu option:selected').val(),
        "p_ClientName" : jQuery("#Clientname").val(),
        "p_ProjectStatus" : jQuery('#prjtStatus option:selected').val(),
        "p_StartedWorkingFrom" : jQuery('#workingfromyear').val(),
        "p_WorkingFrommnth" : $("#workingfrommonth").val(),
        "p_WorkedTill" : jQuery('#workedtillyear').val(),
        "p_WorkedTillMnth" : jQuery('#workedtillmonth').val(),
        "p_ProjectDetail" : jQuery('#comments').val(),
        "p_IpAddress" : sessionStorage.getItem("IpAddress"),
        "p_UserId" : sessionStorage.getItem("CandidateId"),
        }

}
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/project";
    securedajaxpost(path,'parsrdataInsUpdProject','comment',MasterData,'control')
}

function parsrdataInsUpdProject(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        InsUpdProject();
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else if (data[0][0].ReturnValue == "1") {
        ProfileStatus('parsedataProfileStatus','progressbar');
        $('#myModal1').modal('toggle');
        FillProjectDetail('parsedatasecuredFillProjectDetail','Projectdetail');
        toastr.success("Insert Successful", "", "success")
        
        return true;
    }
    else if (data[0][0].ReturnValue == "2") {
           $('#myModal1').modal('toggle');
           resetModeProject ();
           FillProjectDetail('parsedatasecuredFillProjectDetail','Projectdetail');
          toastr.success("Update Successful", "", "success")
          return true;
      }
      else if (data[0][0].ReturnValue == "0") {
        toastr.warning("Project Detail already Exists", "", "info")
       return false;
   }
     }

     
function resetModeProject() {

    $("#ProjectTitle").prop("disabled", false);
    $("#Clientname").prop("disabled", false);
    jQuery("#ProjectTitle").val(""),
    jQuery("#EmployementOREdu").val("0"),
    jQuery("#Clientname").val(""),
    jQuery("#prjtStatus").val("0")
    jQuery("#workingfromyear").val("0")
    jQuery("#workedtillyear").val("0")
    jQuery("#workingfrommonth").val("0")
    jQuery("#workedtillmonth").val("0")
    jQuery("#comments").val("")

        jQuery('#ProjectTitle').css('border-color', '');
        jQuery('#EmployementOREdu').css('border-color', '');
        jQuery('#Clientname').css('border-color', '');
        jQuery('#prjtStatus').css('border-color', '');
        jQuery('#workingfromyear').css('border-color', '');
        jQuery('#comments').css('border-color', '');
                
        $('#validProjectTitle').html("");
        $("#validEmployementOREdu").html("");
        $("#validClientname").html("");
        $("#validprjtStatus").html("");
        $("#validworkingfromyear").html("");
        $("#validcomments").html("");
        
}


function FillProjectDetail(funct,control) {
var path =  serverpath + "secured/project/'" + sessionStorage.getItem("CandidateId") + "'/'" + sessionStorage.getItem("IpAddress") + "'/'" + sessionStorage.getItem("CandidateId") + "'"
securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillProjectDetail(data,control){  
data = JSON.parse(data)
if (data.message == "New token generated"){
    sessionStorage.setItem("token", data.data.token);
    FillProjectDetail('parsedatasecuredFillProjectDetail','Projectdetail');
}
else if (data.status == 401){
    toastr.warning("Unauthorized", "", "info")
    return true;
}
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

else{
    var proupdatedon = "";
    if(data[0]=="")
    {
       localStorage.setItem("ProjectId","0")
        jQuery("#"+control).html("<span lang='en'>Add Project Detail</span>");
    }
    else{
    var data1 = data[0];
    localStorage.setItem("ProjectId",data[0][0].ProjectId)
    var appenddata="";
    jQuery("#"+control).empty();
    for (var i = 0; i < data[0].length; i++) {
        if(data1[i].ProjectStatus=="1"){
            appenddata += "<p class='profiledetail'style='font-weight: bold;padding-top: 15px;'>" + data1[i].ProjectTitle + "<span style='margin-left:10px;'><a href='#Projecttab'><i class='fa fa-pencil' onclick=EditProjectDetail('"+data1[i].ProjectId+"','"+encodeURI(data1[i].ProjectTitle)+"','"+encodeURI(data1[i].EmploymentOREducation)+"','"+encodeURI(data1[i].ClientName)+"','"+encodeURI(data1[i].ProjectStatus)+"','"+encodeURI(data1[i].StartedWorkingFrom)+"','"+encodeURI(data1[i].WorkingFrommnth)+"','"+encodeURI(data1[i].WorkedTill)+"','"+encodeURI(data1[i].WorkedTillMnth)+"','"+encodeURI(data1[i].ProjectDetail)+"')></i></a></span><span style='margin-left:10px;'><a href='#Projecttab' onclick=Delete("+data1[i].ProjectId+",'Project') ><i class='fa fa-times' style='padding-left:20px;'></i></a></span></p><p class='profilesubheading'>" + data1[i].ClientName + "</p><p class='profilesubheading'>" + data1[i].WorkingFrommnth + " " + data1[i].StartedWorkingFrom + "  To  Present</p><p class='profilesubheading'style='word-break: break-word;'> " + data1[i].ProjectDetail + "</p>";
      
        }
        else{
        appenddata += "<p class='profiledetail'style='font-weight: bold;padding-top: 15px;'>" + data1[i].ProjectTitle + "<span style='margin-left:10px;'><a href='#Projecttab'><i class='fa fa-pencil' onclick=EditProjectDetail('"+data1[i].ProjectId+"','"+encodeURI(data1[i].ProjectTitle)+"','"+encodeURI(data1[i].EmploymentOREducation)+"','"+encodeURI(data1[i].ClientName)+"','"+encodeURI(data1[i].ProjectStatus)+"','"+encodeURI(data1[i].StartedWorkingFrom)+"','"+encodeURI(data1[i].WorkingFrommnth)+"','"+encodeURI(data1[i].WorkedTill)+"','"+encodeURI(data1[i].WorkedTillMnth)+"','"+encodeURI(data1[i].ProjectDetail)+"')></i></a></span><span style='margin-left:10px;'><a href='#Projecttab' onclick=Delete("+data1[i].ProjectId+",'Project') ><i class='fa fa-times' style='padding-left:20px;'></i></a></span></p><p class='profilesubheading'>" + data1[i].ClientName + "</p><p class='profilesubheading'>" + data1[i].WorkingFrommnth + " " + data1[i].StartedWorkingFrom + "  To " + data1[i].WorkedTillMnth+" "+ data1[i].WorkedTill+"</p><p class='profilesubheading'style='word-break: break-word;'> " + data1[i].ProjectDetail + "</p>";
                       }
                    }
                    proupdatedon = '<span lang="en">Updated On</span> ' + data[0][0].UpdateOn;
   jQuery("#"+control).html(appenddata);
   
   
 //  ProfileStatus('parsedataProfileStatus','progressbar');
}
}
$("#ProjectUpdateON").html(proupdatedon);
}



function EditProjectDetail(ProjectId,ProjectTitle,EmploymentOREducation,ClientName,ProjectStatus,StartedWorkingFrom,WorkingFrommnth,WorkedTill,ShortMonthName,ProjectDetail) {
modal('Project');
  
$("#ProjectTitle").prop("disabled", true);
$("#Clientname").prop("disabled", true);
localStorage.setItem("ProjectId",ProjectId);
jQuery("#ProjectTitle").val(decodeURI(ProjectTitle)),
jQuery("#EmployementOREdu").val(decodeURI(EmploymentOREducation)),
jQuery("#Clientname").val(decodeURI(ClientName)),
jQuery("#prjtStatus").val(decodeURI(ProjectStatus)),
jQuery("#workingfromyear").val(decodeURI(StartedWorkingFrom)),
jQuery("#workingfrommonth").val(decodeURI(WorkingFrommnth)),
jQuery("#workedtillyear").val(decodeURI(WorkedTill)),
jQuery("#workedtillmonth").val(decodeURI(ShortMonthName)),
jQuery("#comments").val(decodeURI(ProjectDetail))
if ($("#prjtStatus").val() == "1") {
    //$("#workingfromyear").show();
    //$("#workingfrommonth").show();
    $("#tillpresent").show();
    $("#tillyear").hide();
    $("#tillmonth").hide();
}
else {
    $("#tillpresent").hide();
   // $("#workingfromyear").show();
   // $("#workingfrommonth").show();
    $("#tillyear").show();
    $("#tillmonth").show();
}
getvalidated('Clientname','text','Client Name');
getvalidated('prjtStatus','select','Project Status');
getvalidated('workingfromyear','select','Work From in Year');
getvalidated('comments','text','Comment');
getvalidated('workedtillmonth','select','WorkTill in Month');
getvalidated('workedtillyear','select','WorkTill in Year');
getvalidated('workingfrommonth','select','Work From in Month');
getvalidated('ProjectTitle','text','Project Title');
}


function FillProjectCourse(funct,control) {
    var path =  serverpath + "secured/qualification/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillProjectCourse(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillProjectCourse('parsedatasecuredFillProjectCourse','EmployementOREdu');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
        else{
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Employement / Education"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Qualif_id).html(data1[i].Qualif_name));
             }
        }
    }


    function FillProjectMonth(funct,control) {
        var path =  serverpath + "secured/month/0"
        securedajaxget(path,funct,'comment',control);
    }
    function parsedatasecuredFillProjectMonth(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
        
            FillProjectMonth('parsedatasecuredFillProjectMonth','workingfrommonth');
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
           else if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        
            else{
                var data1 = data[0];
                jQuery("#workingfrommonth").append(jQuery("<option ></option>").val("0").html("Select Worked From Month"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#workingfrommonth").append(jQuery("<option></option>").val(data1[i].ShortMonthName).html(data1[i].ShortMonthName));
                 }
                 jQuery("#workedtillmonth").append(jQuery("<option ></option>").val("0").html("Select Worked Till Month"));
                 for (var i = 0; i < data1.length; i++) {
                     jQuery("#workedtillmonth").append(jQuery("<option></option>").val(data1[i].ShortMonthName).html(data1[i].ShortMonthName));
                  }
            }
        }
    
        function FillProjectYear(funct,control) {
            var path =  serverpath + "secured/year/0/20"
            securedajaxget(path,funct,'comment',control);
        }
        function parsedatasecuredFillProjectYear(data,control){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
            
                FillProjectYear('parsedatasecuredFillProjectYear','workingfromyear');
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
              else  if(data.errno) {
                    toastr.warning("Something went wrong Please try again later", "", "info")
                    return false;
                }
            
                else{
                   // ProfileStatus('parsedataProfileStatus','progressbar');
                    var data1 = data[0];
                    jQuery("#workingfromyear").append(jQuery("<option ></option>").val("0").html("Select Worked From Year"));
                    for (var i = 0; i < data1.length; i++) {
                        jQuery("#workingfromyear").append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
                     }
                    
                }
            }
            function FillProjectTillYear(funct,control) {
                var path =  serverpath + "secured/year/0/"+control+""
                securedajaxget(path,funct,'comment',control);
            }
            function parsedatasecuredFillProjectTillYear(data,control){  
                data = JSON.parse(data)
                if (data.message == "New token generated"){
                    sessionStorage.setItem("token", data.data.token);
                
                    FillProjectTillYear('parsedatasecuredFillProjectTillYear',control);
                }
                else if (data.status == 401){
                    toastr.warning("Unauthorized", "", "info")
                    return true;
                }
                   else if(data.errno) {
                        toastr.warning("Something went wrong Please try again later", "", "info")
                        return false;
                    }
                
                    else{
                      var data1 = data[0];
                      jQuery("#workedtillyear").empty()
                         jQuery("#workedtillyear").append(jQuery("<option ></option>").val("0").html("Select Worked Till Year"));
                         for (var i = 0; i < data1.length; i++) {
                             jQuery("#workedtillyear").append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
                          }
                    }
                }
            function CheckProjectWorkingYear(){
                var currentyear=new Date().getFullYear()
                var startedyear=$("#workingfromyear").val()
                var difference = currentyear - startedyear;
                FillProjectTillYear('parsedatasecuredFillProjectTillYear',difference+1);
            }



            function projectAPI(){
                FillProjectDetail('parsedatasecuredFillProjectDetail','Projectdetail');
                FillProjectCourse('parsedatasecuredFillProjectCourse','EmployementOREdu');
                FillProjectMonth('parsedatasecuredFillProjectMonth','workingfrommonth');
                FillProjectYear('parsedatasecuredFillProjectYear','workingfromyear');
                FillProjectTillYear('parsedatasecuredFillProjectTillYear', '20');

            }