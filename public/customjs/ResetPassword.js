function UpdChangePassword() {

    var MasterData = {
        
        "p_Password":sessionStorage.getItem("Password"),
        "p_CandidateId": sessionStorage.getItem("CandidateId"),
        "p_NewPassword": md5(jQuery("#newpassword").val()),
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "changepassword";
    ajaxpost(path, 'parsrdatachangepassword', 'comment', MasterData, 'control')
}
function parsrdatachangepassword(data) {
    data = JSON.parse(data)
     if (data[0][0].ReturnValue == "1") {
        $('#modalresetpassword').modal('toggle');
     resetmode();
     resetModeforget();
       toastr.success("Password Changed Successfully", "", "success")
        return true;
    }
    if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }
    
   
}
function resetmode() {

    createcaptchareset() ;
   
    jQuery("#newpassword").val(""),
    jQuery("#Repassword").val(""),
    jQuery("#cpatchaTextBoxreset").val("")

}
function CheckChangePassword(){
    if(isValidate){
    if (jQuery('#newpassword').val() == "") {
        getvalidated('newpassword','text','New Password');
        return false;
    }
    
        var password = $("#newpassword").val();
        if (password != '') {
            var regularExpression =/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$/;
          
            if (!regularExpression.test(password)) {
                $("#passwordvalidatereset").show();
                return false;
            }
            else {
                $("#passwordvalidatereset").hide();
                if (jQuery('#Repassword').val() == "") {
                    getvalidated('Repassword','text','Confirm Password');
                    return false;
                }
                if (jQuery('#Repassword').val() !=jQuery('#newpassword').val()) {
                    $('#validRepassword').html("Password Not Match") ;
                    jQuery('#Repassword').css('border-color', 'red');
                    return false;
                }
                else{
                    $('#validRepassword').html("") ;
                    jQuery('#Repassword').css('border-color', '');
                  //  UpdChangePassword()

                  if(isCaptchaValidated){ 
                    if (jQuery('#cpatchaTextBoxreset').val() == "") {
                        getvalidated('cpatchaTextBoxreset','text','Captcha');
                        return false;
                    }
                    else{
                        validatepassword();
                    }
                
                }
                else{
                    validatepassword();
                }
                }
            }
        }
      
            }

            else{
                UpdChangePassword();
            }
    
  
    

}
var codereset;
                function createcaptchareset() {
                  //clear the contents of captcha div first 
                  document.getElementById('captchareset').innerHTML = "";
                  var charsArray =
                  "0123456789";
                  var lengthOtp = 6;
                  var captcha = [];
                  for (var i = 0; i < lengthOtp; i++) {
                    //below code will not allow Repetition of Characters
                    var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
                    if (captcha.indexOf(charsArray[index]) == -1)
                      captcha.push(charsArray[index]);
                    else i--;
                  }
                  var canv = document.createElement("canvas");
                  canv.id = "captchareset";
                  canv.width = 100;
                  canv.height = 50;
                  var ctx = canv.getContext("2d");
                  ctx.font = "25px Georgia";
                  ctx.strokeText(captcha.join(""), 0, 30);
                  //storing captcha so that can validate you can save it somewhere else according to your specific requirements
                  codereset = captcha.join("");
                  document.getElementById("captchareset").appendChild(canv); // adds the canvas to the body element
                }
                function validateCaptchareset() {
                  event.preventDefault();
                  
                  if (document.getElementById("cpatchaTextBoxreset").value == codereset) {
                    UpdChangePassword();
                 
                  }else{
                    $("#cpatchaTextBoxreset").val('');
                    $("#cpatchaTextBoxreset").css('border-color', 'red');
                    $("#validcpatchaTextBoxreset").html("Please enter Valid Captcha");
                    createcaptchareset();
                    
                  
                    
                  }
                }
                $("#newpassword").focusout(function () {
                    getvalidated('newpassword','text','New Password');
                    
                });   
                $("#newpassword").keyup(function () {
                    var password = $("#newpassword").val();
                    if (password != '') {
                        var regularExpression =/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$/;
                      
                        if (!regularExpression.test(password)) {
                            $("#passwordvalidatereset").show();
                            return false;
                        }
                        else {
                            $("#passwordvalidatereset").hide();

                        }
                    }
                });     
                function validatepassword(){
                    var password = $("#newpassword").val();
                    if (password != '') {
                        var regularExpression =/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$/;
                      
                        if (!regularExpression.test(password)) {
                            $("#passwordvalidatereset").show();
                            return false;
                        }
                        else {
                            $("#passwordvalidatereset").hide();
                            if(isCaptchaValidated){
                                validateCaptchareset() ;
                            }
                            else{
                                UpdChangePassword();
                            }
                           
                        }
                    }
                }  


                $(".toggle-password").click(function () {
                    $(this).toggleClass("fa-eye fa-eye-slash");
                    var input = $($(this).attr("toggle"));
                    if (input.attr("type") == "password") {
                        input.attr("type", "text");
                    } else {
                        input.attr("type", "password");
                    }
                    });