
function FillCategory() {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "jobprefrences/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {
            var data1 = data[0];
            jQuery("#ddlCategory").empty();
            jQuery("#ddlCategory").append(jQuery("<option ></option>").val("0").html("Select Categories"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#ddlCategory").append(jQuery("<option></option>").val(data1[i].FunctionalArea_id).html(data1[i].FunctionalArea));
            }
            
        },
        error: function (xhr) {
            //swal(xhr.d, "", "error")
        }
    });
    
}
function FillCity() {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "district/0/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {
            var data1 = data[0];
            jQuery("#ddlcity").empty();
            jQuery("#ddlcity").append(jQuery("<option ></option>").val("0").html("Select District"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#ddlcity").append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
            }
            
           
        },
        error: function (xhr) {
            //swal(xhr.d, "", "error")
        }
    });
}


function fetchJobfair(){
    var MasterData ={
        
        "p_JobfairParticipantId":'0',
        "p_Jobfair_Id":sessionStorage.Id,
        "p_CandidateId":sessionStorage.CandidateId,
        "p_Selection_Status":'0',
        "p_EntryBy":sessionStorage.CandidateId,
        "p_IpAddress":sessionStorage.ipAddress
        
        };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "ParticipantJobFair";
    securedajaxpost(path, 'parsedataparticipant', 'comment', MasterData, 'control')
    
    }
    function parsedataparticipant(data) {
        data = JSON.parse(data)
      //  console.log(data);
       // $('#m_modal_6').modal('hide')
       if(data[0][0].ReturnValue=='0'){
        toastr.warning("Allready Applied", "", "info")
        return false;
       }
       $("#Close").click();
    }

function Filljobfairs() {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "latestJobfair",
        cache: false,
        dataType: "json",
        success: function (data) {
            var appenddata="";
            var appenddata1 ="";
            var Modal ="";
            for (var i = 0; i < data[0].length; i++) {

           if (data[0][i].FromDate != "") {
            var fromdate = data[0][i].ToDate;
            var d1 = new Date();
            var d2 = new Date(fromdate);
            if (d1 <= d2) {
                appenddata += "<ul class='categories'><li><h5>"+data[0][i].Head+"</h5><div style='font-size:inherit'>"+data[0][i].FromDate+" To "+data[0][i].ToDate+"</div><br><div>"+data[0][i].Venue+"</div><br>"+data[0][i].Details+"<a href='/Login' onclick=sessionStorage.Type='Jobfair' style='font-size:small;color:blue;padding-left:10px'>   Apply Here</a></li></ul>"
         }
        }   
       
         if (data[0][i].FromDate != "") {
       
            var fromdate = data[0][i].ToDate;
            var d1 = new Date();
            var d2 = new Date(fromdate);
            if (d2 > d1) {
             var detail="Closed";
            }
            else{
            //  var detail="<button type='button' onclick=InsUpdInterestedcandidatejf();$('#closebtn').click();sessionStorage.jfId="+data[0][i].JobFairId+"  class='btn btn-brand btn-sm m-btn m-btn--pill m-btn--wide'>YES </button>";
              var detail="<a data-toggle='modal' data-target='#m_modal_5' href='#'><button type='button' class='btn btn-brand btn-sm m-btn m-btn--pill m-btn--wide' onclick=modalDetail('"+encodeURI(data[0][i].Jobfair_Title)+"','"+encodeURI(data[0][i].Zone)+"','"+encodeURI(data[0][i].District)+"','"+encodeURI(data[0][i].Institute_Name)+"','"+encodeURI(data[0][i].Gender)+"','"+encodeURI(data[0][i].Jobfair_Details)+"','"+encodeURI(data[0][i].Venue)+"','"+encodeURI(data[0][i].FromDate)+"','"+encodeURI(data[0][i].ToDate)+"','"+encodeURI(data[0][i].Trade)+"','"+encodeURI(data[0][i].percentage_req)+"','"+encodeURI(data[0][i].passingoutyearfrom)+"','"+encodeURI(data[0][i].passingoutyearto)+"');sessionStorage.jfId="+data[0][i].JobFairId+">Detail </button></a>";
        }
          }
         appenddata1 += " <tr><td>" + [i+1] + "</td><td>" + data[0][i].Head + "</td><td style='word-break: break-word;'>"+data[0][i].Venue+"</td><td>"+data[0][i].FromDate+"</td><td>"+data[0][i].ToDate+"</td> <td >"+detail+"</td></tr>"   
                }  
                if (appenddata == ""){
                    $("#jobfairs").html("No Active Job Fair !!!!!")
                }
                else{
                    $("#jobfairs").html(appenddata)
                }
        
        $("#jobfairlisttable").html(appenddata1)
        },
        error: function (xhr) {
        }
        
    });
    
}

function modalDetail(Jobfair_Title,Zone,District,Institute_Name,Gender,Jobfair_Details,Venue,FromDate,ToDate,Trade,percentage_req,passingoutyearfrom,passingoutyearto){
    if(Gender==1){
        var gender="Male"
    }
   else if(Gender==2){
        var gender="Female"
    }
    else if (Gender==3){
       var gender="Both"
       }
    Modalcontent = "<div class='job-header'><div class='jobdetail' style='padding: 40px;'><ul class='jbdetail'><h6>"+decodeURI(Jobfair_Title)+" Details</h6>"+
    "<li class='row'><div class='col-md-4 col-xs-6' >JobFair Title</div><div   class='col-md-6 col-xs-6'  ><span style=' color: darkgrey;  text-align: left; width: 145%;'>"+decodeURI(Jobfair_Title)+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' >Zone</div><div   class='col-md-6 col-xs-6'  ><span style=' color: darkgrey;  text-align: left; width: 145%;'>"+decodeURI(Zone)+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' >District</div><div   class='col-md-6 col-xs-6'  ><span style=' color: darkgrey;  text-align: left; width: 145%;'>"+decodeURI(District)+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' >ITI Name</div><div   class='col-md-6 col-xs-6'  ><span style=' color: darkgrey;  text-align: left; width: 145%;'>"+decodeURI(Institute_Name)+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' >Gender</div><div   class='col-md-6 col-xs-6'  ><span style=' color: darkgrey;  text-align: left; width: 145%;'>"+decodeURI(gender)+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' >Job Detail</div><div   class='col-md-6 col-xs-6'  ><span style=' color: darkgrey;  text-align: left; width: 145%;'>"+decodeURI(Jobfair_Details)+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' >Venue</div><div   class='col-md-6 col-xs-6'  ><span style=' color: darkgrey;  text-align: left; width: 145%;'>"+decodeURI(Venue)+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' >From Date</div><div   class='col-md-6 col-xs-6'  ><span style=' color: darkgrey;  text-align: left; width: 145%;'>"+decodeURI(FromDate)+" - "+decodeURI(ToDate)+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' >Trade</div><div   class='col-md-6 col-xs-6'  ><span style=' color: darkgrey;  text-align: left; width: 145%;'>"+decodeURI(Trade)+"</span></div></li>"+  
    "<li class='row'><div class='col-md-4 col-xs-6' >Percentage Required</div><div   class='col-md-6 col-xs-6'  ><span style=' color: darkgrey;  text-align: left; width: 145%;'>"+decodeURI(percentage_req)+" %</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' >Passing Out Year</div><div   class='col-md-6 col-xs-6'  ><span style=' color: darkgrey;  text-align: left; width: 145%;'>"+decodeURI(passingoutyearfrom)+" - "+decodeURI(passingoutyearto)+"</span></div></li>"+

    "</ul></div></div>"
    $("#modalbody2").html(Modalcontent);
}

function CCDetails(JobFairId,Head,Details,Ex_name,Venue,FromDate,ToDate){
    $("#title").html(decodeURI(Head));
    $("#details").html(decodeURI(Details));
    $("#venue").html(decodeURI(Venue));
   $("#exchangename").html(decodeURI(Ex_name));
    $("#date").html(decodeURI(FromDate)+' To '+decodeURI(ToDate))
}

 function FillSkills() {
        var path = serverpath + "skill/0/0/0/0"
        securedajaxget(path,'parsedataskill','comment','control');
    }
    function parsedataskill(data,control){  
        data = JSON.parse(data)
        
        var data1 = data[0];
        
            $('.skillsearch').magicsearch({
                dataSource: data1,
                fields: ['SkillName'],
                id: 'SkillId',
                format: '%SkillName%',
                multiple: true,
                multiField: 'SkillName',
                multiStyle: {
                    space: 5,
                    width: 80
                }
            });

        }
   
function checksearchJob(){
    if ($("#Sector").val() == "" && $("#Qualification").val() == ""&& $("#Location").val() == ""){
        return false;
    }
    else{
        sessionStorage.setItem("Sector",$("#Sector").val());
        sessionStorage.setItem("Qualification",$("#Qualification").val());
        sessionStorage.setItem("Location",$("#Location").val());  
        sessionStorage.setItem("Designation",'');
        sessionStorage.type='searchJob' ; 
        window.location = '/joblisting' 
        }
    }
    function SetJobByDesignation(Designation){
        sessionStorage.setItem("Sector",'');
            sessionStorage.setItem("Designation",decodeURI(Designation));
            sessionStorage.setItem("Qualification",'');  
            sessionStorage.setItem("Location",'');  
            sessionStorage.type='jobByDesignation';  
            window.location = '/joblisting' 
            
        }

        

        function SetJobByQualification(Qualification){
       
            sessionStorage.setItem("Sector",'');
            sessionStorage.setItem("Qualification",decodeURI(Qualification));
            sessionStorage.setItem("Location",'');  
            sessionStorage.setItem("Designation",'');  
            window.location = '/joblisting' 
            
        }


        function SetJobByLocation(Location){
       
            sessionStorage.setItem("Sector",'');
            sessionStorage.setItem("Qualification",'');
            sessionStorage.setItem("Location",decodeURI(Location));  
            sessionStorage.setItem("Designation",'');  
            window.location = '/joblisting' 
            
        }
       
 
    function Fetchweblinkurl() {
        var path = serverpath + "wlurl/0/0/0"
        ajaxget(path, 'parsedataFetchweblinkurl', 'comment', "control");
    }
    function parsedataFetchweblinkurl(data) {
        data = JSON.parse(data)
        var data1 = data[0];
        var appenddata = "";
        var active = "";
        for (var i = 0; i < data1.length; i++) {
            if (data1[i].Active_YN == '1') {
                
                appenddata += "<li class='col-md-5 col-sm-6'  style='margin-left: 43px;'   ><a  target='blank' href='"+data1[i].WL_URL+"'><span lang='en'>" + data1[i].WL_Description + "</span></a></li>";
       
            }
           
            
        } jQuery("#weblinks").html(appenddata);
    
    
    }    
    

    function FetchFeaturedJob() {
        var path = serverpath + "FeaturedJob/0/0"
        ajaxget(path, 'parsedataFetchFeaturedJob', 'comment', "control");
    }
    function parsedataFetchFeaturedJob(data) {
        data = JSON.parse(data)
        var data1 = data[0];
        var appenddata = "";
        
        for (var i = 0; i < 4; i++) {
            if(data1[i].EmployerLogo !=null){
                logosrc=EmployerLogoPath+'EmployerLogo/'+encodeURI(data1[i].EmployerLogo)
                }
                else{
                logosrc='images/jobs/jobimg.jpg';
                }
                //checkIfFileLoaded(logosrc);
                //if (globalfilestatus == false){
                //    logosrc='images/jobs/jobimg.jpg';
                //}
              appenddata += "<li class='col-md-6'><div class='jobint'><div class='row'><div class='col-md-2 col-sm-2'><img style='max-width: 100% !important;' src='"+ logosrc +"' alt='" + data1[i].CompName +"' style='max-width:83%'/></div><div class='col-md-6 col-sm-7'><h4><a onclick=SetJobByDesignation('"+ encodeURI(data1[i].Designation) +"')><div style=' overflow: hidden;text-overflow: ellipsis;white-space: nowrap;' >" + data1[i].Designation +"</div></a></h4><div class='company' style=' overflow: hidden;text-overflow: ellipsis;white-space: nowrap;'><a href=' http://"+data1[i].url+"' target='blank'> " + data1[i].CompName + " </a></div><div class='jobloc'><label class='fulltime'>" + data1[i].VacancyType + "</label> - <span>" + data1[i].Placeofwork + "</span></div></div><div class='col-md-3 col-sm-3'><a href='/Login' onclick=setFeaturedJobFilter('"+encodeURI(data1[i].Designation)+"','"+encodeURI(data1[i].Placeofwork)+"') class='applybtn'>Apply Now</a></div><div class='col-md-4 col-sm-3 PostedOn'> <span> Posted " + data1[i].Posted + "  Ago</span></div></div></div></li>";    
        } 
        jQuery("#FeaturedJob").html(appenddata);
    
    
    }  

        function CareerInformationJob(){
                sessionStorage.setItem("Designation",'')
                sessionStorage.setItem("Location",'');    
                    window.location = '/joblisting' 
                    
                }



                function FetchTopEmployers() {
                     var path = serverpath + "TopEmployers"
                     ajaxget(path, 'parsedataFetchTopEmployers', 'comment', "control");
                     }
                function parsedataFetchTopEmployers(data) {
                     data = JSON.parse(data)
                     var data1 = data[0];
                     var appenddata = "";
                     
                     for (var i = 0; i < data1.length; i++) {
                     if(data1[i].EmployerLogo !=null || data1[i].EmployerLogo !=undefined){
                        logosrc=EmployerLogoPath+'EmployerLogo/'+encodeURI(data1[i].EmployerLogo) 
                     }
                     else{
                        logosrc ='images/jobs/jobimg.jpg';
                     }
                     appenddata += " <li data-toggle='tooltip' data-placement='top' title='" + data1[i].CompName + "' data-original-title='" + data1[i].CompName + "'><a href='http://" +data1[i].URL+"' target='blank'><img src='" + logosrc + "' alt='" + data1[i].CompName + "' style=' width: 70px;height: 50px;' /></a>";
                     
                     } 
                     jQuery("#EmployersList").html(appenddata);
                     
                     
                    }

                    function FillNote() {
                        var path = serverpath + "indexnote"
                        ajaxget(path, "parsedataFillNote", 'comment', "control");
                    }
                    function parsedataFillNote(data) {
                        data = JSON.parse(data)
                        var data1 = data[0];
                       
                        
                         jQuery("#noteindex").html(data[0][0].Description+" &nbsp&nbsp  "+data[0][0].Description_H);
                    
                        
                    }
                    function InsUpdInterestedcandidatejf() {//Sonali
                        
                        var MasterData = {
                            "p_js_Id": sessionStorage.getItem("CandidateId"),
                            "p_jf_Id": sessionStorage.getItem("jfId"),
                            
                            
                        }
                        MasterData = JSON.stringify(MasterData)
                        var path = serverpath + "secured/Interestedcandidatejf";
                        securedajaxpost(path, 'parsrdataInterestedcandidatejf', 'comment', MasterData, 'control')
                    }
                    function parsrdataInterestedcandidatejf(data) {
                        data = JSON.parse(data)
                        if (data.message == "New token generated") {
                            sessionStorage.setItem("token", data.data.token);
                            InsUpdInterestedcandidatejf();
                        }
                      
                        else if (data[0][0].ReturnValue == "1") {
                            
                           
                            // toastr.warning("Already Submitted", "", "info")
                            // $('#myModalcareer').modal('toggle');
                            window.location='/HallTicket'
                            sessionStorage.setItem("TypeHome","WarningJF")
                            // location.reload();
                            return true;
                        }
                        else if (data[0][0].ReturnValue == "2") {
                            toastr.success("Submit Successfully", "", "success")
                            window.location='/HallTicket'
                            // $('#modalChangepassword').modal('toggle');
                            // sessionStorage.setItem("TypeHome","SuccessJF")
                            // location.reload();
                            return true;
                        }
                    
                    }
                    function setFeaturedJobFilter(Designation,Placeofwork){
                        sessionStorage.Type='Search';
                        sessionStorage.Designation=decodeURI(Designation);
                        sessionStorage.Location=decodeURI(Placeofwork)
                    }
                    function latestNotification(){
                        var path = serverpath + "fetch_latestNotification"
                        ajaxget(path, "parsedatalatestnotification", 'comment', "control");
                    }
                    function parsedatalatestnotification(data) {
                        data = JSON.parse(data);
                        var appenddata='';
                        for (var i = 0; i < data[0].length; i++) {
                           if(data[0][i].Type=='ITI' || data[0][i].Type=='Jobfair'){
                               var href='/Login';
                               var Text='Job Fair'
                           }
                           else{
                            var href='/CareerCounsellingForm';
                            var Text='Career Counselling'
                           }
                            appenddata += `<a href='${href}'  onclick=sessionStorage.Type='${data[0][i].Type}';sessionStorage.Id=${data[0][i].Id};window.location='${href}'>
                            <p ><span class="blinking">`+Text+`</span> 
                           ${data[0][i].Head}
                              </p>
                              </a><br>
                            `} 
                            jQuery("#latestnotification").append(appenddata);
                    }



                    function latestJobFairs(){
                        var path = serverpath + "fetch_latestNotification"
                        ajaxget(path, "parsedatalatestJobFairs", 'comment', "control");
                    }
                    function parsedatalatestJobFairs(data) {
                        data = JSON.parse(data);
                        var appenddata='';
                        var appenddata1='';
                        for (var i = 0; i < data[0].length; i++) {

                            if (data[0][i].FromDate != "") {
                             var fromdate = data[0][i].ToDate;
                             var d1 = new Date();
                             var d2 = new Date(fromdate);
                             if (d1 <= d2) {
                                 appenddata += "<ul class='categories'><li><h5>"+data[0][i].Head+"</h5><div style='font-size:inherit'>"+data[0][i].FromDate+" To "+data[0][i].ToDate+"</div><br><div>"+data[0][i].Venue+"</div><br>"+data[0][i].Details+"<a href='/Login' onclick=sessionStorage.Type='Jobfair' style='font-size:small;color:blue;padding-left:10px'>   Apply Here</a></li></ul>"
                          }
                         }   
                        
                          if (data[0][i].FromDate != "") {
                        
                             var fromdate = data[0][i].ToDate;
                             var d1 = new Date();
                             var d2 = new Date(fromdate);
                             if (d2 < d1) {
                              var detail="Closed";
                             }
                             else {
                             //  var detail="<button type='button' onclick=InsUpdInterestedcandidatejf();$('#closebtn').click();sessionStorage.jfId="+data[0][i].JobFairId+"  class='btn btn-brand btn-sm m-btn m-btn--pill m-btn--wide'>YES </button>";
                               var detail="<a data-toggle='modal' data-target='#m_modal_6' href='#'><button type='button' class='btn btn-brand btn-sm m-btn m-btn--pill m-btn--wide' onclick=modalJobFairDetail('"+encodeURI(data[0][i].Head)+"','"+encodeURI(data[0][i].Details)+"','"+encodeURI(data[0][i].Venue)+"','"+encodeURI(data[0][i].FromDate)+"','"+encodeURI(data[0][i].ToDate)+"','"+encodeURI(data[0][i].Links)+"');sessionStorage.Jobfair_Id="+data[0][i].Id+">Detail </button></a>";
                    
                         }
                           }
                         
                          
                          appenddata1 += " <tr><td>" + [i+1] + "</td><td>" + data[0][i].Head + "</td><td style='word-break: break-word;'>"+data[0][i].Venue+"</td><td>"+data[0][i].FromDate+"</td><td>"+data[0][i].ToDate+"</td> <td >"+detail+"</td></tr>"   
                       
                                 }  
                                 if (appenddata == ""){
                                     $("#jobfairs").html("No Active Job Fair !!!!!")
                                 }
                                 else{
                                     $("#jobfairs").html(appenddata)
                                 }
                                 $("#jobfairlisttable").html(appenddata1)
                                }


                                function modalJobFairDetail(Head,Details,Venue,FromDate,ToDate,Links){
                 
                                    Modalcontent = "<div class='job-header'><div class='jobdetail' style='padding: 40px;'><ul class='jbdetail'><h6>"+decodeURI(Head)+" Details</h6>"+
                                    "<li class='row'><div class='col-md-4 col-xs-6' >JobFair Title</div><div   class='col-md-6 col-xs-6'  ><span style=' color: darkgrey;  text-align: left; width: 145%;'>"+decodeURI(Head)+"</span></div></li>"+
                                    "<li class='row'><div class='col-md-4 col-xs-6' >Job Detail</div><div   class='col-md-6 col-xs-6'  ><span style=' color: darkgrey;  text-align: left; width: 145%;'>"+decodeURI(Details)+"</span></div></li>"+
                                    "<li class='row'><div class='col-md-4 col-xs-6' >Venue</div><div   class='col-md-6 col-xs-6'  ><span style=' color: darkgrey;  text-align: left; width: 145%;'>"+decodeURI(Venue)+"</span></div></li>"+
                                    "<li class='row'><div class='col-md-4 col-xs-6' >From Date</div><div   class='col-md-6 col-xs-6'  ><span style=' color: darkgrey;  text-align: left; width: 145%;'>"+decodeURI(FromDate)+" - "+decodeURI(ToDate)+"</span></div></li>"+
                                    "<li class='row'><div class='col-md-4 col-xs-6' > Links </div><div   class='col-md-6 col-xs-6'  ><span style=' color: darkgrey;  text-align: left; width: 145%;'>"+decodeURI(Links)+"</span></div></li>"+ 
                                    "</ul></div></div>"
                                    $("#modalbody6").html(Modalcontent);

                                }