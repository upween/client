function CheckValidation() {
if(isValidate){

    if (jQuery('#Usernamefeedback').val() == '') {
        getvalidated('Usernamefeedback', 'text', 'User name');
        return false;
    }


 if (jQuery('#Comment').val() == '') {
      getvalidated('Comment', 'text', 'Comments');
        return false;
    }




    if (jQuery('#ContactNo').val() == '') {

        getvalidated('ContactNo', 'number', 'Mobile Number');
        return false;
    }
    if (jQuery('#EmailId').val() == '') {

        getvalidated('EmailId', 'email', 'EmailId');
        return false;
    }
    if (jQuery('#Location').val() == '') {

        getvalidated('Location', 'text', 'Location');
        return false;
    }

   

    if (sessionStorage.getItem("CityId") == '0') {
        $("#Location").css('border-color', 'red');
        $("#validLocation").html("Please enter proper Location ");
        return false;}

        if(isCaptchaValidated){  
    if (jQuery('#cpatchaTextBoxfeedback').val() == '') {

        getvalidated('cpatchaTextBoxfeedback', 'text', 'Captcha');
        return false;
    }
 else {
        var radios = document.getElementsByName("checkbox");
        var formValid = false;

        var i = 0;
        while (!formValid && i < radios.length) {
            if (radios[i].checked)
                formValid = true;
            i++;
        }

        if (!formValid) {

            $(window).scrollTop(0);
            toastr.warning("Please Select FeedBack Area", "", "info")
            return false;



        }
        else {
            InsUpdFeedBack();
        }
    }
    }
}
else{
    InsUpdFeedBack();
}
}


function InsUpdFeedBack() {
    var Mail;
    Mail = jQuery('#EmailId').val();

    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if (reg.test(Mail) == false) {
        $(window).scrollTop(0);
        toastr.warning("Please Enter Correct E-MailId", "", "info");
        jQuery('#EmailId').css('border-color', 'red');
        return true;
    }
    else {
        jQuery('#EmailId').css('border-color', '');

    }



    var feedbackArea = [];
    $.each($("input[name='checkbox']:checked"), function () {
        feedbackArea.push($(this).val());
        //feedbackArea += this.value + ","
    });


    //var FileName = jQuery("#fileFileName").val().replace(/C:\\fakepath\\/i, '');
    var MasterData = {
        "p_FeedBackId": '0',
        "p_UserName": jQuery("#Usernamefeedback").val(),
        "p_ContactNumber": jQuery("#ContactNo").val(),
        "p_EmailId": jQuery("#EmailId").val(),
        "p_Location": jQuery("#Location").val(),
        "p_Comments": jQuery("#Comment").val(),
        "p_FeedBackArea": feedbackArea.join(", "),
        "p_UserId":"0",
        "p_IpAddress":sessionStorage.getItem("IpAddress")
        //"p_Documents": FileName,

    };

    jQuery.ajax({
        url: serverpath + 'feedback',
        type: "POST",
        data: JSON.stringify(MasterData),
        contentType: "application/json; charset=utf-8",
        success: function (data, status, jqXHR) {
            //fileUploader();
            // resetMode();
            if (data[0][0].ReturnValue == "1") {
                toastr.success("Insert Successful", "", "success")
                sessionStorage.setItem("FeedbackId", data[0][0].p_FeedBackId);
                sessionStorage.ModalType='Feedback'
                Cookies.set('FeedbackId',data[0][0].p_FeedBackId, { expires: 1, path: '/' });
                $("#postattach").click();
            }

        },
        error: function (result) {
            
            // toastr.success("Insert Successful", "", "success")
        }
    });
}

// function CheckFeedBackUser() {
//     jQuery.ajax({
//         type: "GET",
//         contentType: "application/json; charset=utf-8",
//         url: serverpath + 'feedbackUser/'+$('#Name').val()+'',
//         //data: "{}",
//         cache: false,
//         dataType: "json",
//         success: function (data) {
//             if (data[0][0].ReturnValue == "2") {
//                 $("#validName").html("Please Enter Valid User Name");
//                 return false;
//             }
//             else if (data[0][0].ReturnValue == "1") {
//                 return true;
//             }
//         }
//     });
// }

function resetMode() {
   // createCaptchafeedback();
    jQuery("#Usernamefeedback").val("");
    jQuery("#ContactNo").val("");
    jQuery("#EmailId").val("");
    jQuery("#Location").val("");
    jQuery("#Comment").val("");
    jQuery("#cpatchaTextBoxfeedback").val("");
    jQuery("#Documents").val("");
    $("input[name='checkbox']").prop("checked", false);

}


function FillAutoCompleteCity() {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + 'city/0/0/0/0',
        //data: "{}",
        cache: false,
        dataType: "json",
        success: function (data) {
            if (data.length > 0) {
                sessionStorage.setItem('CityName', JSON.stringify(data[0]));
            }
        }
    });
}

jQuery("#Location").keyup(function () {
    sessionStorage.setItem('CityId', '0');
});

jQuery("#Location").typeahead({
    source: function (query, process) {
        var data = sessionStorage.getItem('CityName');
        cityname = [];
        map = {};
        var Location = "";
        jQuery.each(jQuery.parseJSON(data), function (i, Location) {
            map[Location.CityName] = Location;
            cityname.push(Location.CityName);
        });
        process(cityname);
    },
    minLength: 3,
    updater: function (item) {
        sessionStorage.setItem('CityId', map[item].CityId);
        return item;
    }
});

function UpdFeedbackDoc(){

                
    var MasterData = {

        "p_FeedBackId": sessionStorage.getItem("FeedbackId"),
        "p_UserName": '',
        "p_ContactNumber": '',
        "p_EmailId":'',
        "p_Location": '',
        "p_Comments":'',
        "p_Documents":$('#Feedback').text(),
        "p_FeedBackArea": '',
        
    };
MasterData = JSON.stringify(MasterData)
var path = serverpath + "feedback";
securedajaxpost(path, 'parsrdataUpdFeedbackDoc', 'comment', MasterData, 'control')
}


function parsrdataUpdFeedbackDoc(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            UpdFeedbackDoc();
    }
    else{
     if(data[0][0].ReturnValue=='2'){
         resetMode();
        toastr.success("Submit Successfully", "", "success")
     }
    }
 
}
if( sessionStorage.ModalType=='Feedback'){
    if($("#FeedbackMsg").html()==" File Uploaded!"){
        UpdFeedbackDoc()
   
}

else if($("#FeedbackMsg").html()==" Error: PDF and Image File Only!"){
    toastr.warning("Error: PDF and Image File Only!", "", "success")

}
else if($("#FeedbackMsg").html()==" File Size Limit Exceeded"){
    toastr.warning("File Size Limit Exceeded", "", "success")

}
else if($("#FeedbackMsg").html()==" Error: No File Selected!"){
    
    resetMode();
    toastr.success("Submit Successfully", "", "success")
}
}

// function checkalphabets(){
//     $('#Usernamefeedback').keypress(function (e) {
//       var k = e.which;
//       var ok = k >= 65 && k <= 90 || // A-Z
//          k >= 97 && k <= 122  // a-z
        
  
//       if (!ok){
//         e.preventDefault();
//       }
//   });
  