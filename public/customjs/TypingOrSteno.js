function CheckTyping()    //Sonali 
 {
if(isValidate){

    if (jQuery('#language').val() == "0" ) {
        getvalidated('language','select','Language');
        return false;
    }
    if (jQuery('#type').val() == "0" ) {
        getvalidated('type','select','Type');
        return false;
    }
    else {
      
    InsUpdTypingOrSteno();
}
}
else{
    InsUpdTypingOrSteno();
}
 }

function InsUpdTypingOrSteno(){
  
    $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);
 
    var MasterData = {
        "p_TypingStenoId": localStorage.getItem("TypingStenoId"),
        "p_CandidateId": sessionStorage.getItem("CandidateId"),
        "p_Language":jQuery("#language").val(),
        "p_Type": jQuery("#type").val(),
        "p_Institute": jQuery("#Institute").val(),
        "p_PassedYear": jQuery("#passYear").val(),
        "p_Speed":jQuery("#Speed").val(),
        "p_IpAddress":sessionStorage.getItem("IpAddress"),
       }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/Typing";
    securedajaxpost(path,'parsrdataInsUpdTypingOrSteno','comment',MasterData,'control')
}

function parsrdataInsUpdTypingOrSteno(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        InsUpdTypingOrSteno();
        
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    if (data[0][0].ReturnValue == "1") {
        //  $('#Close').click();
        ProfileStatus('parsedataProfileStatus','progressbar');
          $('#myModal1').modal('toggle');
          FetchTyping('parsedatasecuredFetchTyping','Typingdetail');
          toastr.success("Insert Successful", "", "success")
          return true;
      }
       else if (data[0][0].ReturnValue == "2") {
        //  $('#Close').click();
        
          $('#myModal1').modal('toggle');
          FetchTyping('parsedatasecuredFetchTyping','Typingdetail');
          resetTyping();
          toastr.success("Update Successful", "", "success")
          return true;
      }
      else if (data[0][0].ReturnValue == "0") {
        resetTyping();
        toastr.warning("Typing Detail already Exists ", "", "info")
        return false;
    }
    
     }

 function resetTyping() {

        $("#language").prop("disabled", false);
        $("#type").prop("disabled", false);
        jQuery("#language").val("0")
        jQuery("#type").val("0")
        jQuery("#Institute").val("0"),
        jQuery("#passYear").val("NA"),
        jQuery("#Speed").val("")
        jQuery('#language').css('border-color', '');
        $('#validlanguage').html("");
        jQuery('#type').css('border-color', '');
        $('#validtype').html("");
         }

     function FetchTyping(funct,control) {
            var path = serverpath + "secured/Typing/'0'/'"+sessionStorage.getItem("CandidateId")+"'/'"+sessionStorage.getItem("IpAddress")+"'"
            securedajaxget(path,funct,'comment',control);
        }

    function parsedatasecuredFetchTyping(data,control){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FetchTyping('parsedatasecuredFetchTyping','Typingdetail');
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
               else if(data.errno) {
                    toastr.warning("Something went wrong Please try again later", "", "info")
                    return false;
                }
            
            else{
                jQuery("#" + control).empty();
                var typohead="";
                if(data[0]=="")
                {
                   localStorage.setItem("TypingStenoId","0")
                }
                else{
                var data1 = data[0];
                localStorage.setItem("TypingStenoId",data1[0].TypingStenoId)
                var appenddata="";
                
                jQuery("#"+control).empty();
                for (var i = 0; i < data1.length; i++) {
                    if(data1[i].Year  ==null || data1[i].Year  =='0'){
                        var year="-";
                    }
                    else{
                        var year= data1[i].Year ;
                    }

                    if(data1[i].TS_Institute_Name  ==null || data1[i].TS_Institute_Name  =='0'){
                        var tS_Institute_Name="-";
                    }
                    else{
                        var tS_Institute_Name= data1[i].TS_Institute_Name ;
                    }
                  
                    if(data1[i].Speed  ==null || data1[i].Speed  =='0'){
                        var speed="-";
                    }
                    else{
                        var speed= data1[i].Speed 
                        // 'WPM';
                    }
                  
                appenddata+="<tr><td>" + data1[i].Language + "</td><td>" + data1[i].Type + "</td><td style='     width: 28%;   word-break: break-word;'>" + tS_Institute_Name + "</td><td>" +year + "</td><td>" + speed + " WPM</td><td><span style='margin-left:10px;'><a href='#Typingadd' onclick=EditTyping('" + data1[i].TypingStenoId + "','" + data1[i].Lang_id + "','" + data1[i].Type_id + "','" + encodeURI(data1[i].Institute) + "','" + encodeURI(data1[i].PassedYear) + "','" + encodeURI(data1[i].Speed) + "') '><i class='fa fa-pencil'></i></a></span></td><td><span style='margin-left:10px;'><a href='#Typingadd' onclick=Delete("+data1[i].TypingStenoId+",'TypingSteno') ><i class='fa fa-times'></i></a></span></td></tr>";

                 typohead=" <span lang='en'>Updated On</span> "+data[0][i].UpdateOn+"";
                 
            }
              jQuery("#"+control).html(appenddata);
               
               
        
              // ProfileStatus('parsedataProfileStatus','progressbar');
            }
            jQuery("#Typingupdateon").html(typohead);
            }
    }

    function EditTyping(TypingStenoId,Lang_id,Type_id,Institute,PassedYear,Speed) { 
        modal('Typing');
        
        $("#language").prop("disabled", true);
        $("#type").prop("disabled", true);
           localStorage.setItem("TypingStenoId",TypingStenoId);     
           $("#language").val(Lang_id)
           $("#type").val(Type_id)
           $("#Institute").val(decodeURI(Institute)); 
           $("#passYear").val(decodeURI(PassedYear))
           $("#Speed").val(decodeURI(Speed))

        }


   function FillPassingYear(funct,control) {
            var path =  serverpath + "secured/year/0/20"
            securedajaxget(path,funct,'comment',control);
      }
        
   function parsedatasecuredFillPassingYear(data,control){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FillPassingYear('parsedatasecuredFillPassingYear','passYear');
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
               else if(data.errno) {
                    toastr.warning("Something went wrong Please try again later", "", "info")
                    return false;
                }
            
                else{
                    var data1 = data[0];
                    jQuery("#"+control).append(jQuery("<option ></option>").val("NA").html("Select Passed Year"));
                    for (var i = 0; i < data1.length; i++) {
                        jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].FinYearId).html(data1[i].Year));
                     }
                }
                      
      }

               
   function FillTSLanguage(funct,control) {
    var path =  serverpath + "secured/Languagetype/0/0"
    securedajaxget(path,funct,'comment',control);
      }
  
  function parsedatasecuredFillTSLanguage(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillTSLanguage('parsedatasecuredFillTSLanguage','language');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Language"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Lang_id).html(data1[i].Lang_name));
             }
        }
              
  }

  function FillTSType(funct,control) {
    var path =  serverpath + "secured/typingstenomaster/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillTSType(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillTSType('parsedatasecuredFillTSType','type');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select type"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Type_id).html(data1[i].Type));
             }
        }
              
  }

  function FillTSInstitute(funct,control) {
    var path =  serverpath + "secured/TypingInstitute/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillTSInstitute(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillTSInstitute('parsedatasecuredFillTSInstitute','Institute');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Institute"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].TS_Institute_id).html(data1[i].TS_Institute_Name));
             }
        }
              
  }


  function TypingStenoAPI(){
    FetchTyping('parsedatasecuredFetchTyping','Typingdetail');
    FillPassingYear('parsedatasecuredFillPassingYear','passYear');
    FillTSLanguage('parsedatasecuredFillTSLanguage','language');
    FillTSType('parsedatasecuredFillTSType','type');
    FillTSInstitute('parsedatasecuredFillTSInstitute','Institute');

  }