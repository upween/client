function FillJobs(funct, control, Type) {
    var path = EmployerPath + "Jobs/0/'Location'"
    securedajaxget(path, funct, 'comment', control);
    if (Type == 'Recommended') {
        jQuery("#JobType").html("Recommended Jobs");
    }
    if (Type == 'Applied') {
        jQuery("#JobType").html("Applied Jobs");
    }
    if (Type == 'Saved') {
        jQuery("#JobType").html("Saved Jobs");
    }
    if (Type == 'New') {
        jQuery("#JobType").html("New Jobs");
    }
}
function parsedatasecuredjobs(data, control) {
    data = JSON.parse(data)

    var data1 = data[0];

    var appenddata = "";
    jQuery("#" + control).empty();
    for (var i = 0; i < data1.length; i++) {
        appenddata += "<li style='cursor: pointer;'><div class='row'><div onclick=SetJobId('"+data1[i].JobId+"')  class='col-md-8 col-sm-8'><div class='jobimg'><img src='images/jobs/jobimg.jpg' style='max-width:83%' alt='Job Name'></div><div class='jobinfo'><h3><a href='#.'>" + data1[i].JobTitle + "</a></h3><div class='companyName'><a href='" + data1[i].Website + "' target='_blank'>" + data1[i].Name + "</a></div><div class='location' style='    padding-left: 70px' ><div class='col-md-4' style='margin-top: 3px;'><i class='fa fa-briefcase' aria-hidden='true'></i><span>  " + data1[i].WorkExpMin + " yrs - " + data1[i].WorkExpMax + " yrs</span></div><div class='col-md-4' style='margin-top: 3px;'><i class='fa fa-map-marker' aria-hidden='true'></i><span> " + data1[i].Location + "</span></div></div></div><div class='clearfix'></div> </div><div class='col-md-4 col-sm-4'><div class='listbtn'><a href='#.'>Apply Now</a></div></div><div class='col-md-4 col-sm-4' style='margin-top: 2%'><span style='margin-left:-2%;color: #999;'>Posted By- " + data1[i].ContactPerson + "</span></div><i class='fa fa-pencil' style='padding-left:100px;padding-right:5px;padding-top:5px'></i><span>" + data1[i].Keywords + "</span><div class='location'><div class='col-md-4' style='margin-top: 3px;'><i class='fa fa-money' style=' padding-left: 81px;padding-top:5px'></i><span style='padding-left:5px'>INR " + data1[i].AnnualCTCMin + " - " + data1[i].AnnualCTCMax + " CTC</span></div></div></div></li>";
    }
    jQuery("#" + control).html(appenddata);



}
function SetJobId(Id){
    sessionStorage.setItem("JobId",Id);
    window.location='/JobDetail';
}