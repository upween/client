function InsUpdCounsellingform() {


    $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    
    var MasterData = {
        "p_CandidateName":$('#candidatename').val(),
        "p_DateOfBirth":$('#DOBtextbox').val(),
        "p_Address":$('#address').val(),
        "p_ContactNumber":$('#contactnumber').val(),
        "p_MaritalStatus":$('#maritalstatus').val(),
        "p_Category":$('#category').val(),
        "p_EducationalQualification":$('#edcationhalqualification').val(),
        "p_Training":$('#training').val(),
        "p_Experience":$('#experience').val(),
        "p_Problem":$('#problems').val(),
        "p_Advice":$('#advice').val(),
        "p_HighestScore_1":$('#HighestScore_1').val(),
        "p_HighestScore_2":$('#HighestScore_2').val(),
        "p_Difficulty_1":$('#difficulty_1').val(),
        "p_Difficulty_2":$('#difficulty_2').val(),
        "p_Health":$('#health').val(),
        "p_Temperament_1":$('#Temperament_1').val(),
        "p_Temperament_2":$('#Temperament_2').val(),
        "p_Temperament_3":$('#Temperament_3').val(),
        "p_Temperament_4":$("input[name='checkbox']:checked").val(),
        "p_Interest_1_1":$('#Interest_1_1').val(),
        "p_Interest_1_2":$('#Interest_1_2').val(),
        "p_Interest_2_1":$('#Interest_2_1').val(),
        "p_Interest_2_2":$('#Interest_2_2').val(),
        "p_Interest_3":$('#Interest_3').val(),
        "p_Interest_4":$('#Interest_4').val(),
        "p_Interest_5_1":$("input[name='interested']:checked").val(),
        "p_Interest_5_2":$("input[name='interested']:checked").val(),
        "p_Problem_1":$('#Problem_1').val(),
        "p_Problem_2":$('#Problem_2').val(),
        "p_Problem_3_1":$('#Problem_3_1').val(),
        "p_Problem_3_2":$('#Problem_3_2').val(),
        "p_Problem_4_1":$('#Problem_4_1').val(),
        "p_Problem_4_2":$('#Problem_4_2').val(),
        "p_EnteredOn":date,
        "p_EnteredIP":sessionStorage.ipAddress,
        "p_CC_Id":sessionStorage.Id?sessionStorage.Id:0
        }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "counsellingform";
    securedajaxpost(path, 'parsrdatacounsellingform', 'comment', MasterData, 'control')
}
function parsrdatacounsellingform(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        InsUpdCounsellingform();
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    else if (data[0][0].ReturnValue == "1") {
      
        InsUpdeducationdetail(data[0][0].CandidateId) ;
        insFamilydetail(data[0][0].CandidateId);
    }
    else if (data[0][0].ReturnValue == "2") {
      
toastr.warning('Candidate Details Already Exists')
  }
  
   

}
function addNewProductRow() {
    var x = $('.addproductrow').length;
    var productrow = `<tr class="addproductrow" id='productrow`+ x+`'>
  <td>
  `+[x+2]+`
  </td>
  <td style='    width: 18%;'>
  <select  id="college`+x+`" class="form-control" >
  <option value="पीजी">पीजी</option>
  <option value="ग्रेजुएट">ग्रेजुएट</option>
  <option value="12वी">12वी</option>
  <option value="10वी">10वी</option>
  <option value="मिडिल">मिडिल</option>
  <option value="पीजी डि">पीजी डि</option>
  <option value="डिप्लोमा">डिप्लोमा</option>
  <option value="प्रमाण पत्र">प्रमाण पत्र</option>
  <option value="अन्य">अन्य</option>
</select>

  </td>
  <td>
    <input type="text" id="examlevel`+x+`" class="form-control" placeholder="" />
  </td>
 
  <td>
    <input type="text" id="subject`+x+`" class="form-control" placeholder="" />
  </td>
  <td>
    <input type="text" id="percetage`+x+`" class="form-control" placeholder="" />
  </td>
  <td>
    <input type="text" id="grade`+x+`" class="form-control" placeholder="" />
  </td>
  <td>
    <input type="submit"  class="btn" value="हटाओ" onclick="$('#productrow`+ x+`').remove(); "  />
  </td>
</tr>`;
    $('.educationdetail').append(productrow);
}
function InsUpdeducationdetail(id) {


    $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);
 
    $('.addproductrow').each(function (i) {
    var MasterData = {
        
            "p_CandidateId":id,
            "p_Education":$('#college'+i+'').val(),
            "p_Year":$('#examlevel'+i+'').val(),
            "p_Subject":$('#subject'+i+'').val(),
            "p_Percentage":$('#percetage'+i+'').val(),
            "p_Division":$('#grade'+i+'').val()
            
        
        
        }
    MasterData = JSON.stringify(MasterData)
    console.log(MasterData)
    var path = serverpath + "counsellingeducation";
    securedajaxpost(path, 'parsrdataInsUpdeducationdetail', 'comment', MasterData, 'control')

 });
 }
function parsrdataInsUpdeducationdetail(data) {
    data = JSON.parse(data)

    console.log(data)

  
   

}

    function insFamilydetail(id) {


        $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);
     
        $('.addproductrowfamily').each(function (i) {
        var MasterData = {
            "p_CandidateId":id,
            "p_Relation":$('#relation'+i+'').val(),
            "p_Age":$('#age'+i+'').val(),
            "p_Education":$('#qual'+i+'').val(),
            "p_Profession":$('#occupation'+i+'').val(),
            "p_Salary":$('#salary'+i+'').val()
            
        
        }
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "counsellingfamily";
        securedajaxpost(path, 'parsrdatainsFamilydetail', 'comment', MasterData, 'control')
   
     });
     resetMode();
     }
    function parsrdatainsFamilydetail(data) {
        data = JSON.parse(data)
        console.log(data)
        if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        
        else{
            console.log(data)
            toastr.success("Insert Successful", "", "success")
            return true;
        }
      
       
    
    }

    function addNewfamily() {
        var x = $('.addproductrowfamily').length;
        var productrow = `<tr class="addproductrowfamily" id='familyrow`+ x+`'>
     
      <td style='    width: 18%;'>
      <select  id="relation`+x+`" class="form-control">
      <option value="स्वयं ">स्वयं</option>
      <option value="पति/पत्नी ">पति/पत्नी</option>
      <option value="पिता">पिता</option>
      <option value="माता">माता</option>

      <option value="भाई">भाई</option>
      <option value="बहन">बहन</option>
    </select></td>
      <td>
        <input type="text" id="age`+x+`" class="form-control" placeholder="" />
      </td>
      <td>
        <input type="text" id="qual`+x+`" class="form-control" placeholder="" />
      </td>
      <td>
        <input type="text" id="occupation`+x+`" class="form-control" placeholder="" />
      </td>
      <td>
        <input type="text" id="salary`+x+`" class="form-control" placeholder="" />
      </td>
      <td>
        <input type="submit"  class="btn" value="हटाओ" onclick="$('#familyrow`+ x+`').remove(); "  />
      </td>
    </tr>`;
        $('.familydetail').append(productrow);
    }
  
function FillMaritalstatus(funct,control) {
  var path =  serverpath + "Maritalstatus/0/0"
  securedajaxget(path,funct,'comment',control);
 }
 
 function parsedatasecuredFillMaritalstatus(data,control){  
  data = JSON.parse(data)
  if (data.message == "New token generated"){
      sessionStorage.setItem("token", data.data.token);
      FillMaritalstatus('parsedatasecuredFillMaritalstatus','Marital');
  }
  else if (data.status == 401){
      toastr.warning("Unauthorized", "", "info")
      return true;
  }
    else if(data.errno) {
         toastr.warning("Something went wrong Please try again later", "", "info")
         return false;
     }
 
      else{
          jQuery("#"+control).empty();
          var data1 = data[0];
          jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Marital Status"));
          for (var i = 0; i < data1.length; i++) {
              jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Marital_Status).html(data1[i].Marital_Status));
           }
      }
            
 }
 function FillCategory(funct,control) {
  var path =  serverpath + "category/0/0/0/0"
  securedajaxget(path,funct,'comment',control);
 }
 
 function parsedatasecuredFillCategory(data,control){  
  data = JSON.parse(data)
  if (data.message == "New token generated"){
      sessionStorage.setItem("token", data.data.token);
      FillCategory('parsedatasecuredFillCategory','Category');
  }
  else if (data.status == 401){
      toastr.warning("Unauthorized", "", "info")
      return true;
  }
    else if(data.errno) {
         toastr.warning("Something went wrong Please try again later", "", "info")
         return false;
     }
 
      else{
          jQuery("#"+control).empty();
          var data1 = data[0];
          jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Category"));
          for (var i = 0; i < data1.length; i++) {
              jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].CategoryName).html(data1[i].CategoryName));
           }
      }
            
 }
 function Checkcounselling(){
  if(isValidate){
      if ($.trim(jQuery('#candidatename').val()) == "") {
          getvalidated('candidatename','text','Name');
          return false;
       }
      if ($.trim(jQuery('#DOBtextbox').val()) == "") {
          getvalidated('DOBtextbox','text','Date of Birth');
          return false; }
      if ($.trim(jQuery('#address').val()) == "0") {
          getvalidated('address','text','Address');
          return false; }
          if ($.trim(jQuery('#contactnumber').val()) == "") {
            getvalidated('contactnumber','number','Contact Number');
            return false;
         }
        if ($.trim(jQuery('#maritalstatus').val()) == "0") {
            getvalidated('maritalstatus','text','Marital Status');
            return false; }
        if ($.trim(jQuery('#category').val()) == "0") {
            getvalidated('category','number','Category');
            return false; }
          
      else{
        InsUpdCounsellingform() ;
      }
  }
  else{
    InsUpdCounsellingform() ;
  } 

}
function resetMode(){
  $('#candidatename').val('')
  $('#DOBtextbox').val('')
  $('#address').val('')
$('#contactnumber').val('')
$('#maritalstatus').val('0')
 $('#category').val('0')
$('#edcationhalqualification').val('')
$('#training').val('')
$('#experience').val('')
$('#problems').val('')
$('#advice').val('')
$('#HighestScore_1').val('')
$('#HighestScore_2').val('')
$('#difficulty_1').val('')
$('#difficulty_2').val('')
$('#health').val('')
$('#Temperament_1').val('')
$('#Temperament_2').val('')
$('#Temperament_3').val('')
$("input[name='checkbox']").checked=false
$('#Interest_1_1').val('')
$('#Interest_1_2').val('')
$('#Interest_2_1').val('')
$('#Interest_2_2').val('')
$('#Interest_3').val('')
$('#Interest_4').val('')
$("input[name='interested']:checked").checked=false
$('#Problem_1').val('')
$('#Problem_2').val('')
$('#Problem_3_1').val('')
$('#Problem_3_2').val('')
$('#Problem_4_1').val('')
$('#Problem_4_2').val('')
  $('.addproductrowfamily').each(function (i) {
   $('#relation'+i+'').val(''),
       $('#age'+i+'').val(''),
     $('#qual'+i+'').val(''),
     $('#occupation'+i+'').val(''),
       $('#salary'+i+'').val('')
       $('#familyrow'+ i+'').remove(); 
    
    });
    $('.addproductrow').each(function (i) {
     
         
            $('#college'+i+'').val(''),
         $('#examlevel'+i+'').val(''),
            $('#subject'+i+'').val(''),
            $('#percetage'+i+'').val(''),
             $('#grade'+i+'').val('')
             $('#productrow'+ i+'').remove(); 
          
          
          });
        }