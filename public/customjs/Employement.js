


function CheckEmployement()    //Sonali 
{
    if(isValidate){

    
   if ($("#iscurrentcompany").val() == "1") {
   if ($.trim(jQuery('#Designation').val()) == "" ) {
       getvalidated('Designation','text','Designation')
   return false;}
   if (sessionStorage.getItem("DesignationId") == '0') {
       $("#Designation").css('border-color', 'red');
       $("#validDesignation").html("Please enter proper Designation ");
       return false;}
   if ($.trim(jQuery('#Organization').val())== '') {
       getvalidated('Organization','text','Organization')
   return false;} 
  
   if (jQuery('#startedworkinginyear').val() == '0') {
       getvalidated('startedworkinginyear','select','Started work in year') 
   return false;}
   if (jQuery('#startedworkinginmonth').val() == '0') {
       getvalidated('startedworkinginmonth','select','Started work in Month')
   return false;}
   if (jQuery('#inlacs').val() == '0') {
       getvalidated('inlacs','select','Salary in lacs') 
   return false;}
   if (jQuery('#inthousands').val() == '0') {
       getvalidated('inthousands','select','Salary in Thousand')
   return false;}
   if ($.trim(jQuery('#JobProfile').val()) == '') {
       getvalidated('JobProfile','text','JobProfile')
   return false;}
   if (jQuery('#noticeperiod').val() == '0') {
       getvalidated('noticeperiod','select','Notice Period')
   return false;}
   else{
       InsUpdEmployement();
   }

   }
else if ($("#iscurrentcompany").val() == "2" || $("#iscurrentcompany").val() == "0" ) {
       if (jQuery('#Designation').val() == "" ) {
           getvalidated('Designation','text','Designation')
       return false;}
       if (sessionStorage.getItem("DesignationId") == '0') {
           $("#Designation").css('border-color', 'red');
           $("#validDesignation").html("Please enter proper Designation ");
           return false;}
       if (jQuery('#Organization').val() == '') {
           getvalidated('Organization','text','Organization')
       return false;} 
       
       if (jQuery('#startedworkinginyear').val() == '0') {
           getvalidated('startedworkinginyear','select','Started work in year') 
       return false;}
       if (jQuery('#startedworkinginmonth').val() == '0') {
           getvalidated('startedworkinginmonth','select','Started work in Month')
       return false;}
       if (jQuery('#workinyear').val() == '0') {
           getvalidated('workinyear','select','Work Till in year')
       return false;}
       if (jQuery('#workinmonth').val() == '0') {
           getvalidated('workinmonth','select','Work Till in Month')
       return false;}
       if (jQuery('#inlacs').val() == '0') {
           getvalidated('inlacs','select','Salary in lacs') 
       return false;}
       if (jQuery('#inthousands').val() == '0') {
           getvalidated('inthousands','select','Salary in Thousand')
       return false;}
       if (jQuery('#JobProfile').val() == '') {
           getvalidated('JobProfile','text','Job Profile')
       return false;}
       else{
           InsUpdEmployement();
       }
       }
    }
    else{
       InsUpdEmployement();
    }
   
   
}
function FillDesignationSecured(funct) {
   var path = serverpath + "secured/adminDesignation/0/1"
   securedajaxget(path, funct, 'comment', 'control');
}
function parsedatasecureddesignation(data, control) {
   data = JSON.parse(data)
   if (data.message == "New token generated") {
       sessionStorage.setItem("token", data.data.token);
       FillDesignationSecured('parsedatasecureddesignation');
   }
   else if (data.status == 401) {
       toastr.warning("Unauthorized", "", "info")
       return true;
   }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

   else {
       if (data.length > 0) {
           sessionStorage.setItem('Designation', JSON.stringify(data[0]));
       }
   }
}
jQuery("#Designation").keyup(function () {
   sessionStorage.setItem('DesignationId', '0');
});

jQuery("#Designation").typeahead({
   source: function (query, process) {
       var data = sessionStorage.getItem('Designation');
       designation = [];
       map = {};
       var Designation = "";
       
       jQuery.each(jQuery.parseJSON(data), function (i, Designation) {
           map[Designation.Designation] = Designation;
          designation.push(Designation.Designation);
       
       });
       process(designation);
   },
   minLength: 3,
   updater: function (item) {
       sessionStorage.setItem('DesignationId', map[item].Designation_Id);
       return item;
   },
   
   
});

function InsUpdEmployement() {
   if ($("#iscurrentcompany").val() == "1" ) {
       var MasterData = {
           "p_EmploymentId": localStorage.getItem("EmployementId"),
           "p_CandidateId": sessionStorage.getItem("CandidateId"),
           "p_DesignationId": sessionStorage.getItem("DesignationId"),
           "p_Organisation": jQuery('#Organization').val(),
           "p_IsCurrentCompany": jQuery('#iscurrentcompany option:selected').val(),
           "p_StartedWorkinYear": jQuery('#startedworkinginyear').val(),
           "p_StartedWorkinMonth": jQuery('#startedworkinginmonth').val(),
           "p_WorkedTillinYear": "0",
           "p_WorkedTillinMonth":"0",
           "p_SalaryinLacs": jQuery('#inlacs').val(),
           "p_SalaryinThousand":jQuery('#inthousands').val(),
           "p_ProfileDescription": jQuery('#JobProfile').val(),
           "p_NoticePeriod": jQuery('#noticeperiod option:selected').text()

       }
   }
   else if ($("#iscurrentcompany").val() == "2"  || $("#iscurrentcompany").val() == "0") {
       var MasterData = {
           "p_EmploymentId": localStorage.getItem("EmployementId"),
           "p_CandidateId": sessionStorage.getItem("CandidateId"),
           "p_DesignationId": sessionStorage.getItem("DesignationId"),
           "p_Organisation": jQuery('#Organization').val(),
           "p_IsCurrentCompany": jQuery('#iscurrentcompany option:selected').val(),
           "p_StartedWorkinYear": jQuery('#startedworkinginyear').val(),
           "p_StartedWorkinMonth": jQuery('#startedworkinginmonth').val(),
           "p_WorkedTillinYear": jQuery('#workinyear').val(),
           "p_WorkedTillinMonth": jQuery('#workinmonth').val(),
           "p_SalaryinLacs": jQuery('#inlacs').val(),
           "p_SalaryinThousand":jQuery('#inthousands').val(),
           "p_ProfileDescription": jQuery('#JobProfile').val(),
           "p_NoticePeriod": ''

       }
   }
   MasterData = JSON.stringify(MasterData)
   var path = serverpath + "secured/employement";
   securedajaxpost(path, 'parsrdataemployement', 'comment', MasterData, 'control')
}
function parsrdataemployement(data) {
   data = JSON.parse(data)
   if (data.message == "New token generated") {
       sessionStorage.setItem("token", data.data.token);
       InsUpdEmployement();
   }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

   else if (data[0][0].ReturnValue == "1") {
       ProfileStatus('parsedataProfileStatus','progressbar');
       toastr.success("Submit Successfully", "", "success")
       $('#myModal1').modal('toggle');
       FillEmployementSecured('parsedatasecuredemployement', 'employementdetail');
       resetmodeEmployment();
       return true;
   }

   else if (data[0][0].ReturnValue == "2") {
       toastr.success("Update Successfully", "", "success")
       $('#myModal1').modal('toggle');
       FillEmployementSecured('parsedatasecuredemployement', 'employementdetail');
       FillProfileDetail('parsedatasecuredFillProfileDetail');
       resetmodeEmployment();
       return true;
   }
   else if (data[0][0].ReturnValue == "0") {
     toastr.warning("Employement Detail already Exists", "", "info")
     return true;
   }
   else if (data[0][0].ReturnValue == "4") {
       toastr.warning("Please enter valid details", "", "info")
       return true;
     }
}
function FillEmployementSecured(funct, control) {
   var path = serverpath + "secured/employement/'" + sessionStorage.getItem("CandidateId") + "'"
   securedajaxget(path, funct, 'comment', control);
}
function parsedatasecuredemployement(data, control) {
   data = JSON.parse(data)
   if (data.message == "New token generated") {
       sessionStorage.setItem("token", data.data.token);
       FillEmployementSecured('parsedatasecuredemployement', 'employementdetail');
   }
   else if (data.status == 401) {
       toastr.warning("Unauthorized", "", "info")
       return true;
   }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

   else {
       var employementhead="";
       if (data[0] == "") {
           localStorage.setItem("EmployementId", "0");
           jQuery("#" + control).html("<span lang='en'>Add Experience Detail</span>");
           $("#EmpExperience").hide();
           $("#EmpDesignation").hide();
       }
       else {
           var data1 = data[0];

           var appenddata = "";
            
           jQuery("#" + control).empty();
           for (var i = 0; i < data1.length; i++) {
               if (data1[i].IsCurrentCompany == '1') {
                   appenddata += "<p style='padding-top: 15px;font-weight:bold'>" + data1[i].DesignationName + "<span style='margin-left:10px;'><a href='#employementtab'><i class='fa fa-pencil' onclick=EditEmpDetail('"+data1[i].EmploymentId+"','"+encodeURI(data1[i].DesignationName)+"','"+encodeURI(data1[i].Organisation)+"','"+encodeURI(data1[i].IsCurrentCompany)+"','"+encodeURI(data1[i].StartedWorkinYear)+"','"+encodeURI(data1[i].StartedWorkinMonth)+"','"+encodeURI(data1[i].WorkedTillinYear)+"','"+encodeURI(data1[i].WorkedTillinMonth)+"','"+encodeURI(data1[i].SalaryinLacs)+"','"+encodeURI(data1[0].SalaryinThousand)+"','"+encodeURI(data1[i].ProfileDescription)+"','"+encodeURI(data1[i].NoticePeriod)+"')></i></a></span><span style='margin-left:10px;'><a href='#employementtab' onclick=Delete("+data1[i].EmploymentId+",'Employment')><i class='fa fa-times' style='padding-left:20px;'></i></a></span></p><p >" + data1[i].Organisation + "</p><p class='profileheading'>" + data1[i].StartedWorkinYear + ", "+data1[i].StartedWorkinMonth+" To Present</p><p class='profileheading'>Availabe To join in " + data1[i].NoticePeriod + "</p><p class='profileheading' style='word-break: break-word'>" + data1[i].ProfileDescription + "</p>";
               }
               if (data1[i].IsCurrentCompany == '2') {
                   appenddata += "<p style='padding-top: 15px;font-weight:bold' >" + data1[i].DesignationName + "<span style='margin-left:10px;'><a href='#employementtab'><i onclick=EditEmpDetail('"+data1[i].EmploymentId+"','"+encodeURI(data1[i].DesignationName)+"','"+encodeURI(data1[i].Organisation)+"','"+encodeURI(data1[i].IsCurrentCompany)+"','"+encodeURI(data1[i].StartedWorkinYear)+"','"+encodeURI(data1[i].StartedWorkinMonth)+"','"+encodeURI(data1[i].WorkedTillinYear)+"','"+encodeURI(data1[i].WorkedTillinMonth)+"','"+encodeURI(data1[i].SalaryinLacs)+"','"+encodeURI(data1[0].SalaryinThousand)+"','"+encodeURI(data1[i].ProfileDescription)+"','"+encodeURI(data1[i].NoticePeriod)+"') class='fa fa-pencil'></i></a></span><span style='margin-left:10px;'><a href='#employementtab' onclick=Delete("+data1[i].EmploymentId+",'Employment') ><i class='fa fa-times' style='padding-left:20px;'></i></a></span></p><p class='profileheading'>" + data1[i].Organisation + "</p><p class='profileheading'>" + data1[i].StartedWorkinYear + ", "+data1[i].StartedWorkinMonth+" To " + data1[i].WorkedTillinYear+", "+data1[i].WorkedTillinMonth+"</p><p class='profileheading' style='word-break: break-word'>" + data1[i].ProfileDescription + "</p>";
               }
           }
           jQuery("#" + control).html(appenddata);

           employementhead += "<span lang='en'>Updated On</span> " + data[0][0].UpdateOn + "";
           
           
     //  ProfileStatus('parsedataProfileStatus','progressbar');
       }
       jQuery("#emphd").html(employementhead);
   }
}
function EditEmpDetail(EmployementId,DesignationName,Organisation,Iscurrentcompany,StartedWorkinYear,StartedWorkinMonth,WorkedTillinYear,WorkedTillinMonth,SalaryinLacs,SalaryinThousand,JobProfile,NoticePeriod) {
//Sonali
   modal('employement');
   localStorage.setItem("EmployementId",EmployementId);
    jQuery('#Designation').val(decodeURI(DesignationName)),
    jQuery('#Organization').val(decodeURI(Organisation)),
    jQuery('#iscurrentcompany').val(decodeURI(Iscurrentcompany)),
    jQuery('#startedworkinginyear').val(decodeURI(StartedWorkinYear)),
    jQuery('#startedworkinginmonth').val(decodeURI(StartedWorkinMonth)),
    jQuery('#workinyear').val(decodeURI(WorkedTillinYear)),
    jQuery('#workinmonth').val(decodeURI(WorkedTillinMonth)),
    jQuery('#inlacs').val(decodeURI(SalaryinLacs)),
    jQuery('#inthousands').val(decodeURI(SalaryinThousand)),
    jQuery('#JobProfile').val(decodeURI(JobProfile)),
    jQuery('#noticeperiod').val(decodeURI(NoticePeriod))
    $("#Designation").prop("disabled", true);
    $("#Organization").prop("disabled", true);

    if (decodeURI(Iscurrentcompany)== "1") {
       $("#workingyr").hide();
       $("#present").show();
      
       $("#ntcperd").show();
   }
   else if (decodeURI(Iscurrentcompany) == "2") {
       $("#workingyr").show();
       $("#present").hide();
       
       $("#ntcperd").hide();

   }
}

function resetmodeEmployment() {
   localStorage.setItem("EmployementId",'0'),
  
   $("#Designation").prop("disabled", false);
   $("#Organization").prop("disabled", false);
   jQuery('#Designation').val(""),
   jQuery('#Organization').val(""),
   jQuery('#iscurrentcompany').val("0"),
   jQuery('#startedworkinginyear').val("0"),
   jQuery('#startedworkinginmonth').val("0"),
   jQuery('#workinyear').val("0"),
   jQuery('#workinmonth').val("0"),
   jQuery('#inlacs').val("0"),
   jQuery('#inthousands').val("0"),
   jQuery('#JobProfile').val(""),
   jQuery('#noticeperiod').val("0")
   $("#present").hide();
  
    $("#ntcperd").hide();

    jQuery('#Designation').css('border-color', '');
    jQuery('#Organization').css('border-color', '');
    jQuery('#iscurrentcompany').css('border-color', '');
    jQuery('#startedworkinginyear').css('border-color', '');
    jQuery('#startedworkinginmonth').css('border-color', '');
    jQuery('#JobProfile').css('border-color', '');

        jQuery('#workinyear').css('border-color', '');
       $('#validworkinyear').html("");
       jQuery('#workinmonth').css('border-color', '');
       $('#validworkinmonth').html("");

       jQuery('#inlacs').css('border-color', '');
       $('#validinlacs').html("");
       jQuery('#inthousands').css('border-color', '');
       $('#validinthousands').html("");

    $('#validDesignation').html("");
    $("#validOrganization").html("");
    $("#validiscurrentcompany").html("");
    $("#validstartedworkinginyear").html("");
    $("#validstartedworkinginmonth").html("");
    $("#validJobProfile").html("");
}

function FillIscurrentcompany(funct,control) {
   var path =  serverpath + "secured/Status/0/0"
   securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillIscurrentcompany(data,control){  
   data = JSON.parse(data)
   if (data.message == "New token generated"){
       sessionStorage.setItem("token", data.data.token);
       FillIscurrentcompany('parsedatasecuredFillIscurrentcompany','iscurrentcompany');
   }
   else if (data.status == 401){
       toastr.warning("Unauthorized", "", "info")
       return true;
   }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

       else{
           jQuery("#"+control).empty();
           var data1 = data[0];
           jQuery("#"+control).append(jQuery("<option ></option>").val("0").html(" Is your Current Company? "));
           for (var i = 0; i < data1.length; i++) {
               jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Status_id).html(data1[i].Status_name));
            }
       }
             
}


function Fillstartedworkinginyear(funct,control) {
   var path =  serverpath + "secured/year/0/20"
   securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillstartedworkinginyear(data){  
   data = JSON.parse(data)
   if (data.message == "New token generated"){
       sessionStorage.setItem("token", data.data.token);
       Fillstartedworkinginyear('parsedatasecuredFillstartedworkinginyear');
   }
   else if (data.status == 401){
       toastr.warning("Unauthorized", "", "info")
       return true;
   }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

       else{
           jQuery("#startedworkinginyear").empty();
           var data1 = data[0];
           jQuery("#startedworkinginyear").append(jQuery("<option ></option>").val("0").html("Select Started Working From Year"));
           for (var i = 0; i < data1.length; i++) {
               jQuery("#startedworkinginyear").append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
            }

            
       }
             
}

function FillStartedworkinginmonth(funct,control) {
   var path =  serverpath + "secured/month/0"
   securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillStartedworkinginmonth(data){  
   data = JSON.parse(data)
   if (data.message == "New token generated"){
       sessionStorage.setItem("token", data.data.token);
       FillStartedworkinginmonth('parsedatasecuredFillStartedworkinginmonth');
   }
   else if (data.status == 401){
       toastr.warning("Unauthorized", "", "info")
       return true;
   }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

       else{
           jQuery("#startedworkinginmonth").empty();
           var data1 = data[0];
           jQuery("#startedworkinginmonth").append(jQuery("<option ></option>").val("0").html("Started Working From Month"));
           for (var i = 0; i < data1.length; i++) {
               jQuery("#startedworkinginmonth").append(jQuery("<option></option>").val(data1[i].ShortMonthName).html(data1[i].ShortMonthName));
            }

            jQuery("#workinmonth").empty();
            var data1 = data[0];
            jQuery("#workinmonth").append(jQuery("<option ></option>").val("0").html("Worked Till Month"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#workinmonth").append(jQuery("<option></option>").val(data1[i].ShortMonthName).html(data1[i].ShortMonthName));
             }
       }
             
}

function FillSalaryinlacs(funct,control) {
   var path =  serverpath + "secured/salary/0/0"
   securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillSalaryinlacs(data){  
   data = JSON.parse(data)
   if (data.message == "New token generated"){
       sessionStorage.setItem("token", data.data.token);
       FillSalaryinlacs('parsedatasecuredFillSalaryinlacs');
   }
   else if (data.status == 401){
       toastr.warning("Unauthorized", "", "info")
       return true;
   }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

       else{
           jQuery("#inlacs").empty();
           var data1 = data[0];
           jQuery("#inlacs").append(jQuery("<option ></option>").val("0").html("Salary in Lacs"));
           for (var i = 0; i < data1.length; i++) {
               jQuery("#inlacs").append(jQuery("<option></option>").val(data1[i].salary_l).html(data1[i].salary_l));
            }

            jQuery("#inthousands").empty();
            var data1 = data[0];
            jQuery("#inthousands").append(jQuery("<option ></option>").val("0").html("Select Salary in Thousands "));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#inthousands").append(jQuery("<option></option>").val(data1[i].salary_t).html(data1[i].salary_t));
             }
       }
             
}
function Fillworkinyear(funct,control) {
   var path =  serverpath + "secured/year/0/"+control+""
   securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredworkinyear(data,control){  
   data = JSON.parse(data)
   if (data.message == "New token generated"){
       sessionStorage.setItem("token", data.data.token);
       Fillworkinyear('parsedatasecuredworkinyear',control);
   }
   else if (data.status == 401){
       toastr.warning("Unauthorized", "", "info")
       return true;
   }
   else if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }

       else{
          
            jQuery("#workinyear").empty();
            var data1 = data[0];
            jQuery("#workinyear").append(jQuery("<option ></option>").val("0").html("Worked Till Year"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#workinyear").append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
             }
       }
             
}
function CheckWorkingYear(){
   var currentyear=new Date().getFullYear()
   var startedyear=$("#startedworkinginyear").val()
   var difference = currentyear - startedyear;
   Fillworkinyear('parsedatasecuredworkinyear',difference+1);
}


function employementAPI(){
    FillDesignationSecured('parsedatasecureddesignation');
    FillEmployementSecured('parsedatasecuredemployement', 'employementdetail');
    FillIscurrentcompany('parsedatasecuredFillIscurrentcompany','iscurrentcompany');
    Fillstartedworkinginyear('parsedatasecuredFillstartedworkinginyear');
    FillStartedworkinginmonth('parsedatasecuredFillStartedworkinginmonth');
    FillSalaryinlacs('parsedatasecuredFillSalaryinlacs');
    Fillworkinyear('parsedatasecuredworkinyear', '20');

}