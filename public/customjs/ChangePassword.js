function UpdChangePassword() {

    var MasterData = {
        
        "p_Password":md5(jQuery("#CurrentPassword").val()),
        "p_CandidateId": sessionStorage.getItem("CandidateId"),
        "p_NewPassword": md5(jQuery("#passwordfield").val()),
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/changepassword";
    securedajaxpost(path, 'parsrdatachangepassword', 'comment', MasterData, 'control')
}
function parsrdatachangepassword(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        UpdChangePassword();
    }
    else if (data[0][0].ReturnValue == "1") {
        $('#modalChangepassword').modal('toggle');
        sessionStorage.setItem("TypeHome","Change")
        location.reload();
     resetmode();
       toastr.success("Password Changed Successfully", "", "success")
        return true;
    }
    else if (data[0][0].ReturnValue == "2") {
        resetmode();
        toastr.warning("Wrong Current Password ", "", "info")
        return true;
    }

}
$(".toggle-password").click(function () {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});
function resetmode() {

    createcaptchaheader() ;
    jQuery("#CurrentPassword").val(""),
    jQuery("#passwordfield").val(""),
    jQuery("#Repassword").val(""),
    jQuery("#cpatchaTextBoxheader").val("")

}
function CheckChangePassword(){
    if(isValidate){

    
    if (jQuery('#CurrentPassword').val() == "") {
        getvalidated('CurrentPassword','text','Current Password');
        return false;
    }
    if (jQuery('#passwordfield').val() == "") {
        getvalidated('passwordfield','text','New Password');
        return false;
    }
   else{
        var password = $("#passwordfield").val();
        if (password != '') {
            var regularExpression = /^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$/;
          
            if (!regularExpression.test(password)) {
                $("#passwordvalidate").show();
                return false;
            }
            else {
                $("#passwordvalidate").hide();
                if (jQuery('#Repassword').val() == "") {
                    getvalidated('Repassword','text','Re-Password');
                    return false;
                }
                if (jQuery('#Repassword').val() !=jQuery('#passwordfield').val()) {
                    $('#validRepassword').html("Password Not Match") ;
                    jQuery('#Repassword').css('border-color', 'red');
                    return false;
                }
                else{
                    $('#validRepassword').html("") ;
                    jQuery('#Repassword').css('border-color', '');
                    if(isCaptchaValidated){
                        if (jQuery('#cpatchaTextBoxheader').val() == "") {
                            getvalidated('cpatchaTextBoxheader','text','Captcha');
                            return false;
                        }
                        else{
                          
                            validateCaptchaheader();
                        }
                    }
                    else{
                        UpdChangePassword();
                    }
                   
                }
            }
         }
      }
    }
    else{
        UpdChangePassword()
    }
   }
var codeheader;
                function createcaptchaheader() {
                  //clear the contents of captcha div first 
                  document.getElementById('captchaheader').innerHTML = "";
                  var charsArray =
                  "0123456789";
                  var lengthOtp = 6;
                  var captcha = [];
                  for (var i = 0; i < lengthOtp; i++) {
                    //below code will not allow Repetition of Characters
                    var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
                    if (captcha.indexOf(charsArray[index]) == -1)
                      captcha.push(charsArray[index]);
                    else i--;
                  }
                  var canv = document.createElement("canvas");
                  canv.id = "captchaheader";
                  canv.width = 100;
                  canv.height = 50;
                  var ctx = canv.getContext("2d");
                  ctx.font = "25px Georgia";
                  ctx.strokeText(captcha.join(""), 0, 30);
                  //storing captcha so that can validate you can save it somewhere else according to your specific requirements
                  codeheader = captcha.join("");
                  document.getElementById("captchaheader").appendChild(canv); // adds the canvas to the body element
                }
                function validateCaptchaheader() {
                  event.preventDefault();
                  
                  if (document.getElementById("cpatchaTextBoxheader").value == codeheader) {
                    UpdChangePassword();
                 
                  }else{
                    $("#cpatchaTextBoxheader").val('');
                    $("#cpatchaTextBoxheader").css('border-color', 'red');
                    $("#validcpatchaTextBoxheader").html("Please enter Valid Captcha");
                    createcaptchaheader();
                    
                  
                    
                  }
                }
                $("#passwordfield").focusout(function () {
                    getvalidated('passwordfield','text','New Password');
                    var password = $("#passwordfield").val();
                    if (password != '') {
                        var regularExpression = /^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$/;
                      
                        if (!regularExpression.test(password)) {
                            $("#passwordvalidate").show();
                            return false;
                        }
                        else {
                            $("#passwordvalidate").hide();

                        }
                    }
                });   
                $("#passwordfield").keyup(function () {
                    var password = $("#passwordfield").val();
                    if (password != '') {
                        var regularExpression = /^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$/;
                      
                        if (!regularExpression.test(password)) {
                            $("#passwordvalidate").show();
                            return false;
                        }
                        else {
                            $("#passwordvalidate").hide();

                        }
                    }
                });     
                function validatepassword(){
                    var password = $("#passwordfield").val();
                    if (password != '') {
                        var regularExpression = /^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$/;
                      
                        if (!regularExpression.test(password)) {
                            $("#passwordvalidate").show();
                            return false;
                        }
                        else {
                            $("#passwordvalidate").hide();
                            validateCaptchaheader() ;
                        }
                    }
                }  


                $(".toggle-password").click(function () {
                    $(this).toggleClass("fa-eye fa-eye-slash");
                    var input = $($(this).attr("toggle"));
                    if (input.attr("type") == "password") {
                        input.attr("type", "text");
                    } else {
                        input.attr("type", "password");
                    }
                    });