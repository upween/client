
function CheckValidation(){

    if (jQuery('#Head').val() == ''){
        $(window).scrollTop(0);
        getvalidated('Head','text','Head');
    return false;}
     if (jQuery('#Body').val() == '') {
        $(window).scrollTop(0);
        getvalidated('Body','text','Body')
    return false;}
     if (jQuery('#Usefullinks').val() == '') {
        getvalidated('Usefullinks','text','Link');
    return false;}
     if (jQuery('#fromdate').val() == '') {
        getvalidated('fromdate','text','From date')
    return false;}
     if (jQuery('#todate').val() == '') {
        getvalidated('todate','text','To date');
    return false;}
        else{
        InsUpdjobfairs();
       
    }
}
function InsUpdjobfairs() {

   
    var MasterData = {
        "p_JobFairId":'0',
        "p_Head": jQuery("#Head").val(),
        "p_Details": jQuery("#Body").val(),
        "p_Links": jQuery("#Usefullinks").val(),
        "p_FromDate": jQuery("#fromdate").val(),
        "p_ToDate": jQuery("#todate").val(),
        "p_IpAddress": sessionStorage.getItem('IpAddress'),
    };

    jQuery.ajax({
        url: serverpath + 'jobfair',
        type: "POST",
        data: JSON.stringify(MasterData),
        contentType: "application/json; charset=utf-8",
        success: function (data, status, jqXHR) {
            resetMode();
            if(data[0][0].ReturnValue == "1"){
                toastr.success("Insert Successful", "", "success")
                return true;
            }
        },
          error: function (result) {
           
            toastr.success(result, "", "success")
        }
    });

}
function resetMode() {
    $("#btnSubmit").html('Submit');
    jQuery("#Head").val("");
     jQuery("#Body").val("");
     jQuery("#Usefullinks").val("");
     jQuery("#fromdate").val("");
     jQuery("#todate").val(""); 
     jQuery('#todate').css('border-color', '');
     jQuery('#validtodate').html('');
    }


    var dateToday = new Date();
var dates = $("#fromdate, #todate").datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    numberOfMonths: 1,
    minDate: dateToday,
    onSelect: function(selectedDate) {
        var option = this.id == "fromdate" ? "minDate" : "maxDate",
            instance = $(this).data("datepicker"),
            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
        dates.not(this).datepicker("option", option, date);
    }
});