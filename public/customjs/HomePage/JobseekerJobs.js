

function FillJobs(Type) {
    if(Type=='Search'){
    
        jQuery("#JobType").html("Searched Jobs");
  sessionStorage.Type='Search';
    var MasterData = {
        "p_Sector": sessionStorage.Sector,
        "p_Qualification":sessionStorage.Qualification,
        "p_Placeofwork": sessionStorage.Location,
        "p_Designation":sessionStorage.Designation,
        "p_JSRegNO": sessionStorage.RegistrationId,
        
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "SearchJobRecommended";
    // var path = serverpath + "newSearchJob";
    ajaxpost(path, 'parsedatasecuredjobssearch', 'comment', MasterData, 'joblists')
}
}
function parsedatasecuredjobssearch(data, control) {
    data = JSON.parse(data)
        
    var data1 = data[0];
    var cacheArr=[]
var saved=""  ;
  var appenddata = "";
    jQuery("#joblists").empty();
   
    for (var i = 0; i < data1.length; i++) {
        if (cacheArr.indexOf(data[0][i].Postid) >= 0) {
            continue;
        }
        cacheArr.push(data[0][i].Postid);
        if(data1[i].EmployerLogo !=null){
            logosrc=EmployerLogoPath+'EmployerLogo/'+encodeURI(data1[i].EmployerLogo)
        }
        else{
            logosrc='images/jobs/jobimg.jpg'
        }
        if (parseInt(data1[i].Apply) == 1 || parseInt(data1[i].Save) == 1){}
        else{
          saved= "<button  style='background: #fc9928;'class='mt-style-button normal' id='savebutton"+data1[i].Postid+"' onclick='saveJobs("+data1[i].Postid+")'>SAVE</button>"
         appenddata += "<li style='cursor: pointer;' class='i'><div class='row'><div onclick=SetJobId('"+data1[i].Postid+"')  class='col-md-8 col-sm-8'><div class='jobimg' style='height:50px'><img src="+logosrc+" alt='Job Name' style='max-width:83%'></div><div class='jobinfo'><h3><a href='#.'>" + data1[i].Designation + "</a></h3><div class='companyName'><a href="+data1[i].url+" target='_blank'>"+data1[i].CompName+"</a></div><div class='location' style='    padding-left: 70px' ><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-briefcase' aria-hidden='true'></i><span>  " + " (prefered " + data1[i].Desirable + " )</span></div><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-map-marker' aria-hidden='true'></i><span> " + data1[i].Placeofwork + "</span></div><div class='clearfix' style='padding: 25px 0px 0px 0px;'></div><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-money' aria-hidden='true'></i><span>  INR " + data1[i].PayandAllowances + " </span></div></div></div><div class='clearfix'></div> </div><div class='col-md-2 col-sm-4'><div class='listbtn'><button style='    background: #fc9928'  class='mt-style-button normal' id='applybutton"+data1[i].Postid+"' onclick=ApplyJobs("+data1[i].Postid+")>Apply</button></div></div><div class='col-md-2 col-sm-4'><div class='listbtn'>"+saved+"</div></div><div class='col-md-4 col-sm-4' style='margin-top: 2%'><span style='color: #999;'>Posted "+data1[i].Posted+"   ago</span></div><div class='location'></div></div></li>";
                 }
                
          }
    jQuery("#joblists").html(appenddata);

    $('#joblists').clientSidePagination();
}
function checksearchJob(){
    if ($("#Sector").val() == "" && $("#Location").val() == "" &&  $("#Qualification").val() == ""){
        return false;
    }
    else{
        sessionStorage.setItem("Designation",'');
        sessionStorage.setItem("Sector",$("#Sector").val()); 
        sessionStorage.setItem("Qualification",$("#Qualification").val()); 
        sessionStorage.setItem("Location",$("#Location").val());    
        FillJobs('Search')
        }
    }
function SetJobId(Id){
    sessionStorage.setItem("JobId",Id);
    window.location='/JobDescription';
}

function ApplyJobs(JobID)
{
    sessionStorage.JobId=JobID
    var MasterData = {
        "p_Id":'0',
        "p_JobId": JobID,
        "p_JSRegNo":sessionStorage.RegistrationId,
        "p_Save":'0',
        "p_Apply":'1'
        
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/SavedAppliedjobs";
    securedajaxpost(path, 'parsrdataApplyJobs', 'comment', MasterData, 'control')
}
function parsrdataApplyJobs(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        ApplyJobs(sessionStorage.JobId)
    }
    else if (data[0][0].ReturnValue == "1") {
        toastr.success("Apply Successfully", "", "success")
        //$("#applybutton"+sessionStorage.JobId).hide();
        if(sessionStorage.Type=='Search'){
            FillJobs('Search')
          }
          else{
            Fillrecommended()
          }
        $("#applybutton"+sessionStorage.JobId).html("Applied");
        $("#applybutton"+sessionStorage.JobId).prop('disabled', true);
        fetchCounts();
      
        return true;
    }
 }
 function saveJobs(JobID)
{
    sessionStorage.JobId=JobID
  
    var MasterData = {
        "p_Id":'0',
        "p_JobId": JobID,
        "p_JSRegNo":sessionStorage.RegistrationId,
        "p_Save":'1'
        
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/savedjobs";
    securedajaxpost(path, 'parsrdatasaveJobs', 'comment', MasterData, 'control')
}
function parsrdatasaveJobs(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        saveJobs(sessionStorage.JobId)
    }
    else if (data[0][0].ReturnValue == "1") {
        toastr.success("Saved Successfully", "", "success")
      //  $("#savebutton"+sessionStorage.JobId).hide();
      if(sessionStorage.Type=='Search'){
        FillJobs('Search')
    }
        else{
            Fillrecommended()
        }
       
      
      $("#savebutton"+sessionStorage.JobId).html("Saved");
      $("#savebutton"+sessionStorage.JobId).prop('disabled', true);
      fetchCounts();
        return true;
    }
 }

 function fetchCounts(){
    var path = serverpath + "saveapplyCounter/'" + sessionStorage.getItem("RegistrationId") + "'"
    ajaxget(path,'parsedatafetchCounts','comment','control');
}
function parsedatafetchCounts(data,control){
    data = JSON.parse(data)
    $("#appliedcount").html(data[0][0].appliedjob);
    $("#savedcount").html(data[0][0].savedjob);  
}

function fetchappliedjobby(){
    var path = serverpath + "appliedjobbyregno/'" + sessionStorage.getItem("RegistrationId") + "'"
    ajaxget(path,'parsedatafetchappliedjobby','comment','control');
}
function parsedatafetchappliedjobby(data) {
      jQuery("#JobType").html("Applied jobs");
    data = JSON.parse(data)
	
		var data1 = data[0];
		var appenddata="";
        var active="";
        var logosrc=""
        var cacheArr=[]
        jQuery("#joblists").empty();
		for (var i = 0; i < data1.length; i++) {
            if (cacheArr.indexOf(data[0][i].Postid) >= 0) {
                continue;
            }
            cacheArr.push(data[0][i].Postid);
            if(data1[i].EmployerLogo !=null){
                logosrc=EmployerLogoPath+'EmployerLogo/'+encodeURI(data1[i].EmployerLogo)
            }
            else{
                logosrc='images/jobs/jobimg.jpg'
            }
            appenddata += "<li style='cursor: pointer;' class='i'><div class='row'><div onclick=SetJobId('"+data1[i].Postid+"')  class='col-md-8 col-sm-8'><div class='jobimg'  style='height: 50px;'><img src="+logosrc+" alt='Job Name' style='max-width:83%'></div><div class='jobinfo'><h3><a href='#.'>" + data1[i].Designation + "</a></h3><div class='companyName'><a href="+data1[i].url+" target='_blank'>"+data1[i].CompName+"</a></div><div class='location' style='    padding-left: 70px' ><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-briefcase' aria-hidden='true'></i><span>  " + " (prefered " + data1[i].Desirable + " )</span></div><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-map-marker' aria-hidden='true'></i><span> " + data1[i].Placeofwork + "</span></div><div class='clearfix' style='padding: 25px 0px 0px 0px;'></div><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-money' aria-hidden='true'></i><span>  INR " + data1[i].PayandAllowances + " </span></div></div></div><div class='clearfix'></div> </div><div class='col-md-4 col-sm-4' style='margin-top: 2%'><span style='color: #999;'>Posted "+data1[i].Posted+"   Ago</span></div><div class='location'></div></div></li>";
        }
    
    jQuery("#joblists").html(appenddata);  
    $('#joblists').clientSidePagination();
    fetchCounts();
   
	}

    function fetchSavedjobby(){
        var path = serverpath + "savedjobbyregno/'" + sessionStorage.getItem("RegistrationId") + "'"
        ajaxget(path,'parsedatafetchsavedjobby','comment','control');
    }
    function parsedatafetchsavedjobby(data) {
        jQuery("#JobType").html("Saved jobs");
        data = JSON.parse(data)
        var Qualification_essential=""
            var data1 = data[0];
            var appenddata="";
            var active="";
            var logosrc=""
            var cacheArr=[]
            jQuery("#joblists").empty();
            for (var i = 0; i < data1.length; i++) {
                if (cacheArr.indexOf(data[0][i].Postid) >= 0) {
                    continue;
                }
                cacheArr.push(data[0][i].Postid);
                if(data1[i].EmployerLogo !=null){
                    logosrc=EmployerLogoPath+'EmployerLogo/'+encodeURI(data1[i].EmployerLogo)
                }
                else{
                    logosrc='images/jobs/jobimg.jpg'
                }
                if (parseInt(data1[i].Apply) == 1 ){
                  }
          else{
           
                appenddata += "<li style='cursor: pointer;' class='i'><div class='row'><div onclick=SetJobId('"+data1[i].Postid+"')  class='col-md-8 col-sm-8'><div class='jobimg'  style='height: 50px;'><img src="+logosrc+" alt='Job Name' style='max-width:83%'></div><div class='jobinfo'><h3><a href='#.'>" + data1[i].Designation + "</a></h3><div class='companyName'><a href="+data1[i].url+" target='_blank'>"+data1[i].CompName+"</a></div><div class='location' style='    padding-left: 70px' ><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-briefcase' aria-hidden='true'></i><span>  "  + " (prefered " + data1[i].Desirable + " )</span></div><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-map-marker' aria-hidden='true'></i><span> " + data1[i].Placeofwork + "</span></div><div class='clearfix' style='padding: 25px 0px 0px 0px;'></div><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-money' aria-hidden='true'></i><span>  INR " + data1[i].PayandAllowances + " </span></div></div></div><div class='clearfix'></div> </div><div class='col-md-2 col-sm-4'><div class='listbtn'><button style='background: #fc9928;'  class='mt-style-button normal' id='applybutton"+data1[i].Postid+"' onclick=ApplyJobs("+data1[i].Postid+")>Apply</button></div></div><div class='col-md-4 col-sm-4' style='margin-top: 2%'><span style='color: #999;'>Posted "+data1[i].Posted+"   ago</span></div><div class='location'></div></div></li>";
       }}
    
        jQuery("#joblists").html(appenddata);  
        $('#joblists').clientSidePagination();
        fetchCounts();
        }


        function Recommendedjob(){
            if(sessionStorage.Type=='Search'){
                FillJobs('Search')
              }
        }
       
        
        function Fillrecommended() {
          //  if(Type=='Search'){
            
                jQuery("#JobType").html("Recommended Job");
                sessionStorage.Type=='Recommended Job'

              if(!sessionStorage.CandidateSector){var Sector=''}
              else{var Sector = sessionStorage.CandidateSector}

              if(sessionStorage.CandidateQualification=='' && sessionStorage.CandidateQualification=='null'){ var CandidateQualification=''}
              else{var CandidateQualification = sessionStorage.CandidateQualification.split(',')}

              if(!localStorage.CandidateDistrictName){var CandidateDistrictName=''}
              else{ var CandidateDistrictName = sessionStorage.CandidateDistrictName}

              if(!sessionStorage.CandidateDesignation){var Designation=''}
              else{ var Designation = sessionStorage.CandidateDesignation.split(',')}

             

            var MasterData = {
                "p_Sector": Sector,
                "p_Qualification":CandidateQualification,
                "p_Placeofwork": CandidateDistrictName,
                "p_Designation":Designation,
                 "p_JSRegNO": sessionStorage.RegistrationId,
               
            };
       
            MasterData = JSON.stringify(MasterData)
          //  var path = serverpath + "SearchJob";
             var path = serverpath + "SearchJobRecommended";
            ajaxpost(path, 'parsedatasecuredjobs', 'comment', MasterData, 'joblists')
        }
        function parsedatasecuredjobs(data, control) {
            data = JSON.parse(data)
            if(data.errno) {
                return;
            }
            var data1 = data[0];
        var saved=""  ;
          var appenddata = "";
          var cacheArr=[]
            jQuery("#joblists").empty();
           
            for (var i = 0; i < data1.length; i++) {
                var emplogo=""
                if (cacheArr.indexOf(data[0][i].Postid) >= 0) {
                    continue;
                }
                cacheArr.push(data[0][i].Postid);
                if(data1[i].EmployerLogo !=null){

                    logosrc=EmployerLogoPath+'EmployerLogo/'+encodeURI(data1[i].EmployerLogo)
                }
                else{
                    logosrc='images/jobs/jobimg.jpg'
                }
                if (parseInt(data1[i].Apply) == 1 || parseInt(data1[i].Save) == 1){}
                else{
                  saved= "<button  style='background: #fc9928;'class='mt-style-button normal' id='savebutton"+data1[i].Postid+"' onclick='saveJobs("+data1[i].Postid+")'>SAVE</button>"
                 appenddata += "<li style='cursor: pointer;' class='i'><div class='row'><div onclick=SetJobId('"+data1[i].Postid+"')  class='col-md-8 col-sm-8'><div class='jobimg' style='height:50px'><img src="+logosrc+" alt='Job Name' style='max-width:83%'></div><div class='jobinfo'><h3><a href='#.'>" + data1[i].Designation + "</a></h3><div class='companyName'><a href="+data1[i].url+" target='_blank'>"+data1[i].CompName+"</a></div><div class='location' style='    padding-left: 70px' ><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-briefcase' aria-hidden='true'></i><span>  " + " (prefered " + data1[i].Desirable + " )</span></div><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-map-marker' aria-hidden='true'></i><span> " + data1[i].Placeofwork + "</span></div><div class='clearfix' style='padding: 25px 0px 0px 0px;'></div><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-money' aria-hidden='true'></i><span>  INR " + data1[i].PayandAllowances + " </span></div></div></div><div class='clearfix'></div> </div><div class='col-md-2 col-sm-4'><div class='listbtn'><button style='    background: #fc9928'  class='mt-style-button normal' id='applybutton"+data1[i].Postid+"' onclick=ApplyJobs("+data1[i].Postid+")>Apply</button></div></div><div class='col-md-2 col-sm-4'><div class='listbtn'>"+saved+"</div></div><div class='col-md-4 col-sm-4' style='margin-top: 2%'><span style='color: #999;'>Posted "+data1[i].Posted+"   ago</span></div><div class='location'></div></div></li>";
                         }
                  }
            jQuery("#joblists").html(appenddata);
            $('#joblists').clientSidePagination();
         }