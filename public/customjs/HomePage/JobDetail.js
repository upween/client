function FillJobDetail(funct) {
    var path = serverpath + "Jobs/0/'Location'"
    securedajaxget(path, funct, 'comment', 'control');

}
function parsedatasecuredjobs(data, control) {
    data = JSON.parse(data)

    var data1 = data[0];
    jQuery("#Jobdescription").empty();
    jQuery("#skill").empty();
    jQuery("#Jobdetail").empty();
    jQuery("#Companydetail").empty();
     jQuery("#relatedjobs").empty();
     var appenddata="";
     var cacheArr = [];
    for (var i = 0; i < data1.length; i++) {
     var Id=""+data1[i].JobId+"";
     if (cacheArr.indexOf(data[0][i].JobId) >= 0) {
        continue;
    }
    cacheArr.push(data[0][i].JobId);
        if (Id == sessionStorage.getItem("JobId")) {
            var SkillName = data1[i].Keywords.split(",");
       
            var jobdetail = "<div class='col-md-8'><h2>" + data1[i].JobTitle + "</h2><div class='ptext'>Date Posted: "+data1[i].UpdateOn+"</div> <div class='salary'>Monthly Salary: <strong>INR " + data1[i].AnnualCTCMin + " - " + data1[i].AnnualCTCMax + " CTC</strong></div></div><div class='col-md-4'><div class='companyinfo'><div class='companylogo'><img src='images/employers/emplogo1.jpg' style='max-width: 100%' alt='your alt text'></div> <div class='title' style='    font-size: 18px; font-weight: 600;color: #263bd6;'><a href=" + data1[i].Website + ">" + data1[i].Name + "</a></div><div class='ptext' style='    margin: 6px'>" + data1[i].Address + "</div><div class='opening'><a href='#'>" + data1[i].NoOfVacancy + " Current Jobs Openings</a></div><div class='clearfix'></div> </div></div>";
            var jobdescription = "<p> " + data1[i].JobDescription + " </p>"
            var skill="";
            for (var j = 0; j < SkillName.length; j++) {
                skill += "<div class='chip'>" + SkillName[j] + "</div>";
            }
            var companydetails = "<p>" + data1[i].Name + "</p><p>" + data1[i].Address + "</p><p>" + data1[i].Website + "</p><p>" + data1[i].ContactNumber + "</p>";
            var JbDetails = "<li class='row'><div class='col-md-6 col-xs-6'>Job Id</div><div class='col-md-6 col-xs-6'><span>"+data1[i].JobId+"</span></div></li><li class='row'><div class='col-md-6 col-xs-6'>Location</div><div class='col-md-6 col-xs-6'><span>"+data1[i].Location+"</span></div></li><li class='row'><div class='col-md-6 col-xs-6'>Company</div><div class='col-md-6 col-xs-6'><a href='"+data1[i].Website+"'>"+data1[i].Name+"</a></div></li><li class='row'><div class='col-md-6 col-xs-6'>Employment Detail</div> <div class='col-md-6 col-xs-6'><span>"+data1[i].EmploymentDetails+"</span></div></li><li class='row'><div class='col-md-6 col-xs-6'>Vacancy</div><div class='col-md-6 col-xs-6'><span>"+data1[i].NoOfVacancy+"</span></div></li><li class='row'><div class='col-md-6 col-xs-6'>Experience</div><div class='col-md-6 col-xs-6'><span>"+data1[i].WorkExpMin+" Years - "+data1[i].WorkExpMax+" Years</span></div> </li>";
            jQuery("#Jobdescription").html(jobdescription);
            jQuery("#skill").html(skill);
            jQuery("#Jobdetail").html(jobdetail);
            jQuery("#Companydetail").html(companydetails);
            jQuery("#jbdetail").html(JbDetails);
            jQuery("#contact").html("Contact "+data1[i].Name+"");
        }
        else {

             appenddata += "<li><div class='row'><div onclick=SetJobId('" + data1[i].JobId + "')  class='col-md-8 col-sm-8'><div class='jobimg'><img src='images/jobs/jobimg.jpg' alt='Job Name'></div><div class='jobinfo'><h3><a href='#.'>" + data1[i].JobTitle + "</a></h3><div class='companyName'><a href='" + data1[i].Website + "' target='_blank'>" + data1[i].Name + "</a></div><div class='location' style='    padding-left: 70px' ><div class='col-md-4' style='margin-top: 3px;'><i class='fa fa-briefcase' aria-hidden='true'></i><span>  " + data1[i].WorkExpMin + " yrs - " + data1[i].WorkExpMax + " yrs</span></div><div class='col-md-4' style='margin-top: 3px;'><i class='fa fa-map-marker' aria-hidden='true'></i><span> " + data1[i].Location + "</span></div></div></div><div class='clearfix'></div> </div><div class='col-md-4 col-sm-4'><div class='listbtn'><a href='#.'>Apply Now</a></div></div><div class='col-md-4 col-sm-4' style='margin-top: 2%'><span style='margin-left:-2%;color: #999;'>Posted By- " + data1[i].ContactPerson + "</span></div><i class='fa fa-pencil' style='padding-left:100px;padding-right:5px;padding-top:5px'></i><span>" + data1[i].Keywords + "</span><div class='location'><div class='col-md-4' style='margin-top: 3px;'><i class='fa fa-money' style=' padding-left: 81px;padding-top:5px'></i><span style='padding-left:5px'>INR " + data1[i].AnnualCTCMin + " - " + data1[i].AnnualCTCMax + " CTC</span></div></div></div></li>";
            jQuery("#relatedjobs").html(appenddata);
        }

    }

}

 function CheckCompanyMails() {
    if (jQuery('#urname').val() == "") {
        toastr.warning("Please Enter Your Name", "", "info");
    }
        else if (jQuery('#uremail').val() == "") {
        toastr.warning("Please Enter Your E-Mail", "", "info");
        }
        else  if (jQuery('#urphone').val() == "") {
        toastr.warning("Please Enter Your Phone No", "", "info");
        }
        else  if (jQuery('#ursubject').val() == "") {
        toastr.warning("Please Enter Subject", "", "info");
        }
        else if (jQuery('#message').val() == "") {
        toastr.warning("Please Enter Message", "", "info");
        }
     
      else  {
        InsCompanyEmail();
           }
    

 };

function InsCompanyEmail()  {
    var Mail;
    Mail = jQuery('#uremail').val();

    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if (reg.test(Mail) == false) {
        $(window).scrollTop(0);
        toastr.warning("Please Enter Correct E-MailId", "", "info");
        jQuery('#uremail').css('border-color', 'red');
        return true;
    }
    else {
        jQuery('#uremail').css('border-color', '');

    }

    var MasterData = {
        "p_CompanyId": '0',
        "p_CandidateId": sessionStorage.getItem("CandidateId"),
        "p_Name": jQuery("#urname").val(),
        "p_EmailId": jQuery("#uremail").val(),
        "p_PhoneNo": jQuery("#urphone").val(),
        "p_Subject": jQuery("#ursubject").val(),
        "p_Message":jQuery("#message").val(),
        "p_IpAddress":sessionStorage.getItem("IpAddress"),
        "p_UserId": sessionStorage.getItem("CandidateId"),
    }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/CompanyEmails";
    securedajaxpost(path,'parsrdataInsCompanyEmail','comment',MasterData,'control')
}

function parsrdataInsCompanyEmail(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        InsCompanyEmail();
        
    }
    
    if (data[0][0].ReturnValue == "1") {
        mail();
        resetCompanyEmail();
          toastr.success("Submit Successful", "", "success")
          return true;
      }
      
    
     }

     function resetCompanyEmail() {
        jQuery("#urname").val("")
        jQuery("#uremail").val("")
        jQuery("#urphone").val("")
        jQuery("#ursubject").val("")
        jQuery("#message").val("")
         }

  function mail() {

                     
		    var sub = "Enquiry from MP Rojgar";
        var body = "<table width='600' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td align='left' valign='top' style='border-right: solid thick #f0f0f0; border-left: solid thick #f0f0f0; border-top: solid thick #f0f0f0;'><img src='' class='yatmlogo' style='height: 70px; float: left;'> <img src='' class='mplogo' style='float: right; margin-top: -14.5%;'></td> </tr>" +
        "<tr><td align='center' valign='top' style=' border-right: solid thick #f0f0f0; border-left: solid thick #f0f0f0; background-color:#f0f0f0; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000; padding:10px;'><table width='100%' border='0' cellspacing='0' cellpadding='0' style='margin-top:10px;'><tr>" +
        "<td align='left' valign='top' style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#525252;'><div style='font-family:Georgia, Times New Roman, Times, serif; font-size:56px; color:#000000;'>Msg from MP Rojgar <span style='color:#478730;'>*</span></div>" +
        "<div style='font-size:24px;'><br> Name : " + $("#urname").val() + " </div>" +
		"<div style='font-size:24px;'><br> E-mail : " + $("#uremail").val() + " </div>" +
		"<div style='font-size:24px;'><br> Phone No. : " + $("#urphone").val() + " </div>" +
		"<div style='font-size:24px;'><br> Subject : " + $("#ursubject").val() + " </div>" +
		"<div style='font-size:24px;'><br> Message : " + $("#message").val() + " </div>" +
        "<div> <br>Note: This is autogenerated email please do not reply.<br></div></td></tr></table></td></tr>" +
        "<tr><td align='left' valign='top' style='background-color:#3498DB; border-right: solid thick #f0f0f0; border-left: solid thick #f0f0f0; border-bottom: solid thick #f0f0f0;'><table width='100%' border='0' cellspacing='0' cellpadding='15'><tr>" +
        "<td align='left' valign='top' style='color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:13px;'>Company Address: <br>St Joseph's Rd, Idgah Hills, Bhopal, Madhya Pradesh 462001" +
        "<br />MP Rojgar <br>Phone: 0755-2767927<br>Email: <a href='helpdesk[dot]mprojgar[at]mp[dot]gov[dot]in' style='color:#ffffff; text-decoration:none;'>helpdesk[dot]mprojgar[at]mp[dot]gov[dot]in </a><br>Website: <a href='http://mprojgar.gov.in/' target='_blank' style='color:#ffffff; text-decoration:none;'>http://mprojgar.gov.in/</a></td></tr></table></td></tr></table>";
     Email.send(globalemailid,
     sessionStorage.getItem('EmailId'),
sub,
body,
"smtp.gmail.com",
globalemailid,
globalemailpass);
      
}