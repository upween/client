$('#grievenceupload').submit(function () {
    $(this).ajaxSubmit({
        error: function (xhr) {
            toastr.warning(xhr.status, "", "info")
        },
        success: function (response) {
            if (response == "Error No File Selected"){
                sessionStorage.setItem("Grievencedoc", '')
                InsUpdGrievenceForm();
            }
             if (response == "Request Entity Too Large") {
                toastr.warning(response, "", "info")
            }
            else if (response == "Error: PDF and Image File Only!") {
                toastr.warning(response, "", "info")
            }
            else {
                var str = response;
                var res = str.split("!");
                sessionStorage.setItem("Grievencedoc", res[1])
                toastr.success(res[0], "", "success")
                InsUpdGrievenceForm();
            }
        }
    });
    return false;
});
function FillDistrictOffice() {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "districtoffice",
        cache: false,
        dataType: "json",
        success: function (data) {
            var cacheArr = [];
            jQuery("#DistrictOffice").empty();

            jQuery("#DistrictOffice").append(jQuery("<option ></option>").val("0").html("Select Office"));
            for (var i = 0; i < data[0].length; i++) {
                if (cacheArr.indexOf(data[0][i].DivisionCallCentre) >= 0) {
                    continue;
                }
                cacheArr.push(data[0][i].DivisionCallCentre);
                jQuery("#DistrictOffice").append(jQuery("<option></option>").val(data[0][i].DistrictSystemId).html(data[0][i].DivisionCallCentre));
            }
        },
        error: function (xhr) {
            //swal(xhr.d, "", "error")
        }
    });
}

function CheckGrievenceForm() {

    if (isValidate == true) {


        if (jQuery('#Registeredby').val() == '0') {
            getvalidated('Registeredby', 'select', 'Registered By');
            return false;
        }
        if ($.trim(jQuery('#RegistrationId').val()) == '') {
            getvalidated('RegistrationId', 'text', 'Registration ID')
            return false;
        }
        if (jQuery('#CandidateName').val() == '') {
            getvalidated('CandidateName', 'text', 'Your Name')
            return false;
        }
        if (jQuery('#EmailID').val() == '') {
            getvalidated('EmailID', 'email', 'Email ID')
            return false;
        }
        if (jQuery('#Contact').val() == '') {
            getvalidated('Contact', 'number', 'Mobile Number')
            return false;
        }
        if (jQuery('#DistrictOffice').val() == '0') {
            getvalidated('DistrictOffice', 'select', 'District Office')
            return false;
        }
        if (jQuery('#department').val() == '0') {
            getvalidated('department', 'select', 'Department')
            return false;
        }
        if (jQuery('#grievencetype').val() == '0') {
            getvalidated('grievencetype', 'select', 'Type of Grievance')
            return false;
        }
        if (jQuery('#grievencedetail').val() == '') {
            getvalidated('grievencedetail', 'text', 'Detail')
            return false;
        }


        if ($("#Declaration").is(":checked")) {
            if (isCaptchaValidated == true) {
                validateCaptchaGrievence();
            }
            else {
                $('#postattach').click()
            }

        }

        else {
            $("#validDeclaration").html("Please  accept declaration");
            return false;
        }
    }
    else {
        $('#postattach').click();
    }

};



function InsUpdGrievenceForm() {

    var MasterData = {

        "p_GrievenceId": '0',
        "p_RegisteredBy": jQuery("#Registeredby option:selected").val(),
        "p_RegistrationId": $.trim(jQuery("#RegistrationId").val()),
        "p_Name": jQuery("#CandidateName").val(),
        "p_EmailId": jQuery("#EmailID").val(),
        "p_ContactNumber": jQuery("#Contact").val(),
        "p_DistrictOfficeId": jQuery("#DistrictOffice option:selected").val(),
        "p_Department": jQuery("#department option:selected").val(),
        "p_TypeOfGrievence": jQuery("#grievencetype option:selected").val(),
        "p_GrievenceDetail": jQuery("#grievencedetail").val(),
        "p_DocumentAttached": sessionStorage.getItem("Grievencedoc"),
        "p_UserId": "1",
        "p_IpAddress": sessionStorage.getItem("IpAddress")
    }
    jQuery.ajax({
        url: serverpath + "grievenceform",
        type: "POST",
        data: JSON.stringify(MasterData),
        contentType: "application/json; charset=utf-8",
        success: function (data, status, jqXHR) {

            resetModeGrievence();
            if (data[0][0].ReturnValue == "1") {

                sessionStorage.setItem("Name", data[0][0].Name);
                sessionStorage.setItem("GrievenceNumber", data[0][0].GrievenceNumber);
                sessionStorage.setItem("GrievenceId", data[0][0].GrievenceId);
              
                Cookies.set('GrievenceId', data[0][0].GrievenceId, { expires: 1, path: '/' });
                //$("#postattach").click();
                 window.location = '/SuccessGrievance'


            }

        },
        error: function (xhr) {
        }
    });
}
function resetModeGrievence() {
    createCaptchaGrievence();
    jQuery("#Registeredby").val("0");
    jQuery("#RegistrationId").val("");
    jQuery("#CandidateName").val("");
    jQuery("#EmailID").val("");
    jQuery("#Contact").val("");
    jQuery("#DistrictOffice").val("0");
    jQuery("#department").val("0");
    jQuery("#grievencetype").val("0");
    jQuery("#grievencedetail").val("");
    jQuery("#cpatchatextgrievence").val("");
    jQuery("#Declaration").prop("checked", false);
    $("#validDeclaration").html("");
}

function focusoutDeclaration() {
    if ($("#Declaration").is(":checked")) {
        $("#validDeclaration").html("");
        return false;
    }
}
var code;
function createCaptchaGrievence() {
    //clear the contents of captcha div first 
    document.getElementById('captchagrievence').innerHTML = "";
    var charsArray =
        "0123456789";
    var lengthOtp = 6;
    var captcha = [];
    for (var i = 0; i < lengthOtp; i++) {
        //below code will not allow Repetition of Characters
        var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
        if (captcha.indexOf(charsArray[index]) == -1)
            captcha.push(charsArray[index]);
        else i--;
    }
    var canv = document.createElement("canvas");
    canv.id = "captchagrievence";
    canv.width = 100;
    canv.height = 50;
    var ctx = canv.getContext("2d");
    ctx.font = "25px Georgia";
    ctx.strokeText(captcha.join(""), 0, 30);
    //storing captcha so that can validate you can save it somewhere else according to your specific requirements
    code = captcha.join("");
    document.getElementById("captchagrievence").appendChild(canv); // adds the canvas to the body element
}
function validateCaptchaGrievence() {
    event.preventDefault();

    if (document.getElementById("cpatchatextgrievence").value == code) {
        $('#postattach').click();
    } else {

        jQuery("#cpatchatextgrievence").val("");
        $("#cpatchatextgrievence").css('border-color', 'red');
        $("#validcpatchatextgrievence").html("Please enter Valid Captcha");
        createCaptchaGrievence();
    }
}
