var companyall=[];
var cityall=[];  
var designationall=[];
var datafilter=[];
var globalfilter=[];
function FillJobList(funct) {
   var MasterData = {
    "p_Sector":sessionStorage.Sector,
    "p_Qualification":sessionStorage.Qualification,
    "p_Placeofwork": sessionStorage.Location,
    "p_Designation":sessionStorage.Designation
    
}
MasterData = JSON.stringify(MasterData)
var path = serverpath + "SearchJob";



ajaxpost(path, funct, 'comment', MasterData, 'joblists');
}
function parsedatajobs(data) {
    data = JSON.parse(data)
    if(data.errno) {
        return;
    }
    var data1 = data[0];
    datafilter = data[0];
    var appenddata = "";
    var logosrc=""
    var companyfour =[];
    var cityfour =[];
    var designationfour=[];
    var emptyArray=[];
    // if (typeof emptyArray != "undefined"  
    // && emptyArray != null  
    // && emptyArray.length != null  
    // && emptyArray.length > 0){
    //var cacheArr = [];
    jQuery("#joblists").empty();
    for (var i = 0; i < data1.length; i++) {
       
        //appenddata += "<li style='cursor: pointer;'><div class='row'><div onclick=SetJobId('"+data1[i].Postid+"')  class='col-md-8 col-sm-8'><div class='jobimg'><img src='images/jobs/jobimg.jpg' alt='Job Name'></div><div class='jobinfo'><h3><a href='#.'>" + data1[i].Designation + "</a></h3><div class='companyName'><a href="+data1[i].url+" target='_blank'>"+data1[i].CompName+"</a></div><div class='location' style='    padding-left: 70px' ><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-briefcase' aria-hidden='true'></i><span>  " + data1[i].Qualification_essential + " (prefered " + data1[i].Desirable + " )</span></div><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-map-marker' aria-hidden='true'></i><span> " + data1[i].Placeofwork + "</span></div><div class='clearfix' style='padding: 25px 0px 0px 0px;'></div><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-money' aria-hidden='true'></i><span>  INR " + data1[i].PayandAllowances + " </span></div></div></div><div class='clearfix'></div> </div><div class='col-md-4 col-sm-4'><div class='listbtn'><a href='/Login' onclick=sessionStorage.Type='Search'>Apply Now</a></div></div><div class='col-md-4 col-sm-4' style='margin-top: 2%'><span style='color: #999;'>Posted "+data1[i].Posted+"  ago</span></div><div class='location'></div></div></li>";
        if(data1[i].EmployerLogo !=null){
            logosrc=EmployerLogoPath+'/EmployerLogo/'+encodeURI(data1[i].EmployerLogo) 
        }
        else{
            logosrc='images/jobs/jobimg.jpg'
        }
        if (i<7){
            companyfour.push(encodeURI(data[0][i].CompName))
            cityfour.push(encodeURI(data[0][i].Placeofwork))
            designationfour.push(encodeURI(data[0][i].Designation))
            $("#companyview").hide();$("#designationview").hide();$("#cityview").hide();
        }
        else{
            $("#companyview").show();$("#designationview").show();$("#cityview").show();
        }
        companyall.push(encodeURI(data[0][i].CompName))
        cityall.push(encodeURI(data[0][i].Placeofwork))
        designationall.push(encodeURI(data[0][i].Designation))
        
        var Qualification_essential=""
        designationfour = designationfour.filter(onlyUnique);
        companyfour = companyfour.filter(onlyUnique);
        cityfour = cityfour.filter(onlyUnique);
        cityall = cityall.filter(onlyUnique);
        companyall = companyall.filter(onlyUnique);
        designationall = designationall.filter(onlyUnique);
        if(data[0][i].Qualification_essential==null){
            Qualification_essential=""
        }
        else{
            Qualification_essential=data[0][i].Qualification_essential;
        }
        if( data1[i].Desirable !=""){
            var prefered=`(prefered ${data1[i].Desirable} ) `
        }
            else{
                var prefered=''
            }
        
        
            
        appenddata += " <li class='i'><div class='row'><div class='col-md-8 col-sm-8'><div class='jobimg'  style='height: 50px;'><img src='"+logosrc+"' alt='"+data1[i].CompName+"' style='max-width:83%'/></div><div class='jobinfo'><h3><a href='#.'onclick=SetJobId('" + data1[i].Postid + "')><div style=' overflow: hidden;text-overflow: ellipsis;white-space: nowrap;' >" + data1[i].Designation + "</div></a></h3><div class='companyName'><a href="+data1[i].url+" target='blank'>"+data1[i].CompName+"</a></div><div class='location'><label class='fulltime'>" + data1[i].VacancyType + "</label>   - <span> <i class='fa fa-map-marker' aria-hidden='true'></i>  "+data1[i].Placeofwork+"  ("+data1[i].TotalVacancy+" Openings)</span></div></div><div class='clearfix'></div></div><div class='col-md-4 col-sm-4'><div class='listbtn'><a  style='cursor:pointer;' onclick=applyonJob("+data1[i].Postid+")>Apply Now</a></div></div><div class='col-md-4 col-sm-4' style='margin-top: 2%'><span style='color: #999;'>Posted "+data1[i].Posted+" Ago</span></div></div><p><i class='fa fa-briefcase' aria-hidden='true'></i>  " + Qualification_essential + prefered+" </p><p> <i class='fa fa-money' aria-hidden='true'></i><span>  INR " + data1[i].PayandAllowances + " </span></p></li>"
        
    }     
    // else{
    //     appenddata += " <li ><div class='row' style='height: 138px;'><div></div></br><div></div></br><div></div></br><div></div></br><div class='col-md-12 col-sm-8' style='font-size: large;text-align: center;color: grey; font-weight: bold;'>No Jobs Found</div></li>"
       
    // }
    
    jQuery("#joblists").html(appenddata);
    $('#joblists').clientSidePagination();
    fillfilter(companyfour,cityfour,designationfour);
}
function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}
function fillmodal(type){
    $("#filterbody").empty();
    var appendata4 = "";
    var rowLength = 2
    if (type == "company"){
        $("#headername").html('Company name')
        for (let i = 0; i < companyall.length; i+=rowLength) {
            const tds = companyall.slice(i, i + rowLength).map(value => `<td style=' border-top: 0px'><input style='margin-right: 5px;' type='checkbox' name='DesignationFilter'/><label></label>${decodeURI(value)}</td>`)
            appendata4 += `<tr>${tds.join("")}</tr>`
        } 
    }
    if (type == "designation"){
        $("#headername").html('Designations')

        for (let i = 0; i < designationall.length; i+=rowLength) {
            const tds = designationall.slice(i, i + rowLength).map(value => `<td style=' border-top: 0px'><input style='margin-right: 5px;' type='checkbox' name='DesignationFilter'/><label></label>${decodeURI(value)}</td>`)
            appendata4 += `<tr>${tds.join("")}</tr>`
        }  
    }
    if (type == "city"){
        $("#headername").html('Cities')

        for (let i = 0; i < cityall.length; i+=rowLength) {
            const tds = cityall.slice(i, i + rowLength).map(value => `<td style=' border-top: 0px'><input style='margin-right: 5px;' type='checkbox' name='DesignationFilter'/><label></label>${decodeURI(value)}</td>`)
            appendata4 += `<tr>${tds.join("")}</tr>`
        } 
    }
    $("#filterbody").html(appendata4);
}
function fillfilter(companyfour,cityfour,designationfour){
    if(sessionStorage.Designation==''){
        $("#CityWidget").hide();
        $("#DesignationWidget").show()
    }
    if(sessionStorage.Location==''){
        $("#CityWidget").show();
        $("#DesignationWidget").hide()
    }
    if(sessionStorage.Location!=''&&sessionStorage.Designation!=''){
        $("#CityWidget").hide();
        $("#DesignationWidget").hide()
    }
    var appendata1 = "";
    var appendata2 = "";
    var appendata3 = "";
    $.each(companyfour, function( index, value ) {
        if(value!=null ||value!=''|| value!='null'){
            appendata1+="<li style='word-break: break-word;'><input type='checkbox' name='CompanyFilter' id='Company"+[index]+"' value='"+value+"'/><label for='Company"+[index]+"'></label>"+decodeURI(value)+"<span></span></li>";
        }
    });  
    $.each(designationfour, function( index1, value1 ) {
        if(value1!=null ||value1!=''|| value1!='null'){
            appendata2+="<li style='word-break: break-word;'><input type='checkbox' name='DesignationFilter' value='"+value1+"' id='Designation"+[index1]+"'/><label for='Designation"+[index1]+"'></label>"+decodeURI(value1)+"<span></span></li>";
        }
    });  
    $.each(cityfour, function( index2, value2 ) {
        if(value2!=null ||value2!=''|| value2!='null'){
            appendata3+="<li style='word-break: break-word;'><input type='checkbox' name='LocationFilter' value='"+value2+"' id='Location"+[index2]+"' /><label for='Location"+[index2]+"'></label>"+decodeURI(value2)+"<span></span></li>";
        }
    }); 
    $("#Companyfilter").html(appendata1);
    $("#designationlist").html(appendata2);
    $("#cityfilter").html(appendata3);
}
function SetJobId(Id){
    sessionStorage.setItem("JobId",Id);
    //window.location='/JobDetail';
    window.location='/JobDescription';
}
function filteroption(){
    var comval = [];
    var cityval = [];
    var desigval = [];
    var combinefilter=[];
    globalfilter = datafilter;
    $('[name="CompanyFilter"]:checked').each(function(i){
        comval[i] = decodeURI($(this).val());
    });
    $('[name="DesignationFilter"]:checked').each(function(i){
        desigval[i] = decodeURI($(this).val());
    });
    $('[name="LocationFilter"]:checked').each(function(i){
        cityval[i] = decodeURI($(this).val());
    });
    
    if (comval && comval.length > 0) {
        for (var i = 0; i < comval.length; i++) {
            var filtered = datafilter.filter(function(element) {
                var cats = element.CompName.split(',');
                
                return cats.filter(function(cat) {
                    return comval.indexOf(cat) > -1;
                }).length === comval.length;
            });
            //combinefilter.push(filtered);
        }
        
        globalfilter = filtered;
        //remapobject(filtered);
    }
    if (desigval && desigval.length > 0) {
        if (globalfilter && globalfilter.length>0){
            var filtered = globalfilter.filter(function(element) {
                var cats = element.Designation.split(',');
                
                return cats.filter(function(cat) {
                    return desigval.indexOf(cat) > -1;
                }).length === desigval.length;
            });
        }
        else{
            var filtered = datafilter.filter(function(element) {
                var cats = element.Designation.split(',');
                
                return cats.filter(function(cat) {
                    return desigval.indexOf(cat) > -1;
                }).length === desigval.length;
            });
        }
        globalfilter = filtered;
        //remapobject(filtered);
    }
    if (cityval && cityval.length > 0) {
        if (globalfilter && globalfilter.length>0){
            var filtered = globalfilter.filter(function(element) {
                var cats = element.Placeofwork.split(',');
                
                return cats.filter(function(cat) {
                    return cityval.indexOf(cat) > -1;
                }).length === cityval.length;
            });
        }
        else{
            var filtered = datafilter.filter(function(element) {
                var cats = element.Placeofwork.split(',');
                
                return cats.filter(function(cat) {
                    return cityval.indexOf(cat) > -1;
                }).length === cityval.length;
            });
        }
        globalfilter = filtered;
        //remapobject(filtered);
    }
    //if (globalfilter && globalfilter.length>0){
        remapobject(globalfilter);
    //}
    //else{
    //    remapobject(datafilter);
    //}
}
function remapobject(filtered){
    var appenddata = "";
    jQuery("#joblists").empty();
    var Qualification_essential=""
    for (var i = 0; i < filtered.length; i++) {
        if(filtered[i].EmployerLogo !=null){
            logosrc=EmployerLogoPath+'/EmployerLogo/'+encodeURI(filtered[i].EmployerLogo)
        }
        else{
            logosrc='images/jobs/jobimg.jpg'
        }
        if(filtered[i].Qualification_essential==null){
            Qualification_essential="";
        }
        else{
            Qualification_essential=filtered[i].Qualification_essential;
        }
        if(filtered[i].Desirable!='' ){
            var prefered=`(prefered ${filtered[i].Desirable} ) `
        }
            else{
                var prefered=''
            }
        

           
        appenddata += " <li class='i'><div class='row'><div class='col-md-8 col-sm-8'><div class='jobimg'  style='height: 50px;'><img style='max-width: 83%;' src='"+logosrc+"' alt='"+filtered[i].CompName+"' /></div><div class='jobinfo'><h3><a href='#.'onclick=SetJobId('" + filtered[i].Postid + "')><div style=' overflow: hidden;text-overflow: ellipsis;white-space: nowrap;' >" + filtered[i].Designation + "</div></a></h3><div class='companyName'><a href="+filtered[i].url+" target='blank'>"+filtered[i].CompName+"</a></div><div class='location'><label class='fulltime'>Full Time</label>   - <span> <i class='fa fa-map-marker' aria-hidden='true'></i>  "+filtered[i].Placeofwork+"  ("+filtered[i].TotalVacancy+" Openings)</span></div></div><div class='clearfix'></div></div><div class='col-md-4 col-sm-4'><div class='listbtn'><a style='cursor:pointer;' onclick=applyonJob("+filtered[i].Postid+")>Apply Now</a></div></div><div class='col-md-4 col-sm-4' style='margin-top: 2%'><span style='color: #999;'>Posted "+filtered[i].Posted+"  Ago</span></div></div><p><i class='fa fa-briefcase' aria-hidden='true'></i>  " + Qualification_essential + prefered+"</p><p> <i class='fa fa-money' aria-hidden='true'></i><span>  INR " + filtered[i].PayandAllowances + " </span></p></li>"
        
    }            
    jQuery("#joblists").html(appenddata);
    $('#joblists').clientSidePagination();
}
function applyonJob(PostId){
    if(sessionStorage.CandidateId){
        ApplyJobs(PostId);
    }
    else{
        sessionStorage.Type='Search';
        setTimeout(function(){
            window.location='/Login';
        },500);
       
    }
}
function ApplyJobs(JobID)
{
    sessionStorage.JobId=JobID
    var MasterData = {
        "p_Id":'0',
        "p_JobId": JobID,
        "p_JSRegNo":sessionStorage.RegistrationId,
        "p_Save":'0',
        "p_Apply":'1'
        
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/SavedAppliedjobs";
    securedajaxpost(path, 'parsrdataApplyJobs', 'comment', MasterData, 'control')
}
function parsrdataApplyJobs(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        ApplyJobs(sessionStorage.JobId)
    }
    else if (data[0][0].ReturnValue == "1") {
        toastr.success("Apply Successfully", "", "success")
    
      
        return true;
    }
 }