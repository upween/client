function SearchJOBS() {
    var path = serverpath+"secured/JobSearch/"+sessionStorage.getItem("Designation")+"/"+sessionStorage.getItem("Qualification")+"/"+sessionStorage.getItem("District")+"/"+sessionStorage.getItem("TypeOfJob")+"";
   securedajaxget(path, 'parsedataSearchJOBS', 'comment', 'joblists')

}
function parsedataSearchJOBS(data) {
   data = JSON.parse(data)
   if (data.message == "New token generated"){
       sessionStorage.setItem("token", data.data.token);
       SearchJOBS('parsedataSearchJOBS','joblists');
   }
   else if (data.status == 401){
       toastr.warning("Unauthorized", "", "info")
       return true;
   }
   else{
    var data1 = data[0];
    var saved=""  ;
      var appenddata = "";
        jQuery("#joblists").empty();
   for (var i = 0; i < data[0].length; i++) {

    if(data1[i].EmployerLogo !=null){
        logosrc=EmployerLogoPath+'/EmployerLogo/'+encodeURI(data1[i].EmployerLogo) 
    }
    else{
        logosrc='images/jobs/jobimg.jpg'
    }

    
      
            if (data1[i].Apply == "1" ){
                appenddata += "<li style='cursor: pointer;'><div class='row'><div onclick=SetJobId('"+data1[i].Postid+"')  class='col-md-8 col-sm-8'><div class='jobimg'><img src='"+logosrc+"' alt='Job Name' style='max-width:83%'></div><div class='jobinfo'><h3><a href='#.'>" + data1[i].Designation + "</a></h3><div class='companyName'><a href="+data1[i].url+" target='_blank'>"+data1[i].CompName+"</a></div><div class='location' style='    padding-left: 70px' ><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-briefcase' aria-hidden='true'></i><span>  " + data1[i].Qualification_essential + " (prefered " + data1[i].Desirable + " )</span></div><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-map-marker' aria-hidden='true'></i><span> " + data1[i].Placeofwork + "</span></div><div class='clearfix' style='padding: 25px 0px 0px 0px;'></div><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-money' aria-hidden='true'></i><span>  INR " + data1[i].PayandAllowances + " </span></div></div></div><div class='clearfix'></div> </div><div class='col-md-2 col-sm-4'><div class='listbtn' ><button disabled class='mt-style-button normal'  id='applybutton"+data1[i].Postid+"'>Applied</button></div></div><div class='col-md-2 col-sm-4'><div class='listbtn'></div></div><div class='col-md-4 col-sm-4' style='margin-top: 2%'><span style='color: #999;'>Posted "+data1[i].Posted+"  Ago</span></div><div class='location'></div></div></li>";
            }
    
            else{
                if (data1[i].Save == "1" ){
                  saved=" <button  disabled class='mt-style-button normal' id='savebutton"+data1[i].Postid+"'>Saved</button>"
               
                }
                else
        {
            saved= "<button  style='background: blue;'  class='mt-style-button normal' id='savebutton"+data1[i].Postid+"' onclick='saveJobs("+data1[i].Postid+")'>Save</button>"
               
        }
                appenddata += "<li style='cursor: pointer;' class='i'><div class='row'><div onclick=SetJobId('"+data1[i].Postid+"')  class='col-md-8 col-sm-8'><div class='jobimg'  style='height: 50px;'><img src='images/jobs/jobimg.jpg' alt='Job Name' style='max-width:83%'></div><div class='jobinfo'><h3><a href='#.'>" + data1[i].Designation + "</a></h3><div class='companyName'><a href="+data1[i].url+" target='_blank'>"+data1[i].CompName+"</a></div><div class='location' style='    padding-left: 70px' ><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-briefcase' aria-hidden='true'></i><span>  " + data1[i].Qualification_essential + " (prefered " + data1[i].Desirable + " )</span></div><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-map-marker' aria-hidden='true'></i><span> " + data1[i].Placeofwork + "</span></div><div class='clearfix' style='padding: 25px 0px 0px 0px;'></div><div class='col-md-8' style='margin-top: 3px;'><i class='fa fa-money' aria-hidden='true'></i><span>  INR " + data1[i].PayandAllowances + " </span></div></div></div><div class='clearfix'></div> </div><div class='col-md-2 col-sm-4'><div class='listbtn'><button style='background: blue;'  class='mt-style-button normal' id='applybutton"+data1[i].Postid+"' onclick=ApplyJobs("+data1[i].Postid+")>Apply</button></div></div><div class='col-md-2 col-sm-4'><div class='listbtn'>"+saved+"</div></div><div class='col-md-4 col-sm-4' style='margin-top: 2%'><span style='color: #999;'>Posted "+data1[i].Posted+"  Ago</span></div><div class='location'></div></div></li>";
            }
            
       
        }
   jQuery("#joblists").html(appenddata);
   if(data.length>0){
    $('#joblists').clientSidePagination();
   }
  
   }
}

function SetJobId(Id){
    sessionStorage.setItem("JobId",Id);
    window.location='/JobDescription';
}


function ApplyJobs(JobID)
{
    sessionStorage.JobId=JobID
    var MasterData = {
        "p_Id":'0',
        "p_JobId": JobID,
        "p_JSRegNo":sessionStorage.RegistrationId,
        "p_Save":'0',
        "p_Apply":'1'
        
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/SavedAppliedjobs";
    securedajaxpost(path, 'parsrdataApplyJobs', 'comment', MasterData, 'control')
}
function parsrdataApplyJobs(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        ApplyJobs(sessionStorage.JobId)
    }
    else if (data[0][0].ReturnValue == "1") {
        toastr.success("Apply Successfully", "", "success")
        //$("#applybutton"+sessionStorage.JobId).hide();
        if(sessionStorage.Type=='Search'){
            FillJobs('Search')
          }
        $("#applybutton"+sessionStorage.JobId).html("Applied");
        $("#applybutton"+sessionStorage.JobId).prop('disabled', true);
        fetchCounts();
      
        return true;
    }
 }


 function saveJobs(JobID){
    sessionStorage.JobId=JobID
  
    var MasterData = {
        "p_Id":'0',
        "p_JobId": JobID,
        "p_JSRegNo":sessionStorage.RegistrationId,
        "p_Save":'1'
        
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/savedjobs";
    securedajaxpost(path, 'parsrdatasaveJobs', 'comment', MasterData, 'control')
}
function parsrdatasaveJobs(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        saveJobs(sessionStorage.JobId)
    }
    else if (data[0][0].ReturnValue == "1") {
        toastr.success("Saved Successfully", "", "success")
      //  $("#savebutton"+sessionStorage.JobId).hide();
      if(sessionStorage.Type=='Search'){
        FillJobs('Search')
      }
      $("#savebutton"+sessionStorage.JobId).html("Saved");
      $("#savebutton"+sessionStorage.JobId).prop('disabled', true);
      fetchCounts();
        return true;
    }
 }