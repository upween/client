function FillVacancy() {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "vacancytype/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {
            jQuery("#Vacancy").empty();
            var data1 = data[0];
            jQuery("#Vacancy").append(jQuery("<option ></option>").val("0").html("Select Type of Job"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#Vacancy").append(jQuery("<option></option>").val(data1[i].VacancyType_id).html(data1[i].VacancyType));
            }
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}

function FillDesignation() {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "adminDesignation/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {
            jQuery("#Designation").append(jQuery("<option ></option>").val("0").html("Select Designation"));

            for (var i = 0; i < data[0].length; i++) {
                jQuery("#Designation").append(jQuery("<option></option>").val(data[0][i].Designation_Id).html(data[0][i].Designation));
            }
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}

function FillDistrict(funct, control, state) {
    var path = serverpath + "district/0/0/0/0"
    securedajaxget(path, funct, 'comment', control);
}

function parsedatasecuredFillDistrict(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillDistrict('parsedatasecuredFillDistrict', 'District');
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else {
        jQuery("#" + control).empty();
        var data1 = data[0];
        jQuery("#" + control).append(jQuery("<option ></option>").val("0").html("Select Location"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#" + control).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
        }
    }

}


function FillEducation(funct, control) {
    var path = serverpath + "secured/qualification/0/0/0"
    securedajaxget(path, funct, 'comment', control);
}

function parsedatasecuredFillEducation(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillEducation('parsedatasecuredFillEducation', 'Qualification');
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else {
        var data1 = data[0];
        jQuery("#" + control).append(jQuery("<option ></option>").val('0').html("Select Education"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#" + control).append(jQuery("<option></option>").val(data1[i].Qualif_id).html(data1[i].Qualif_name));
        }
    }

}






function redirect() {
    // sessionStorage.setItem("Experience",$("#Years").val())
    sessionStorage.setItem("Designation", $("#Designation").val())
    sessionStorage.setItem("Qualification", $("#Qualification").val())
    // if ($("#SubjectGroup").val() == null){

    // }
    // else{
    //     sessionStorage.setItem("SubjectGroup",$("#SubjectGroup").val())
    // }
    sessionStorage.setItem("TypeOfJob", $("#Vacancy").val())
    sessionStorage.setItem("District", $("#District").val())
    // sessionStorage.setItem("Skill",$("#Skill").val())
    window.location = '/JobListbySearch';
}


