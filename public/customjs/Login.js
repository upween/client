
function ValidateLogin() {
  $('#login').attr('disabled',true);
  var MasterData = {
        
    "p_UserId":jQuery('#txtloglogin').val(),
    "p_Password": md5(jQuery('#txtlogpassword').val())
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "validatelogin";
ajaxpost(path, 'parsedatalogin', 'comment', MasterData, 'control')
  
  }   
  function parsedatalogin(data){   
    data = JSON.parse(data);
    $.unblockUI();
  
    if(data.errno) {
      toastr.warning("Something went wrong Please try again later", "", "info")
      return false;
  }
  if(data.result.userDetails){
if (data.result.userDetails.ReturnValue == "1" || data.result.userDetails.ReturnValue == "2") {
      $('#login').attr('disabled',false);
        sessionStorage.setItem("token", data.result.token);
        sessionStorage.setItem("refreshToken", data.result.refreshToken)
        sessionStorage.setItem("RegistrationId", data.result.userDetails.RegistrationId);
        sessionStorage.setItem("CandidateId", data.result.userDetails.CandidateId);
        sessionStorage.setItem("CandidateName", data.result.userDetails.CandidateName);
        sessionStorage.setItem("MobileNumber", data.result.userDetails.MobileNumber);
        sessionStorage.setItem("EmailId", data.result.userDetails.EmailId);
         sessionStorage.setItem("CandidateDistrictName", data.result.userDetails.DistrictName);
         sessionStorage.setItem("CandidateQualification", data.result.userDetails.Qualif_name);
         sessionStorage.setItem("RegDate", data.result.userDetails.RegDate);

        Cookies.set('modaltype', "", { expires: 1, path: '/' });
        Cookies.set('RegistrationId', data.result.userDetails.RegistrationId, { expires: 1, path: '/' });
        Cookies.set('CandidateName', data.result.userDetails.CandidateName, { expires: 1, path: '/' });
        Cookies.set('MobileNumber', data.result.userDetails.MobileNumber, { expires: 1, path: '/' });
        Cookies.set('EmailId', data.result.userDetails.EmailId, { expires: 1, path: '/' });
        Cookies.set('CandidateId', data.result.userDetails.CandidateId, { expires: 1, path: '/' });
        Cookies.set('FirstName', data.result.userDetails.FirstName, { expires: 1, path: '/' });
        
        if (parseInt(data.result.userDetails.ProfileStatus)>=50)
        {
          
          if(sessionStorage.Type=='Jobfair')
          {
            window.location="/JobFairList";
          }
          else   if(sessionStorage.Type=='ITI'){
            window.location="/JobFairList";
          }
          else{
         //   RegDate
         localStorage.setItem("Page","Home");
            window.location="/Home";
          }
        
        }
    
        else{
          localStorage.setItem("Page","reg");
          window.location="/Profilereg";
        }
         
      }
    }
      else{
        $('#login').attr('disabled',false);
        createCaptcha();
        $('#login').attr('disabled',false);
        jQuery('#cpatchaTextBox').val(""); 
        jQuery('#InvalidUserIdCheck').show();
        
        return true;
      }
    
  

  }


     //   window.location="/Profile_new";

    //     jQuery.ajax({
    //     type: "GET",
    //  contentType: "application/json; charset=utf-8",
    //  url: serverpath + "validateJobSeekerReg/'"+jQuery('#txtloglogin').val()+"'/",
    //      cache: false,
    //    dataType: "json",
    //    success: function (data) {
    //      jQuery('#txtlogin').val("");
    //      jQuery('#txtpassword').val("");
    //      jQuery('#cpatchaTextBoxheader').val("");
    //    if  (data[0][0].ReturnValue=="1"){
       
    //          window.location="/Home";
    //    }
    //      else if (data[0][0].ReturnValue=="2")
    //      {
    //        window.location="/Profilereg";
    //      }
    //    },
    //    error: function (xhr) {
    //        toastr.success(xhr.d, "", "error")
    //        return true;
    //       }
    //   });
        //RoleMenuMapping(data[0].RoleId);
       // window.location="/Profile_new";
  //   }
  //   else if(data.result.userDetails.ReturnValue == "2"){
  //       createCaptcha();
  //       jQuery('#cpatchaTextBox').val(""); 
  //       jQuery('#InvalidUserIdCheck').show();
  //       return true;
        
  //   }
  //   else{
  //     toastr.warning(data, "", "info")
  //   }
  // }



function CheckValidation(){
  if(isValidate){
    if (jQuery('#txtloglogin').val() == '') {
        
      getvalidated('txtloglogin','text','User Name');
    }
    if (jQuery('#txtlogpassword').val() == '') {
      getvalidated('txtlogpassword','text','Password');
     
    }
    if(isCaptchaValidated){  
     if (jQuery('#cpatchaTextBox').val() == '') {
      getvalidated('cpatchaTextBox','text','Captcha');  
     
    }
    else { 
       validateCaptcha() ;
     }
    }
    else{
      ValidateLogin();
    }
      
  }
  else{
    ValidateLogin();
  }
    
       
  
}
function checkpassword(){
    var txtpassword;
    txtpassword = jQuery('#txtpassword').val();
    var reg = /[0-9a-fA-F]{4,8}/;
    if (reg.test(txtpassword) == false) {
        $("#txtpassword").focus();
        toastr.warning("Please Enter Correct Password", "", "info")
        return true;
    }
}


  
$(".toggle-password").click(function () {
  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
      input.attr("type", "text");
  } else {
      input.attr("type", "password");
  }
  });
