function FillDesignationTable() {
    var path =  serverpath + "secured/adminDesignation/0/0"
    securedajaxget(path,'parsedatasecuredFillDesignationTable','comment',"control");
}
function parsedatasecuredFillDesignationTable(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDesignationTable();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else{
        
        var rowLength = 4
        var data1 = data[0];
        var appenddata="";
        var desig = [];
        for (var i = 0; i < data1.length; i++) {
            desig.push(encodeURI(data[0][i].Designation))
        }
        for (let i = 0; i < desig.length; i+=rowLength) {
            const tds = desig.slice(i, i + rowLength).map(value => `<td><a href='#.' onclick=SetDesignation('${value}')>${decodeURI(value)}</a></td>`)
            appenddata += `<tr>${tds.join("")}</tr>`
        }
        jQuery("#Designation").html(appenddata);  
    }    
    
}

jQuery("#txtSearch").keyup(function () {
    searchTextInTable("Designation");
})


function SetDesignation(DesName){
    sessionStorage.setItem("Designation",decodeURI(DesName));
    sessionStorage.setItem("Location",'');
    sessionStorage.setItem("Qualification",'');
    sessionStorage.setItem("Sector",'');
    window.location='/joblisting';
}




