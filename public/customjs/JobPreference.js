function fillDepartment(funct,control) {
    var path =  serverpath + "secured/department/0/0/'" + sessionStorage.getItem("IpAddress") + "'/'" + sessionStorage.getItem("CandidateId") + "'"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredfillDepartment(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        fillDepartment('parsedatasecuredfillDepartment','Department');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else{
        

        
        var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val('0').html("Select Department"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].DepartmentId).html(data1[i].DepartmentName));
            }
    }
}
// jQuery('#Department').on('change', function () {
//           FillDesignationJobpref('parsedatasecuredFillDesignationJobpref',jQuery('#Department').val());
//   });  

  function FillDesignationJobpref(funct,DepartmentId) {
// var path =  serverpath + "secured/designation/'" + DepartmentId + "'/'"+sessionStorage.getItem("IpAddress")+"'/0/'"+sessionStorage.getItem("CandidateId")+"'"
var path =  serverpath + "secured/adminDesignation/0/0"
    securedajaxget(path,funct,'comment','control');
}

function parsedatasecuredFillDesignationJobpref(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDesignationJobpref('parsedatasecuredFillDesignationJobpref',jQuery('#Department').val());
    }
    else{
    var data1 = data[0];
    
        $('#Designation').selectize({
            persist: false,
            createOnBlur: true,
             valueField: 'DesignationName',
            labelField: 'DesignationName',
            searchField: 'DesignationName',
            options: data1,
            create: true,
            maxItems:10
        
        });

    }
    }
    function InsUpdJobPreference() {

        
    
        var MasterData = {
            
            "p_CandidateId": sessionStorage.getItem("CandidateId"),
            "p_Qualification": "",
            "p_Location":$("#location").val(),
            "p_Industry": jQuery('#Department option:selected').text(),
           // "p_Title":$("#Designation")[0].dataset.id ,
            "p_Title":$("#Designation").val(),
            "p_Type": "Job",
            
        };
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "secured/setpreference";
        securedajaxpost(path, 'parsrdatajobpreference', 'comment', MasterData, 'control')
    }
    function parsrdatajobpreference(data) {
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            InsUpdJobPreference();
        }
        else if (data[0][0].ReturnValue == "1") {
            if(jQuery('#location').val()=="Within District"){
                localStorage.setItem('CandidateDistrictName',sessionStorage.CandidateDistrictName)
            }
            // else if(jQuery('#location').val()=="Within State"){
            //     localStorage.setItem('CandidateDistrictName',"")
            // }
            else if(jQuery('#location').val()=="Any where in india"){
                localStorage.setItem('CandidateDistrictName',"")
                }

            sessionStorage.setItem('CandidateSector',jQuery('#Department option:selected').text())
            sessionStorage.setItem('CandidateDesignation',jQuery('#Designation').val())

            resetjobpreference();
            FillJobPreference() ;
            toastr.success("Submit Successfully", "", "success")
            return true;
        }
        else if (data[0][0].ReturnValue == "2") {
            if(jQuery('#location').val()=="Within District"){
                localStorage.setItem('CandidateDistrictName',sessionStorage.CandidateDistrictName)
            }
            // else if(jQuery('#location').val()=="Within State"){
            //     localStorage.setItem('CandidateDistrictName',"")
            // }
            else if(jQuery('#location').val()=="Any where in india"){
                localStorage.setItem('CandidateDistrictName',"")
                }
            sessionStorage.setItem('CandidateSector',jQuery('#Department option:selected').text())
            sessionStorage.setItem('CandidateDesignation',jQuery('#Designation').val())

            resetjobpreference();
            FillJobPreference() ;
            toastr.success("Submit Successfully", "", "success")
            
            return true;
        }
        
    
    }
    function resetjobpreference(){
        $("#location").val("Abroad"),
            jQuery('#Department').val("0");
          //  $("#Designation").val('0');
            $("#Designation")[0].selectize.clear();
    }
    
    function FillJobPreference() {
        
        var path =  serverpath + "secured/setpreference/"+sessionStorage.getItem("CandidateId")+""
        securedajaxget(path,'parsedatasecuredFillJobPreference','comment','control');
    }
    
    function parsedatasecuredFillJobPreference(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token); 
            FillJobPreference();
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{

              var appendata="";
              
              appendata +="<tr><td></td><td>"+data[0][0].Location+"</td><td>"+data[0][0].Industry+"</td><td style='word-break:break-word'>"+data[0][0].Title+"</td></tr>"; 
              $("#jobpreferencetbl").html(appendata);
            }
          
                  
    }
    function CheckValidationPreference() {
        if (jQuery('#Department').val() == '0') {
            getvalidated('Department','select','Department')
        return false;}
        
        if ($("#Designation").val() == '') {
            $("#Designation").css('border-color', 'red');
            $("#validDesignation").html("Please Enter Designation");
            return false;
        }
        else { 
           
            InsUpdJobPreference();
        }
      
        
    };