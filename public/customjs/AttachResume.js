function InsResume(){
    $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);

    var MasterData = {
      "p_ResumeId": localStorage.getItem("ResumeId"),
      "p_CandidateId":sessionStorage.getItem("CandidateId"),
      "p_Resume": jQuery("#myCV").val()
  
    }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/Resume";
    securedajaxpost(path,'parsrdataresumeheadline','comment',MasterData,'control')
}
function parsrdataResume(data,control){
        data = JSON.parse(data)
     if   (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        InsResume();
        FillResumeSecured('parsedatasecuredresume','Resumefetch');
    }
}
ProfileStatus('parsedataProfileStatus','progressbar');
/*  else if (data[0][0].ReturnValue == "1") {
        toastr.success("Submit Successfully", "", "success")
        $('#myModal1').modal('toggle');
        FillResumeHeadlineSecured('parsedatasecuredresumeheadline','detail');
        return true;
    }
    else if (data[0][0].ReturnValue == "2") {
        toastr.success("Submit Successfully", "", "success")
        $('#myModal1').modal('toggle');
        FillResumeHeadlineSecured('parsedatasecuredresumeheadline','detail');
        return true;
    } 
}  */

     function FillResumeSecured(funct,control) {
        var path = serverpath + "secured/Resume/'"+sessionStorage.getItem("CandidateId")+"'"
        securedajaxget(path,funct,'comment',control);
    }
    function parsedatasecuredresume(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillResumeSecured('parsedatasecuredresume','Resumefetch');
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
       
            else{
            var data1 = data[0];
            
          //  localStorage.setItem("ResumeId",data1[0].ResumeId)
            appenddata=""+data1[0].Resume+" <i class='fa fa-pencil'style='padding-left: 12px;padding-right: 12px'></i>";
            jQuery("#Resumefetch").html(appenddata);
            jQuery("#AttechResumeUpdate").html("Updated On "+data[0][0].Entry_date+"");
        }
    }
