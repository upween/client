function InsUpdPhysicalDetail(){
    $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);
 
    var MasterData = {
        "p_PhysicalDetailId": localStorage.getItem("PhysicalDetailId"),
        "p_CandidateId": sessionStorage.getItem("CandidateId"),
        "p_Chest": jQuery("#txtchest").val(),
        "p_Height": jQuery("#txtHeight").val(),
        "p_Weight": jQuery("#txtWeight").val(),
        "p_EyeSight": jQuery("#Eyesight").val(),
        "p_IsActive": '1',
        "p_UserId":sessionStorage.getItem("CandidateId"),
        "p_IpAddress":sessionStorage.getItem("IpAddress"),
        "p_SportsName": jQuery("#sportname").val(),
        "p_SportsCertificate": jQuery("#sportcertificate").val(),
        "p_NCCCertificate": $("#NCCcertificate").val(),
    }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/physicaldetails";
    securedajaxpost(path,'parsrdataInsUpdPhysicalDetail','comment',MasterData,'control')
}

function parsrdataInsUpdPhysicalDetail(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        InsUpdPhysicalDetail();
        
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
    if (data[0][0].ReturnValue == "1") {
        //  $('#Close').click();
        ProfileStatus('parsedataProfileStatus','progressbar');
          $('#myModal1').modal('toggle');
          FetchPhysicalDetail('parsedatasecuredFetchPhysicalDetail','physicaldetail');
          toastr.success("Insert Successful", "", "success")
          return true;
      }
       else if (data[0][0].ReturnValue == "2") {
        //  $('#Close').click();
          $('#myModal1').modal('toggle');
          FetchPhysicalDetail('parsedatasecuredFetchPhysicalDetail','physicaldetail');
        //  resetPhysicalDetail();
          toastr.success("Update Successful", "", "success")
          return true;
      }
    
     }

     function resetPhysicalDetail() {
        jQuery("#txtchest").val("")
        jQuery("#txtHeight").val("")
        jQuery("#txtWeight").val("")
        jQuery("#Eyesight").val("0")
        jQuery("#sportname").val("0")
        jQuery("#sportcertificate").val("0")
        jQuery("#NCCcertificate").val("0")
         }

     function FetchPhysicalDetail(funct,control) {
            var path = serverpath + "secured/physicaldetails/'"+sessionStorage.getItem("CandidateId")+"'/'"+sessionStorage.getItem("IpAddress")+"'/'"+sessionStorage.getItem("CandidateId")+"'"
            securedajaxget(path,funct,'comment',control);
        }

    function parsedatasecuredFetchPhysicalDetail(data,control){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FetchPhysicalDetail('parsedatasecuredFetchPhysicalDetail','physicaldetail');
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
               else if(data.errno) {
                    toastr.warning("Something went wrong Please try again later", "", "info")
                    return false;
                }
            
            else{
                if(data[0]=="")
                {
                   localStorage.setItem("PhysicalDetailId","0");
                  // jQuery("#"+control).html("Add Physical Details");
                  $("#yes")[0].checked=false;
                }
                else{
                     $("#yes")[0].checked=true;
                    // $("#radiono")[0].checked=false;
                var data1 = data[0];
                localStorage.setItem("PhysicalDetailId",data1[0].PhysicalDetailId)
                var appenddata="";
                var profilehead="";
                jQuery("#"+control).empty();   

                if(data1[0].Eye_sight =='0' || data1[0].Eye_sight ==null){
                    var eye_sight="-";
                }
                else{
                    var eye_sight= data1[0].Eye_sight ;   
                }

                if(data1[0].S_Name =='0' || data1[0].S_Name ==null){
                    var s_Name="-";
                }
                else{
                    var s_Name= data1[0].S_Name ;   
                }

                if(data1[0].S_Certificate_name =='0' || data1[0].S_Certificate_name ==null){
                    var s_Certificate_name="-";
                }
                else{
                    var s_Certificate_name= data1[0].S_Certificate_name ;   
                }

                if(data1[0].ncc_Certificate_name =='0' || data1[0].ncc_Certificate_name ==null){
                    var NCC_Certificate_name="-";
                }
                else{
                    var NCC_Certificate_name= data1[0].ncc_Certificate_name ;   
                }

               
               

                appenddata+="<div class='col-md-12'><div class='col-md-4'><p class='profilesubheading'><span lang='en'>Chest</span></p><p class='profiledetail'>"+data1[0].Chest+" cm</p></div>"+
                "<div class='col-md-4'><p class='profilesubheading'><span lang='en'>Height</span></p><p class='profiledetail'>"+data1[0].Height+" cm</p></div>"+
                "<div class='col-md-4'><p class='profilesubheading'> <span lang='en'>Weight</span></p><p class='profiledetail'>"+data1[0].Weight+" kg</p></div></div>"+
                
                 "<div class='col-md-12'>"+
                 "<div class='col-md-4'><p class='profilesubheading'><span lang='en'>EyeSight</span></p><p class='profiledetail'>"+eye_sight+"</p></div>"+
                 "<div class='col-md-4'><p class='profilesubheading'><span lang='en'>Sports<span></p><p class='profiledetail'>"+s_Name+" </p></div>"+
                "<div class='col-md-4'><p class='profilesubheading'><span lang='en'>Sport Certificate</span></p><p class='profiledetail'>"+s_Certificate_name+" </p></div></div>"+

                "<div class='col-md-12'><div class='col-md-4'><p class='profilesubheading'><span lang='en'>NCC Certificate</span></p><p class='profiledetail'>"+NCC_Certificate_name+" </p></div></div></div>";
                jQuery("#"+control).html(appenddata);
               profilehead+=" <span lang='en'>Updated On</span> "+data[0][0].UpdateOn+"";
               jQuery("#physicaldetailupdateon").html(profilehead);
               $("#txtchest").val(data1[0].Chest)
               $("#txtHeight").val(data1[0].Height)
               $("#txtWeight").val(data1[0].Weight),
               $("#Eyesight").val(data1[0].EyeSight),
               $("#sportname").val(data1[0].SportsName),
               $("#sportcertificate").val(data1[0].SportsCertificate),
               $("#NCCcertificate").val(data1[0].NCCCertificate)
               
    //    ProfileStatus('parsedataProfileStatus','progressbar');
            }
          
        
    }
    }
    // $('input[type="radio"]').click(function () {
    //     if ($(this).attr('id') == 'yes') {
    //         $("#physicaldetail").show();
    //         //InsUpdPhysicalDetail();
    //     }
    //     else if ($(this).attr('id') == 'radiono') {
    //        // resetPhysicalDetail();
    //         Delete(sessionStorage.getItem("CandidateId"),'PhysicalDetail')
    //         $("#physicaldetail").hide();
    //         jQuery("#physicaldetailupdateon").hide()
            
    //     }
    //   });
$('#radiono').on('click',function(){
    Delete(localStorage.getItem("PhysicalDetailId"),'PhysicalDetail')
    $('#physicaldetail').html(' ');
    resetPhysicalDetail();
})

    function FillEyesight(funct,control) {
        var path =  serverpath + "secured/EyeSight/0/0"
        securedajaxget(path,funct,'comment',control);
      }
      
      function parsedatasecuredFillEyesight(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillEyesight('parsedatasecuredFillEyesight','Eyesight');
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
           else if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Eye Sight"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Eye_sight_id).html(data1[i].Eye_sight));
                 }
            }
                  
      }

      function FillSportname(funct,control) {
        var path =  serverpath + "secured/SportsName/0/0"
        securedajaxget(path,funct,'comment',control);
      }
      
      function parsedatasecuredFillSportname(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillSportname('parsedatasecuredFillSportname','sportname');
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
           else if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Sport Name"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].S_id).html(data1[i].S_Name));
                 }
            }
                  
      }

      function FillSportcertificate(funct,control) {
        var path =  serverpath + "secured/SportsCertificate/0/0"
        securedajaxget(path,funct,'comment',control);
      }
      
      function parsedatasecuredFillSportcertificate(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillSportcertificate('parsedatasecuredFillSportcertificate','sportcertificate');
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
           else if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Sport Certificate"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].S_Certificate_id).html(data1[i].S_Certificate_name));
                 }
            }
                  
      }

      function FillNCCcertificate(funct,control) {
        var path =  serverpath + "secured/ncccertificate/0/0"
        securedajaxget(path,funct,'comment',control);
      }
      
      function parsedatasecuredFillNCCcertificate(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillNCCcertificate('parsedatasecuredFillNCCcertificate','NCCcertificate');
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
           else if(data.errno) {
                toastr.warning("Something went wrong Please try again later", "", "info")
                return false;
            }
        
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select NCC Certificate"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].ncc_Certificate_id).html(data1[i].ncc_Certificate_name));
                 }
            }
                  
      }


   function Check(){
       if(isValidate==false){
        if($('#txtHeight').val() =='' && $('#txtWeight').val() =='' && $('#txtchest').val() =='' && $('#Eyesight').val() =='0' && $('#sportname').val() =='0' && $('#sportcertificate').val() =='0' && $("#NCCcertificate").val()=='0'){
            return false;
        }
    }
      
       else{
        if ($('#txtchest').val() > 200){
            //  alert("No numbers above 200");
            toastr.warning("No numbers above 200", "", "info") 
            return false;
            }
           

        if ($('#txtHeight').val() > 200){
         //   alert("No numbers above 200");
            toastr.warning("No numbers above 200", "", "info")
          return false;
          }
         
  
          if ($('#txtWeight').val() > 200){
            //  alert("No numbers above 200");
            toastr.warning("No numbers above 200", "", "info") 
                 return false;
            }
  
    

  
          
  
          else{
              InsUpdPhysicalDetail();
          }
       }
        
    }
      

    function physicaldetailAPI(){
        FetchPhysicalDetail('parsedatasecuredFetchPhysicalDetail','physicaldetail');
        FillEyesight('parsedatasecuredFillEyesight','Eyesight');
        FillSportname('parsedatasecuredFillSportname','sportname');
        FillSportcertificate('parsedatasecuredFillSportcertificate','sportcertificate');
        FillNCCcertificate('parsedatasecuredFillNCCcertificate','NCCcertificate');

    }