var globalString = "This can be accessed anywhere!";
var globalfilestatus = false;
//var globalpath = "http://182.70.254.93:300/";
var photogallerypath = "http://olex.mprojgar.gov.in/"
var EmployerLogoPath = "http://vacancy.mprojgar.gov.in/"
//var serverpath = "http://182.70.254.93:303/";
//var serverpath="http://localhost:3300/"

var serverpath = "http://mprojgar.gov.in/api/";
var globalpath = "http://localhost:3000/";

var isValidate=true;
var isCaptchaValidated=false;

var globalemailid = ''
var globalemailpass = ''




sessionStorage.setItem("IpAddress", "182.70.92.78");
sessionStorage.setItem("City", "Bhopal");
function securedajaxget(path, type, comment, control) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        headers: { 'authorization': sessionStorage.getItem("token"), 'refreshToken': sessionStorage.getItem("refreshToken") },
        url: path,
        cache: false,
        dataType: "json",
        success: function (successdata) {
            window[type](JSON.stringify(successdata), control);
        },
        error: function (errordata) {
            if (errordata.status == 0) {
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else if (errordata.status == 401) {
                window[type](JSON.stringify(errordata.responseJSON), control);
                return true;
            }
            else {
                window[type](JSON.stringify(errordata), control);
                return true;
            }
        }
    });
}
function securedajaxpost(path, type, comment, masterdata, control) {
    jQuery.ajax({
        url: path,
        type: "POST",
        headers: { 'authorization': sessionStorage.getItem("token"), 'refreshToken': sessionStorage.getItem("refreshToken") },
        data: masterdata,
        contentType: "application/json; charset=utf-8",
        success: function (successdata, status, jqXHR) {
            window[type](JSON.stringify(successdata));
        },
        error: function (errordata) {
            if (errordata.status == 0) {
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else if (errordata.status == 401) {
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
            else {
                window[type](JSON.stringify(errordata));
            }
        }
    });
}
function ajaxget(path, type, comment) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: path,
        cache: false,
        dataType: "json",
        success: function (successdata) {
            window[type](JSON.stringify(successdata));
        },
        error: function (errordata) {
            if (errordata.status == 0) {
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else {
                window[type](JSON.stringify(errordata));
            }
        }
    });
}
function ajaxpost(path, type, comment, masterdata, control) {
    jQuery.ajax({
        url: path,
        type: "POST",
        data: masterdata,
        contentType: "application/json; charset=utf-8",
        success: function (successdata, status, jqXHR) {
            window[type](JSON.stringify(successdata));
        },
        error: function (errordata) {
            if (errordata.status == 0) {
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else {
                window[type](JSON.stringify(errordata));
            }
        }
    });
}
function getvalidated(controlid, type, format) {
    var validid = "valid" + controlid;
    if (type == "text") {
        if ($.trim($("#" + controlid).val()) == "") {
            $("#" + controlid).css('border-color', 'red');
            $("#" + validid).html("Please Enter " + format);
        }
        else {
            jQuery("#" + controlid).css('border-color', '');
            $("#" + validid).html("");
        }
    }
    else if (type == "select") {
        if ($("#" + controlid).val() == "0" || $("#" + controlid).val() == "99999") {
            $("#" + controlid).css('border-color', 'red');
            $("#" + validid).html("Please Select " + format);
        }
        else if ($("#" + controlid).val() == "") {
            $("#" + controlid).css('border-color', 'red');
            $("#" + validid).html("Please Select " + format);
        }
        else if ($("#" + controlid).val() == null) {
            $("#" + controlid).css('border-color', 'red');
            $("#" + validid).html("Please Select " + format);
        }
        else {
            jQuery("#" + controlid).css('border-color', '');
            $("#" + validid).html("");
        }
    }
    else if (type == "email") {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if ($.trim($("#" + controlid).val()) == "") {
            $("#" + controlid).css('border-color', 'red');
            $("#" + validid).html("Please Enter " + format);
        }
        else if ($("#" + controlid).val() != "") {
            if ($("#" + controlid).val().match(mailformat)) {
                jQuery("#" + controlid).css('border-color', '');
                $("#" + validid).html("");
                return true;
            }
            else {
                $("#" + controlid).val("");
                $("#" + controlid).css('border-color', 'red');
                $("#" + validid).html("Please Enter Correct " + format);
                return false;
            }
        }
        else {
            jQuery("#" + controlid).css('border-color', '');
            $("#" + validid).html("")
            return true;
        }
    }
    else if (type == "number") {
        if (format == "Mobile Number") {
            if ($.trim($("#" + controlid).val()) == "") {
                $("#" + controlid).css('border-color', 'red');
                $("#" + validid).html("Please Enter " + format);
            }
            else if ($("#" + controlid).val() != "") {
                var phoneno = /^\d{10}$/;
                if ($("#" + controlid).val().match(phoneno)) {
                    jQuery("#" + controlid).css('border-color', '');
                    $("#" + validid).html("");
                    return true;
                }
                else {
                    $("#" + controlid).css('border-color', 'red');
                    $("#" + validid).html("Please Enter Correct " + format);
                    return false;
                }
            }
            else {
                jQuery("#" + controlid).css('border-color', '');
                $("#" + validid).html("");
            }
        }
    }

}
function onlyNumbers(event) {
    if (event.type == "paste") {
        var clipboardData = event.clipboardData || window.clipboardData;
        var pastedData = clipboardData.getData('Text');
        if (isNaN(pastedData)) {
            event.preventDefault();

        } else {
            return;
        }
    }
    var keyCode = event.keyCode || event.which;
    if (keyCode == 9){
        return true;
    }
    if ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105)) { 
        keyCode -= 48;
    }
    if ((keyCode >= 48 && keyCode <= 57) ) { 
        return true;
    }
    var charValue = String.fromCharCode(keyCode);
    if (isNaN(parseInt(charValue)) && event.keyCode != 8) {
        event.preventDefault();
    }
}
function myFunction(my) {
   
    if (my.text == "English") {
        $(my).text("Hindi");
        window.lang.change('en');

      setTimeout(function(){ 
        if(window.location.href==globalpath+'JsFAQ#'){
                         location.reload();
                       }
      }, 2000); 
        return false;
    }
    else {
       
        $(my).text("English");
        window.lang.change('th');
     
    
      setTimeout(function(){ 
           if(window.location.href==globalpath+'JsFAQ#'){
                            location.reload();
                          }
         }, 3000); 
        return false;
    }
   
}
function onlyAlphabets(event) {
    //allows only alphabets in a textbox
    if (event.type == "paste") {
        var clipboardData = event.clipboardData || window.clipboardData;
        var pastedData = clipboardData.getData('Text');
        if (isNaN(pastedData)) {
            return;

        } else {
            event.prevetDefault();
        }
    }
    var charCode = event.which;
    if (!(charCode >= 65 && charCode <= 120) && (charCode != 32 && charCode != 0) && charCode != 8 && (event.keyCode >= 96 && event.keyCode <= 105)) {
        event.preventDefault();
    }
}

function FillImage(funct) {
    var path = serverpath + "secured/jobseekerimage/'" + sessionStorage.getItem("CandidateId") + "'"
    securedajaxget(path, funct, 'comment', 'control');
}
function parsedatasecuredFillImage(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillImage('parsedatasecuredFillImage');
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }

    else {
        if (data[0] == "") {
            $('#ProfilePic').attr('src', "images/candidates/01.jpg");

            $('#homeimage').attr('src', "images/candidates/01.jpg");

            $('#imagepro').attr('src', "images/candidates/01.jpg");
            $('#CVImage').attr('src', "images/candidates/01.jpg");

        }
        else {
            $('#ProfilePic').attr('src', "Pics/" + data[0][0].image);
            sessionStorage.setItem("PicFileName", data[0][0].image)
            $('#homeimage').attr('src', "Pics/" + data[0][0].image);

            $('#imagepro').attr('src', "Pics/" + data[0][0].image);
            $('#CVImage').attr('src', "Pics/" + data[0][0].image);

        }
    }

}
function searchTextInTable(tblName) {
    var tbl;
    tbl = tblName;
    jQuery("#" + tbl + " tr:has(td)").hide(); // Hide all the rows.

    var sSearchTerm = jQuery('#txtSearch').val(); //Get the search box value

    if (sSearchTerm.length == 0) //if nothing is entered then show all the rows.
    {
        jQuery("#" + tbl + " tr:has(td)").show();
        return false;
    }
    //Iterate through all the td.
    jQuery("#" + tbl + " tr:has(td)").children().each(function () {
        var cellText = jQuery(this).text().toLowerCase();
        if (cellText.indexOf(sSearchTerm.toLowerCase()) >= 0) //Check if data matches
        {
            jQuery(this).parent().show();
            return true;
        }
    });
    //e.preventDefault();
}
function isNumberKey(evt, id) {
    try {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) {
            var txt = document.getElementById(id).value;
            if (!(txt.indexOf(".") > -1)) {

                return true;
            }
        }
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    } catch (w) {
        alert(w);
    }
}
function checkLength(Id, formatlength, length) {
    var textbox = document.getElementById(Id);
    if (textbox.value.length >= parseInt(length)) {
        $("#" + Id).css('border-color', '');
        $("#valid" + Id).html("")
        return true;
    }
    else {
        $("#" + Id).css('border-color', 'red');
        if (formatlength == 'Pin') {
            $("#valid" + Id).html(formatlength + " Must Be Contain 6 Digits")
        }
        else {

            $("#valid" + Id).html(formatlength + " Must Contain Atleast " + length + " Characters")
        }


        return false;
    }
}
function textdecrease() {
    $("body").css('font-size', '12px');
}
function textincrease() {
    $("body").css('font-size', '16px');
}
function textnormal() {
    $("body").css('font-size', '14px');
}
function EmailConfig() {
    var path = serverpath + "emailconfig"
    securedajaxget(path, 'parsedatasecuredEmailConfig', 'comment', 'control');
}

function parsedatasecuredEmailConfig(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        EmailConfig();
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else {
        globalemailid = data[0][0].Email
        globalemailpass = data[0][0].Password
    }

}
function employerurl() {
    window.location = EmployerLogoPath;
}
function employerurlreg() {
    window.location = EmployerLogoPath+"Registration";
}
function adminurl() {
    window.location = photogallerypath;
}
function checkIfFileLoaded(fileName) {
    $.get(fileName, function (data, textStatus) {
        if (textStatus == "success") {
            globalfilestatus = true;
        }
    });
}
$('.yatmlogo').attr('src',globalpath+'images/YATMLogo.png');
$('.mplogo').attr('src',globalpath+'images/logo1.png');