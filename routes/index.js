// var express = require('express');

// /* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index');
// });
// /* GET about page. */
// router.get('/about', function(req, res) {
//   res.render('pages/about');
// });
// module.exports = router;
module.exports = function(app) {
  app.get('/', function(req, res) {
    res.render('index');
  });
  app.get('/about', function(req, res) {
    res.render('pages/about');
  });
  app.get('/JobSeekerRegistration', function(req, res) {
    res.render('pages/JobSeekerRegistration');
  });
  app.get('/forgot', function(req, res) {
    res.render('pages/forgot');
  });
  app.get('/Profile', function(req, res) {
    res.render('pages/Profile');
  });
  app.get('/AttachResume', function(req, res) {
    res.render('pages/AttachResume');
  });
  app.get('/PhysicalDetails', function(req, res) {
    res.render('pages/PhysicalDetails');
  });
  app.get('/DrivingLicenseDetails', function(req, res) {
    res.render('pages/DrivingLicenseDetails');
  });
  app.get('/ElectricLicenseDetails', function(req, res) {
    res.render('pages/ElectricLicenseDetails');
  });
  app.get('/Education', function(req, res) {
    res.render('pages/Education');
  });
  app.get('/Certification', function(req, res) {
    res.render('pages/Certification');
  });
  app.get('/Employment', function(req, res) {
    res.render('pages/Employment');
  });
  app.get('/OtherActivites', function(req, res) {
    res.render('pages/OtherActivites');
  });
  app.get('/PrivacyPolicy', function(req, res) {
    res.render('partials/PrivacyPolicy');
  });
  app.get('/Contactus', function(req, res) {
    res.render('pages/Contactus');
  });
  app.get('/TermsCondition', function(req, res) {
    res.render('partials/TermsCondition');
  });
  app.get('/FAQs', function(req, res) {
    res.render('partials/FAQs');
  });
  app.get('/Subheader', function(req, res) {
    res.render('partials/Subheader');
  });
  app.get('/ResumeHeadline', function(req, res) {
    res.render('pages/ResumeHeadline');
  });
  app.get('/ItSkills', function(req, res) {
    res.render('pages/ItSkills');
  });
  app.get('/PersonalDetails', function(req, res) {
    res.render('pages/PersonalDetails');
  });
  app.get('/Project', function(req, res) {
    res.render('pages/Project');
  });
  app.get('/ProfileSummry', function(req, res) {
    res.render('pages/ProfileSummry');
  });
  app.get('/JobFairs', function(req, res) {
    res.render('pages/JobFairs');
  });
  app.get('/basicdetails', function(req, res) {
    res.render('pages/basicdetails');
  });
  app.get('/Login', function(req, res) {
    req.session.login = "Loggedout";
    res.render('pages/Login');
  });
  app.get('/Skills', function(req, res) {
    res.render('pages/Skills');
  });
  app.get('/Home', function(req, res) {
    req.session.login = "Loggedin";
    res.render('pages/Home');
  });
  app.get('/JobsbyCompany', function(req, res) {
    res.render('pages/JobsbyCompany');
  });
  app.get('/Jobbycategory', function(req, res) {
    res.render('pages/Jobbycategory');
  });
  app.get('/jobbylocation', function(req, res) {
    res.render('pages/jobbylocation');
  });
  app.get('/jobsbydesignations', function(req, res) {
    res.render('pages/jobsbydesignations');
  });
  app.get('/Jobbyskills', function(req, res) {
    res.render('pages/Jobbyskills');
  });
  app.get('/Jobs', function(req, res) {
    res.render('pages/Jobs');
  });
  app.get('/recruiters', function(req, res) {
    res.render('pages/recruiters');
  });
  app.get('/Profile_new', function(req, res) {
    res.render('pages/Profile_new');
  });
  app.get('/Profile_one', function(req, res) {
    res.render('pages/Profile_one');
  });
  app.get('/Profile_two', function(req, res) {
    res.render('pages/Profile_two');
  });
  app.get('/packages', function(req, res) {
    res.render('pages/packages');
  });
  app.get('/GrievenceForm', function(req, res) {
    res.render('pages/GrievenceForm/GrievenceForm');
  });
  app.get('/EmployerRegistration', function(req, res) {
    res.render('pages/EmployerZone/EmployerRegistration');
  });
  app.get('/Employerlogin', function(req, res) {
    res.render('pages/EmployerZone/Employerlogin');
  });
   app.get('/JobAlerts', function(req, res) {
    res.render('pages/JobAlerts');
  });
  app.get('/FeedBackForm', function(req, res) {
    res.render('pages/FeedBack/FeedBackForm');
  });
  app.get('/Resume', function(req, res) {
    res.render('pages/Resume');
  });
  app.get('/CVTemplate', function(req, res) {
    res.render('pages/CVTemplate');
  });
  app.get('/ProfileImage', function(req, res) {
    res.render('pages/ProfileImage');
  });
  app.get('/Category', function(req, res) {
    res.render('pages/Master/CategoryMaster');
  });
  app.get('/FAQ', function(req, res) {
    res.render('pages/JobseekerFAQ');
  });
  app.get('/Component', function(req, res) {
    res.render('pages/Component');
  });
  app.get('/SuccessRegistration', function(req, res) {
    res.render('pages/SuccessRegistration');
  });
  app.get('/Test1', function(req, res) {
    res.render('pages/Test1');
  });
  app.get('/JobDescription', function(req, res) {
    res.render('pages/JobSearch/JobDescription',{
      CandidateId: req.cookies.CandidateId
    });
  });
  app.get('/JobDetail', function(req, res) {
    res.render('pages/HomePage/JobDetail');
  });
  app.get('/GovermentEmpDepartment', function(req, res) {
    res.render('pages/Service/GovermentEmpDepartment');
  });
  app.get('/SkillDevelopmentCenters', function(req, res) {
    res.render('pages/SDCPages/SkillDevelopmentCenters');
  });

  app.get('/RecruiterRegistration', function(req, res) {
    res.render('pages/RecruiterPages/RecruiterRegistration');
  });
  app.get('/Recruiterlogin', function(req, res) {
    res.render('pages/RecruiterPages/Recruiterlogin');
  });
  app.get('/CouncellerAccess', function(req, res) {
    res.render('pages/Service/CouncellerAccess');
    
  });
  app.get('/PlacementServiceModel', function(req, res) {
    res.render('pages/Service/PlacementServiceModel');
  });
  app.get('/SDCRegistration', function(req, res) {
    res.render('pages/SDCPages/SDCRegistration');
  });
  app.get('/SDCLogin', function(req, res) {
    res.render('pages/SDCPages/SDCLogin');
  });
  app.get('/Companies', function(req, res) {
    res.render('pages/Company/Companies');
  });
  app.get('/SelfEmployment', function(req, res) {
    res.render('pages/SelfEmployment');
  });
  app.get('/JobListing', function(req, res) {
    res.render('pages/JobSearch/JobListing',{
      CandidateId: req.cookies.CandidateId
    });
  });
  app.get('/KnowRegistration', function(req, res) {
    res.render('pages/KnowRegistration');
  });
  app.get('/Transfer', function(req, res) {
    res.render('pages/Transfer');
  });
  app.get('/Profilereg', function(req, res) {
    res.render('pages/Profilereg');
  });
  app.get('/JobFairScheme', function(req, res) {
    res.render('pages/JobFairScheme');
  });
  app.get('/CareerCounsellingScheme', function(req, res) {
    res.render('pages/CareerCounsellingScheme');
  });
  app.get('/CareerCounselingQA', function(req, res) {
    res.render('pages/CareerCounselingQA');
  });
  app.get('/Usefullinks', function(req, res) {
    res.render('partials/Usefullinks');
  });
  app.get('/RoleoftheDepartment', function(req, res) {
    res.render('pages/AboutUs/RoleoftheDepartment');
  });
  app.get('/OrganizationalSetup', function(req, res) {
    res.render('pages/AboutUs/OrganizationalSetup',{
      
      lang: req.cookies.langCookie
     
    });
  });
  app.get('/EmploymentOffices', function(req, res) {
    res.render('pages/AboutUs/EmploymentOffices');
  });
  app.get('/MIS', function(req, res) {
    res.render('pages/AboutUs/MIS');
  });
  app.get('/Statistics', function(req, res) {
    res.render('pages/AboutUs/Statistics');
  });
  app.get('/RighttoInformation', function(req, res) {
    res.render('pages/AboutUs/RighttoInformation');
  });
  app.get('/AdministrativeReports', function(req, res) {
    res.render('pages/AboutUs/AdministrativeReports');
  });
  app.get('/CircularOROrders', function(req, res) {
    res.render('pages/AboutUs/CircularOROrders');
  });
  app.get('/InformationofDifferentlyabledPerson', function(req, res) {
    res.render('pages/AboutUs/PersonWithDisabilityInformation');
  });
  app.get('/ActandRules', function(req, res) {
    res.render('pages/AboutUs/ActandRules');
  });
  app.get('/CNVAct', function(req, res) {
    res.render('pages/AboutUs/CNVAct');
  });
  app.get('/facilities', function(req, res) {
    res.render('pages/facilities');
  });
  app.get('/language', function(req, res) {
    res.render('pages/language');
  });
  app.get('/PrintRegistrationcard', function(req, res) {
    res.render('pages/PrintRegistrationcard');
  });
  app.get('/Disclaimer', function(req, res) {
    res.render('partials/Disclaimer');
  });
  app.get('/TypingOrSteno', function(req, res) {
    res.render('pages/TypingOrSteno')
  });
  app.get('/JobfairReg', function(req, res) {
    res.render('pages/JobfairReg');
  });
  app.get('/captcha', function(req, res) {
    res.render('pages/captcha');
  });
  app.get('/Email', function(req, res) {
    res.render('pages/Emailverification/emailmodal');
  });
  app.get('/Mobile', function(req, res) {
    res.render('pages/verification/Mobilemodal');
  });
  app.get('/SuccessEmail', function(req, res) {
    res.render('pages/verification/SuccessEmail');
  });
  app.get('/SuccessGrievance', function(req, res) {
    res.render('pages/GrievenceForm/SuccessGrievance');
  });
  app.get('/ChangePassword', function(req, res) {
    res.render('pages/ChangePassword');
  });
  app.get('/ResetPassword', function(req, res) {
    res.render('pages/ResetPassword');
  });
  app.get('/SetPreference', function(req, res) {
    res.render('pages/SetPreference');
  });
  app.get('/JobPreference', function(req, res) {
    res.render('pages/JobPreference');
  });
  app.get('/Gallery', function(req, res) {
    res.render('pages/Gallery/Gallery');
  });
  app.get('/with-jquery', function(req, res) {
    res.render('pages/with-jquery');
  });
  app.get('/FooterDetails', function(req, res) {
    res.render('pages/FooterDetails');
  });
  app.get('/CareerCounsellingDetails', function(req, res) {
    res.render('pages/CareerCounselling/CareerCounsellingDetails');
  });
  app.get('/JobSeekerRegister', function(req, res) {
    res.render('pages/CareerCounselling/JobSeekerRegister');
  });
  app.get('/Jobfairdetails', function(req, res) {
    res.render('pages/JobFairDetails/Jobfairdetail');
  });
  app.get('/Jobfair/jobseekerselection', function(req, res) {
    res.render('pages/JobFairDetails/jobseekerselection');
  });
  app.get('/forms', function(req, res) {
    res.render('pages/AboutUs/forms');
  });
  app.get('/formsandfont', function(req, res) {
    res.render('pages/AboutUs/formsandfont');
  });
  app.get('/loksewaGuarantee', function(req, res) {
    res.render('pages/AboutUs/loksewaGuarantee');
  });
  app.get('/UnionPublicService', function(req, res) {
    res.render('pages/AboutUs/UnionPublicService');
  });
  app.get('/JobFairList', function(req, res) {
    res.render('pages/JobFairList');
  });
  app.get('/EmailSupport', function(req, res) {
    res.render('pages/EmailSupport');
  });
  app.get('/testingjob', function(req, res) {
    res.render('pages/testing');
  });
  app.get('/jobmodal', function(req, res) {
    res.render('pages/JobSearch/jobmodal');
  });     
  app.get('/RegistrationSuccess', function(req, res) {
    res.render('pages/RegistrationSuccess');
  });     
  app.get('/SearchJobs', function(req, res) {
    res.render('pages/JobSearch/SearchJobs');
  });   

  app.get('/JobListbySearch', function(req, res) {
    res.render('pages/JobSearch/JobListbySearch');
  });   
  app.get('/WalkIn', function(req, res) {
    res.render('pages/WalkIn');
  });  
  app.get('/HallTicket', function(req, res) {
    res.render('pages/HallTicket');
  }); 
  app.get('/LandingPage2', function(req, res) {
    res.render('pages/Index1');
  }); 
  app.get('/About_new', function(req, res) {
    res.render('pages/AboutUs/About_new');
  });

  app.get('/PublicPrivatePartnership', function(req, res) {
    res.render('pages/AboutUs/PublicPrivatePartnership');
  });

  app.get('/JsFAQ', function(req, res) {
    res.render('pages/JsFAQ',{
      
      CandidateId: req.cookies.CandidateId
     
    });
   
  });
  
  app.get('/userDetail', function(req, res) {
    res.render('pages/userDetail');
  });
  app.get('/PopularSearchJob', function(req, res) {
    res.render('pages/PopularSearchJob');
  });
  app.get('/OurStories', function(req, res) {
    res.render('pages/OurStories');
  });
  app.get('/CareerCounsellingForm', function(req, res) {
    res.render('pages/CareerCounselling/CareerCounsellingForm');
  });
  app.get('/ER1Form', function(req, res) {
    res.render('pages/AboutUs/ER1Form');
  });
  app.get('/Dashboard', function(req, res) {
    res.render('pages/Dashboard');
  });

};  



